# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-01-03 09:47:07
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-21 17:24:48

# SYSTEM
import sys
import os
import logging
import pickle
import argparse
import numpy as np
from functools import partial
from glob import glob

# QT
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.uic import loadUiType
from PyQt5.QtCore import pyqtSlot, pyqtSignal
import sip
from ui.systemtrayicon import SystemTrayIcon

# UI
from ui.windowcontroller import DockWindowController
from ui.experimentPropertiesWidget import ExperimentPropertiesWidget
from ui.placeholder import EmptyPlaceholderWidget
from ui.trainingProtocolWidget import TrainingProtocolWidget
from ui.importBackupDialog import ImportBackupDialog

# MODEL
from core.project import Project
from core.annotation import Annotation
from core.experiment import Experiment
from ml.oracle import QueryResult, QueryItem

# CONTROLLERS
from core.query_ctrl import QueryController
from core.project_ctrl import ProjectController
from ml.human_annotation_oracle import HumanAnnotationOracle
from ml.dataset_oracle import DatasetOracle
from core.progresslogging import ProgressLogger

# ALGORITHMS
from ml.learner.learner import BaseLearner
from ml.learner.clusterlearner import ClusterLearner, IterativeClusterLearner
from ml.learner.clustersegmentlearner import ClusterSegmentLearner, IterativeClusterSegmentLearner
from ml.learner.uncertaintylearner import MinDistanceLearner, ClassifierUncertaintyLearner
from ml.metrics import class_average

# UTILS
from core.utils import copy_project_storage, is_project_folder, clear_project_folder, mkdir_p, now, copy_folder
from tools.export import (export_project_properties,
                          export_experiment_properties,
                          export_annotation_to_csv,
                          export_sklearn_model)
from tools.import_backup import (import_detection_model,
                                 import_feature_processor,
                                 import_labeling_stats,
                                 import_logs, import_annotations)
from tools.config_manager import ConfigManager
from ui.messages import msg_incompatible_project

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
uipath = os.path.join(curpath, 'ui')
Ui_MainWindow, QMainWindow = loadUiType(os.path.join(curpath,'ui',
                                                     'MainWindowMDI_UserExp.ui'))

# LOGGING
_LOGFORMAT = '%(asctime)-9s %(levelname)-6s [%(name)s] - %(message)s'
_LOGDATEFORMAT = '%H:%M:%S'

# logging.basicConfig(level=15, format=_LOGFORMAT, datefmt=_LOGDATEFORMAT)
logger = logging.getLogger(__name__)


########################################################################
########################################################################


class ITTProjectOverview(QMainWindow, Ui_MainWindow):

    notify_app = pyqtSignal(str, str)
    trigger_experiment_properties = pyqtSignal()

    @property
    def project(self):
        return self.project_ctrl.get_project()

    def __init__(self, config_file, default_project, initial_labels=None):
        super(ITTProjectOverview, self).__init__()
        self.setupUi(self)

        self.window_ctrl = DockWindowController(self)
        self.progress_logger = None

        # "global" instance management
        # ------------------

        self.project_ctrl = ProjectController(Project('default_project'))

        # load project
        startup_project = os.path.join('projects', default_project, '{}.ittproject'.format(default_project))
        if os.path.isfile(startup_project):
            prj = self._load_project(startup_project)

            if prj is None:
                # for user experiment, we need a project, so we just crash if there isn't one.
                raise ValueError("Invalid startup project. Abort.")

            self.project_ctrl.set_model(prj)
            self.project_has_changed = False
        else:
            raise ValueError('Could not locate project "{}".'.format(startup_project))

        self.experiment = Experiment(self.project_ctrl.name,
                                     participant_name='')
        self.initial_labels = initial_labels
        self.project_ctrl.changed.connect(self.project_update)

        # ADAPT LEARNER OPTIONS:
        # ----------------------

        cfg_manager = ConfigManager(config_file)
        self.global_param = cfg_manager.get_global_parameters()
        self.learner = cfg_manager.make_learner(self.project_ctrl)
        self.oracle = cfg_manager.make_oracle(self.project_ctrl, self)
        self.ui_settings = cfg_manager.get_ui_parameters()
        self.protocol_settings = cfg_manager.get_protocol_parameters()
        if 'user_break_after' not in self.protocol_settings:
            self.protocol_settings['user_break_after'] = None

        # -----------------------------------------

        logger.info('Configured learner: {}'.format(self.learner))

        # -----------------

        self.query_ctrl = QueryController(self.learner)
        self.query_ctrl.update_queued.connect(self.trigger_learner_update)
        self.project_ctrl.changed.connect(self.learner.project_update)

        # SETUP REST OF THE UI
        # ----------------------
        self.init_main_menu()
        self.init_main_widgets()

        self.busyBar = QtWidgets.QProgressBar()
        self.busyBar.setRange(0, 1)
        self.statusbar.addPermanentWidget(self.busyBar, 0)
        self.busyBar.setVisible(False)

        self._disable_on_bg_process = [
            self.menubar, self.window_ctrl['training_protocol'].groupBoxTrainingProtocol, self.window_ctrl['experiment_properties']
        ]

        # Thread handling
        # ---------------
        self.bg_thread = None
        self._init_background_thread()
        self._init_query_background_worker()
        self._init_learner_update_background_worker()

        # Startup
        # -------------------
        self.project_has_changed = True
        self.setup_new_progress_logger(clear_old_log=True)
        self.project_ctrl.emit_changed('project')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # SETUP UI AND ACTION SIGNALS
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def init_main_menu(self):
        # Menu items
        # self.actionProperties.triggered.connect(self.edit_experiment_properties)
        # self.actionExport.triggered.connect(self.export_experiment)
        self.actionReset.triggered.connect(self.reset_progress)
        self.actionImport.triggered.connect(self.import_backup)

    def init_main_widgets(self):
        '''
        Create and initialize the main docked widgets.
        '''

        self.setCentralWidget(EmptyPlaceholderWidget(self))

        # add default widgets to dock area
        # --------------------------------

        # EXPERIMENT

        self.window_ctrl.add_window('experiment_properties', ExperimentPropertiesWidget(self.experiment, self), 'left')

        # TRAINING PROTOCOL
        self.window_ctrl.add_window('training_protocol',
                                    TrainingProtocolWidget(self),
                                    'left')

        self.window_ctrl['training_protocol'].set_protocol_values(
                        self.protocol_settings['max_iterations'],
                        self.protocol_settings['min_iterations'],
                        self.protocol_settings['n_repetitions'])
        self.window_ctrl['training_protocol'].btnStartProtocol.clicked.connect(self._trigger_training_protocol)
        self.window_ctrl['training_protocol'].btnCancelProtocol.clicked.connect(self._cancel_training_protocol)

        # hide windows by default:
        if 'hide_by_default' in self.ui_settings:
            for wname in self.ui_settings['hide_by_default']:
                self.window_ctrl.set_visible(wname, False)

    def toggle_central_widget_visible(self, visible=None):
        if self.centralWidget() is not None:
            if visible is None:
                self.centralWidget().setVisible(not self.centralWidget().isVisible())
            else:
                self.centralWidget().setVisible(visible)

    def show_central_widget(self, widget):
        current = self.centralWidget()
        if current != widget:
            self.setCentralWidget(widget)
            sip.delete(current)
        self.toggle_central_widget_visible(True)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Handling background threads
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def _init_background_thread(self):
        if self.bg_thread is None:
            self.bg_thread = QtCore.QThread()
            self.bg_thread.start()

    def _kill_background_thread(self):
        if self.bg_thread is not None:
            self.bg_thread.quit()

    def closeEvent(self, event):
        self._kill_background_thread()
        super(ITTProjectOverview, self).closeEvent(event)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # UI update
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def emit_notify_app(self, title, text):
        self.notify_app.emit(title, text)

    @pyqtSlot(str)
    def project_update(self, what):

        if what in ['project', 'name', 'annotation', 'label_set',
                    'statistics', 'detection_model', 'bootstrap']:
            self.project_has_changed = True

        if what == 'project':
            self.update_oracle_connections()

        if what in ['project', 'name']:
            self.update_window_title()

    def update_window_title(self):
        self.setWindowTitle('{}ITT - {}'.format('* ' if self.project_has_changed else '', self.project_ctrl.name))

    def update_oracle_connections(self):

        # disconnecting existing
        try:
            self.query_ctrl.queried_item.disconnect(self.oracle.receive_query)
        except TypeError:
            pass  # that's alright, we were not connected yet
        try:
            self.oracle.responded.disconnect(self.query_ctrl.on_query_result)
        except TypeError:
            pass  # that's alright, we were not connected yet

        # connect
        self.query_ctrl.queried_item.connect(self.oracle.receive_query)
        self.oracle.responded.connect(self.query_ctrl.on_query_result)

    def setup_new_progress_logger(self, clear_old_log=False):
        logger.debug('Setup new progress logger. Clear old logs = {}'.format(clear_old_log))

        # disable existing logger
        # -----------------------
        if self.progress_logger is not None:
            # Disconnect signals
            try:
                self.query_ctrl.queried_item.disconnect(self.progress_logger.on_queried_item)
            except TypeError:
                pass  # that's alright, we were not connected yet

            try:
                self.oracle.responded.disconnect(self.progress_logger.on_query_result)
            except TypeError:
                pass  # that's alright, we were not connected yet

            try:
                self.learner.query_suggested.disconnect(self.progress_logger.on_query_suggested)
            except TypeError:
                pass  # that's alright, we were not connected yet

            try:
                self.learner.updated_detection_model.disconnect(self.progress_logger.on_update_detection_model)
            except TypeError:
                pass  # that's alright, we were not connected yet

            try:
                self.learner.learner_state_changed.disconnect(self.progress_logger.on_learner_update)
            except TypeError:
                pass  # that's alright, we were not connected yet

            if clear_old_log:
                self.progress_logger.delete_logs()
            else:
                # close all file handlers
                self.progress_logger.close_all_logs()

        # new logger
        # ----------

        self.progress_logger = ProgressLogger(self.project_ctrl, output_dir=os.path.join(self.project_ctrl.get_project_directory(), 'logs'),
                                              clear_existing=clear_old_log)

        # connect to signals
        self.query_ctrl.queried_item.connect(self.progress_logger.on_queried_item)
        self.oracle.responded.connect(self.progress_logger.on_query_result)
        self.learner.query_suggested.connect(self.progress_logger.on_query_suggested)
        self.learner.updated_detection_model.connect(self.progress_logger.on_update_detection_model)
        self.learner.learner_state_changed.connect(self.progress_logger.on_learner_update)

    def _disable_all_ui_but_quit(self):
        for qitem in self._disable_on_bg_process:
            qitem.setEnabled(False)

    def _enable_all_ui(self):
        for qitem in self._disable_on_bg_process:
            qitem.setEnabled(True)

    def activate_busyBar(self, pulsing=True, status_msg=None):
        self._disable_all_ui_but_quit()
        if status_msg is not None:
            self.statusbar.showMessage(status_msg)
        # self.busyBar.setVisible(True)
        # self.busyBar.setRange(0, 0)

    def deactivate_busyBar(self):
        self._enable_all_ui()
        self.statusbar.clearMessage()
        # self.busyBar.setRange(0, 1)
        # self.busyBar.setVisible(False)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Project Management (creating, loading, saving)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @pyqtSlot(bool)
    def load_project(self, checked):

        # TODO: check for saving current project
        # if self.project_changed:
        #  ...

        # initialize file selector
        selected_file, _ = QtWidgets.QFileDialog.getOpenFileName(self, caption='Open project', directory='projects', filter='*.ittproject')

        if not selected_file or selected_file == '':
            return

        prj = self._load_project(selected_file)

        if prj is None:
            return

        self.project_ctrl.set_model(prj)
        self.setup_new_progress_logger()

        self.statusbar.showMessage('Project "{}" loaded.'.format(self.project.name), 2000)

    @pyqtSlot(bool)
    def save_project(self, checked):
        if self.project_ctrl.get_project_file() is not None:
            self._save_project(self.project_ctrl.get_project_file())
            export_project_properties(self.project_ctrl,
                                      os.path.splitext(self.project_ctrl.get_project_file())[0] + os.path.extsep + 'json',
                                      extra_data={
                                      'learner': str(self.learner),
                                      'oracle': str(self.oracle)})
            self.project_has_changed = False
            self.update_window_title()
            self.statusbar.showMessage('Project "{}" saved.'.format(self.project.name), 2000)
        else:
            self.save_project_as(checked)

    @pyqtSlot(bool)
    def save_project_as(self, checked):

        # init file selector
        # save...

        selected_filename, _ = QtWidgets.QFileDialog.getSaveFileName(self, caption='Save project as', directory='projects', filter='*.ittproject')

        if not selected_filename or selected_filename == '':
            return

        selected_filename = os.path.normpath(selected_filename)
        selected_path = os.path.dirname(selected_filename)

        logger.debug('Save as: {}'.format(selected_filename))
        logger.debug('Current prj dir: {}'.format(self.project_ctrl.get_project_directory()))

        # if this is not the project's own storage location
        if selected_path != self.project_ctrl.get_project_directory():
            logger.debug('Moving project to new directory: {}'.format(selected_path))

            # if there is already another project at the location, ask to overwrite
            if is_project_folder(selected_path):
                QtWidgets.QMessageBox.warning(self, 'Existing project folder', 'The chosen directory already contains a project.\nPlease chose another directory.',
                        buttons=QtWidgets.QMessageBox.Ok)
                return

            # move the project folder to its new location
            self.progress_logger.close_all_logs()
            copy_project_storage(self.project_ctrl.get_project_directory(),
                                 selected_path)

        # update project meta data and save project file
        self.project_ctrl.set_project_file_location(selected_filename)
        self._save_project(selected_filename)
        export_project_properties(self.project_ctrl,
                                  os.path.splitext(self.project_ctrl.get_project_file())[0] + os.path.extsep + 'json',
                                  extra_data={'learner': str(self.learner),
                                              'oracle': str(self.oracle)})
        self.setup_new_progress_logger()
        self.project_has_changed = False
        self.update_window_title()
        self.statusbar.showMessage('Project "{}" saved.'.format(self.project.name), 2000)

    def _load_project(self, filename):
        if (
            not os.path.isfile(filename) or
            os.path.splitext(filename)[1] != '.ittproject'
        ):
            raise IOError('Invalid project file: {}.'.format(filename))

        try:
            # unpickle class attributes
            with open(filename, 'rb') as fp:
                prj = pickle.load(fp)
        except ImportError as err:

            # Handle old, incompatible project files that were created with Qt4:
            if err.message == "No module named indexes.base":
                msg_incompatible_project(self, project_name=filename)
                return None

        return prj

    def _save_project(self, filename):
        """
        Save project to disk.

        Parameters
        ----------
        filename : str, optional
            Description
        """
        if os.path.splitext(filename)[1] != '.ittproject':
            filename = filename + os.path.extsep + 'ittproject'

        mkdir_p(os.path.dirname(filename))

        with open(filename, 'wb') as fp:
            pickle.dump(self.project, fp)

    def load_labels_from(self):

        selected_files, _ = QtWidgets.QFileDialog.getOpenFileNames(self, caption='Open label files', filter='Label files (*.h5 *.csv)')

        if selected_files is None or len(selected_files) == 0:
            return

        success = self.project_ctrl.load_annotations_from_files(selected_files, replace=False)

        self.statusbar.showMessage('Labels {}updated.'.format('' if success else 'not '))

    @pyqtSlot(bool)
    def reset_progress(self, checked=True):
        logger.info('User reqested project reset.')
        ret = QtWidgets.QMessageBox.warning(self, 'Reset all progress?', 'You will loose all information gathered in this project (annotations, predictions, etc.).\n\nAre you sure to reset the project?',
                                  buttons=QtWidgets.QMessageBox.Yes|QtWidgets.QMessageBox.No,
                                  defaultButton=QtWidgets.QMessageBox.No)

        if ret == QtWidgets.QMessageBox.Yes:
            self._reset_learning_progress()

    def _reset_learning_progress(self, backup_progress=True):

        logger.info('Resetting learning progres...')

        if backup_progress:
            self._backup_progress()

        # reset project: annotations, detection model
        self.project_ctrl.reset_project()

        # reset internal learner variables
        self.learner.reset()

        # clear logs and start fresh
        self.setup_new_progress_logger(clear_old_log=True)

        # reset some ui elements
        self.window_ctrl['training_protocol'].clear_progress()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Saving and restoring annotation progress (backup)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def _backup_progress(self):
        # backing up:
        #  - oracle annotated labels
        #  - predictions of current detection model
        #  - all logs up to now (queries, responses, model updates, app logger (if active))
        timestamp = now(microsec=False)
        backup_dir = os.path.join(self.project_ctrl.get_project_directory(), 'backups', timestamp)

        # export labels
        self._export_labels(export_dir=os.path.join(backup_dir, 'exports', 'labels_{}'.format(timestamp)))

        # export predictions
        self._export_predictions(export_dir=os.path.join(backup_dir, 'exports', 'predictions_{}'.format(timestamp)))

        # export detection model and feature processing
        export_sklearn_model(self.project_ctrl.get_detection_model().model,
                             os.path.join(backup_dir, 'models', 'detection_model_{}.gz'.format(timestamp)))

        export_sklearn_model(self.project_ctrl.get_feature_processor().model,
                             os.path.join(backup_dir, 'models', 'feature_processor_{}.gz'.format(timestamp)))

        # copy logs to backup dir
        logdir = self.progress_logger.get_log_directory()
        copy_folder(logdir, os.path.join(backup_dir, 'logs'))

        ppfile = os.path.join(backup_dir,
                              self.project_ctrl.name + os.path.extsep + 'json')
        export_project_properties(self.project_ctrl, ppfile, extra_data={'learner': str(self.learner), 'oracle': str(self.oracle)})

        expfile = os.path.join(backup_dir,
                               'experiment' + os.path.extsep + 'json')
        export_experiment_properties(self.experiment, expfile)


    @pyqtSlot(bool)
    def import_backup(self, checked):
        logger.log(15, "Try to import previous backup...")

        # TODO: check for saving current project
        import_backup_dlg = ImportBackupDialog()
        ret = import_backup_dlg.exec_()

        if ret == 1:
            import_backup_data = import_backup_dlg.get_form_data()

            logger.info(import_backup_data)

            # deactivate progresslogger
            self.disable_progress_logger(clear_log=True)

            backup_path = import_backup_data['backup_folder']

            if import_backup_data['import_detection_model']:
                # load the last available model:
                model_file = sorted(glob(os.path.join(backup_path, 'models', 'detection_model_*.gz')))[-1]
                import_detection_model(self.project_ctrl, model_file)

                # force fitting state of detection model to get updated accuracies
                self.project_ctrl.get_detection_model()._is_fitted = True
                logger.debug("Imported detection model.")

            if import_backup_data['import_feature_processor']:
                # load the last available model:
                model_file = sorted(glob(os.path.join(backup_path, 'models', 'feature_processor_*.gz')))[-1]
                import_feature_processor(self.project_ctrl, model_file)

                # force fitting state of detection model to get updated accuracies
                self.project_ctrl.get_detection_model()._is_fitted = True
                logger.debug("Imported feature processor.")

            if import_backup_data['import_annotations']:
                # load the last available labels:
                labels_folder = sorted(glob(os.path.join(backup_path, 'exports', 'labels_*')))[-1]
                label_files = sorted(glob(os.path.join(labels_folder, '*.csv')))
                import_annotations(self.project_ctrl, label_files)
                logger.debug("Imported annotations.")

            if import_backup_data['import_logs']:
                # load log files:
                logs_folder = os.path.join(backup_path, 'logs')
                import_logs(self.project_ctrl, logs_folder)
                logger.debug("Imported logs.")

            # update statistics history
            import_labeling_stats(self.project_ctrl)

            logger.info("Imported backup '{}'.".format(backup_path))
            self.statusbar.showMessage('Backup "{}" imported.'.format(os.path.basename(backup_path)), 2000)
            self.setup_new_progress_logger(clear_old_log=False)

            # force fitting state of detection model to get updated accuracies
            self.project_ctrl.get_detection_model()._is_fitted = True

            self.project_ctrl.get_prediction(force_update=True)
            if self.project_ctrl.has_testset():
                self.project_ctrl.get_testset_prediction(force_update=True)

            self.project_ctrl.emit_changed('statistics')
            self.project_ctrl.emit_changed('history')
            return True
        else:
            return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Export labels/predictions
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @pyqtSlot(bool)
    def export_labels(self, checked, export_dir=None):
        # check any labels available for export?
        if len(self.project_ctrl.get_label_set()) == 0:
            QtWidgets.QMessageBox.information(self, 'No labels available for export', 'There are no labels available for export because you have not defined the label set yet.')
            return

        if self.project_ctrl.get_annotation().count_labeled(include_uncertain=True) == 0:
            QtWidgets.QMessageBox.information(self, 'No labels available for export', 'There are no labels available for export because you have not labeled any video segments yet.')
            return

        files_written = self._export_labels(export_dir)

        QtWidgets.QMessageBox.information(self, 'Export completed', 'Exported the labels to the following files:\n{}'.format('\n'.join(files_written)))

    def export_labels_as(self):
        # ask the user where to export the labels to
        # check any labels available for export?
        if len(self.project_ctrl.get_label_set()) == 0:
            QtWidgets.QMessageBox.information(self, 'No labels available for export', 'There are no labels available for export because you have not defined the label set yet.')
            return

        if self.project_ctrl.get_annotation().count_labeled(include_uncertain=True) == 0:
            QtWidgets.QMessageBox.information(self, 'No labels available for export', 'There are no labels available for export because you have not labeled any video segments yet.')
            return

        selected_folder = QtWidgets.QFileDialog.getExistingDirectory(self, caption='Save labels in...')

        if not selected_folder or selected_folder == '':
            return

        selected_folder = os.path.normpath(selected_folder)

        logger.debug('Save labels in: {}'.format(selected_folder))

        files_written = self._export_labels(selected_folder)
        self.statusbar.showMessage('Exported {} files with labels.'.format(len(files_written)))

    def _export_labels(self, export_dir=None):
        # we will automatically export to the exports folder if not set
        if export_dir is None:
            export_dir = os.path.join(self.project_ctrl.get_project_directory(), 'exports', 'labels_{}'.format(now(microsec=False)))
        filepattern = 'ITT_LabelExport_%s_{video:s}.csv' % self.project_ctrl.get_dataset().name

        # save to disk
        return export_annotation_to_csv(self.project_ctrl.get_annotation(),
                                                 export_dir,
                                                 filepattern,
                                                 self.project_ctrl.get_metadata()['fps'],
                                                 event_format='frames')


    @pyqtSlot(bool)
    def export_predictions(self, checked, export_dir=None):
        # check any predictions available for export?
          # update detection model first?
        # check any labels available for export?
        if len(self.project_ctrl.get_label_set()) == 0:
            QtWidgets.QMessageBox.information(self, 'No labels available for export', 'There is not prediction available for export because you have not defined the label set yet.')
            return

        if self.project_ctrl.get_project().learning_stats.query_response_stats[QueryResult.LABELED] == 0:
            QtWidgets.QMessageBox.information(self, 'No prediction available for export', 'There is no prediction available for export because you have not labeled any video segments yet.')
            return

        files_written = self._export_predictions(export_dir)

        QtWidgets.QMessageBox.information(self, 'Export completed', 'Exported the predictions to the following files:\n{}'.format('\n'.join(files_written)))

    def _export_predictions(self, export_dir=None):
        # convert prediction Series to Annotation object
        dfpred = self.project_ctrl.get_prediction()

        if dfpred is None:
            logger.warning('Cannot export predictions: No detection model.')
            return []

        if self.project_ctrl.has_testset():
            dfpredtest = self.project_ctrl.get_testset_prediction()
            dfpred = dfpred.append(dfpredtest).sort_index(0)

        annot_pred = Annotation.from_dataframe(dfpred,
                                               is_mutually_exclusive=True)

        if export_dir is None:
            # we will automatically export to the exports folder
            export_dir = os.path.join(self.project_ctrl.get_project_directory(), 'exports', 'predictions_{}'.format(now(microsec=False)))
        filepattern = 'ITT_LabelExport_%s_{video:s}.csv' % self.project_ctrl.get_dataset().name

        # save to disk
        return export_annotation_to_csv(annot_pred,
             export_dir,
             filepattern,
             self.project_ctrl.get_metadata()['fps'],
             event_format='frames')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # QUERY CONTROL
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def _init_query_background_worker(self):
        # move query controller to the background thread
        self.query_ctrl.moveToThread(self.bg_thread)

        # setup connection between main UI (self) and the query controller

        # on error in query controller:
        self.query_ctrl.query_error.connect(self._query_error)

        # on query_item finished update UI (subsequent calls for oracle etc)
        self.query_ctrl.queried_item.connect(self._query_complete)

    def trigger_single_iteration(self):
        self._is_training_protocol = False
        self._trigger_one_labeling_iteration()

    def _trigger_one_labeling_iteration(self):

        if not self._is_training_protocol:
            # some UI feedback that something is happening in the background
            self.activate_busyBar(status_msg='Finding interesting video segment...')

        if (self.project_ctrl.get_feature_processor() is not None and
            not self.project_ctrl.get_feature_processor().is_fitted()):
            self.project_ctrl.fit_feature_processing()

        # if there are no labeled instances yet, ask the oracle to provide one for each class
        if (self.global_param['allow_initialization'] and
            not self.project_ctrl.get_annotation().has(Annotation.ACTIVE) and
            self.oracle.provides_query_kind('sample')):

            logger.info('Query initial class examples.')
            samples = self.oracle._determine_response(QueryItem('initialization', self.project_ctrl.get_label_set(), kind='sample'))

            if samples.state != QueryResult.LABELED:
                self._query_error(samples.response_value)
                QtWidgets.QMessageBox.critical(self, 'Sampling failed', 'The sampling of initial data points failed with error: "{}".'.format(samples.response_value),
                        buttons=QtWidgets.QMessageBox.Ok)
                return

            for label, index in samples.response_value.viewitems():
                logger.log(logging.DEBUG, 'Received {} at {}.'.format(label, index))
                self.project_ctrl.update_annotation(index, label)

            logger.info('Received {} initial examples.'.format(len(samples.response_value)))

            # train and evaluate initial model:
            self.project_ctrl.emit_changed('bootstrap')

        # trigger query controller (emit query) in background as suggesting a query may take a bit of time, depending on what the learner actually performs
        logger.info('Learning iteration: {}'.format(self.project_ctrl.get_current_learning_iteration()))
        self.activate_busyBar(status_msg='Please answer the query.')
        self.query_ctrl.trigger_query.emit(self.global_param['query_type'])

    def _query_error(self, message):
        # catches query errors
        logger.error('Labeling failed: {}'.format(message))

        if not self._is_training_protocol:
            # set back progress bar
            self.deactivate_busyBar()

    def _query_complete(self):

        if not self._is_training_protocol:
            # query complete, deactivate progress bar
            self.deactivate_busyBar()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # TRAINING PROTOCOL
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def _trigger_training_protocol(self, continue_repetition=False):
        """
        Starts a training protocol with multiple labeling iterations and stopping criteria.

        Establishes several signal/slot connections to catch the oracle's response and set off a new labeling iteration.

        Will end by calling _trigger_one_labeling_iteration().
        """
        self._is_training_protocol = True

        tpwidget = self.window_ctrl['training_protocol']

        self._tp = {}
        self._tp['n_iter'] = 0
        self._tp['min_iterations'] = tpwidget.numMinIterations.value()
        self._tp['max_iterations'] = tpwidget.numMaxIterations.value()
        self._tp['stop_criteria'] = self.protocol_settings['stop_criteria_abs_performance']
        self._tp['monitor_value'] = []
        self._tp['repetition_idx'] = 1
        self._tp['cancel_requested'] = False
        if not continue_repetition:
            self._tp_reps_left = tpwidget.numRepetitions.value()
        else:
            self._tp['repetition_idx'] = tpwidget.numRepetitions.value() - self._tp_reps_left + 1

        # set protocol progress widgets
        tpwidget.update_values(n_iter=self._tp['n_iter'],
                               repetition_idx=self._tp['repetition_idx'])
        self.window_ctrl['training_protocol'].set_status_message('Running...')

        logger.info(('Initiate training protocol with parameters: ' +
                     'min_iter = {}, ' +
                     'max_iter = {}, ' +
                     'stop_crit = {}').format(self._tp['min_iterations'],
                                              self._tp['max_iterations'],
                                              self._tp['stop_criteria']))

        self.learner.set_update_cycle(self.global_param['retraining_iterations'])
        self.progress_logger.filter_learner_state = self.global_param['log_learner_update_iterations']

        # load initial labels if available
        if (self.global_param['allow_initialization'] and
            not self.project_ctrl.get_annotation().has(Annotation.ACTIVE) and
            self.initial_labels is not None):

            # make sure feature processing is fitted
            if (self.project_ctrl.get_feature_processor() is not None and
            not self.project_ctrl.get_feature_processor().is_fitted()):
                self.project_ctrl.fit_feature_processing()

            if not isinstance(self.initial_labels, (list, tuple)) and os.path.isdir(self.initial_labels):
                self.initial_labels = [os.path.join(self.initial_labels, f) for f in os.listdir(self.initial_labels) if os.path.isfile(os.path.join(self.initial_labels, f))]

            success = self.project_ctrl.load_annotations_from_files(self.initial_labels, replace=False, ignore_unknown_file_formats=True)
            if success:
                logger.info('Loaded initial labels from file.')
            else:
                logger.warning('Failed to load initial labels from file.')

            self.project_ctrl.emit_changed('bootstrap')

        # connect to query signals
        self.query_ctrl.query_error.connect(self._training_protocol_canceled)
        # self.oracle.responded.connect(self._training_protocol_iteration_done)
        self.learner.learner_update_complete.connect(self._training_protocol_iteration_done)
        self.learner.updated_detection_model.connect(self._training_protocol_check_stopping_criteria)

        self.activate_busyBar(status_msg='Executing training protocol...')
        tpwidget.btnCancelProtocol.setEnabled(True)

        logger.info('Waiting for participant to be ready...')

        QtWidgets.QMessageBox.information(self, 'Experiment protocol', self.protocol_settings['trial_start_message'].format(rep=self._tp['repetition_idx'], total=tpwidget.numRepetitions.value()))

        logger.info('TP Iteration: {}'.format(self._tp['n_iter']))
        self._trigger_one_labeling_iteration()

    def _training_protocol_iteration_done(self):
        """
        Slot that is called when an iteration within a training protocol is done.

        Checks stopping criteria of training protocol and if not reached, triggers the next labeling iteration.
        """

        logger.info('TP Iteration done: {}'.format(self._tp['n_iter']))

        # iteration counter
        self._tp['n_iter'] += 1

        self.window_ctrl['training_protocol'].update_values(n_iter=self._tp['n_iter'])

        # stop if we have reached the maximum number of iterations
        if self._tp['n_iter'] >= self._tp['max_iterations']:
            self._training_protocol_finished('TP reached maximum number of iterations: {}'.format(self._tp['max_iterations']))
            return

        # we have to get through at least min iterations before we need to check for the stopping criteria:
        if self._tp['n_iter'] >= self._tp['min_iterations']:

            # stop protocol if stopping criteria is reached
            #  monitor_value is appended in the background every couple of iterations
            #  the performance stopping criteria is reached if:
            #   we have collected at least n (default: 3) monitor values, and
            #   the performance in the last iteration is larger than a threshold (default: F1 > 0.5), and
            #   the sum of absolute change in performance over the last n iterations is lower than some epsilon (default: 0.003).

            w = self.protocol_settings['stop_criteria_window'] + 1

            if (len(self._tp['monitor_value']) >= w and
                self._tp['monitor_value'][-1] > self._tp['stop_criteria'] and
                np.mean(np.abs(np.diff(self._tp['monitor_value'][-w:]))) < self.protocol_settings['stop_criteria_max_error']):

                self._training_protocol_finished('TP reached stopping criteria: {:.3f} > {:.3f}'.format(self._tp['monitor_value'][-1], self._tp['stop_criteria']))
                return

        if self._tp['cancel_requested']:
            self._training_protocol_canceled('User canceled protocol.')
            return

        if (self.protocol_settings['user_break_after'] is not None and
            self._tp['n_iter'] % self.protocol_settings['user_break_after'] == 0):

            logger.info('User is requested to take a short break.')
            QtWidgets.QMessageBox.information(self, 'Take a break', 'Please take a break for a moment. Click OK to continue.')
            logger.info('User returned from break.')


        logger.info('TP Iteration: {}'.format(self._tp['n_iter']))
        self._trigger_one_labeling_iteration()

    def _training_protocol_check_stopping_criteria(self):
        """
        Slot that is called when the underlying detection model is changed. Obtains the current model's accuracy for evaluating the training protocol's stopping criteria.

        Assigns new value to self._tp['monitor_value'].
        """

        # note that single operations on Python built-in variables are inherently thread-safe. So assigning a new value to this dictionary item from another thread, is fine.

        # compute average class F1
        self._tp['monitor_value'].append(class_average(self.project_ctrl.get_test_accuracies(), return_pdseries=True)['F1'])

    def _training_protocol_finished(self, message=''):
        """
        Is called by _training_protocol_iteration_done() if stopping criteria is reached. Disconnects the previously established signal/slot connections of the training protocol.

        Resets the training protocol dictionary (self._tp).

        The passed message is logged on INFO level.

        Parameters
        ----------
        message : str, optional
            Some extra information about the reason why the protocol finished
        """
        logger.info(message)

        self._tp_reps_left -= 1

        try:
            self.query_ctrl.query_error.disconnect(self._training_protocol_iteration_done)
        except TypeError:
            pass  # don't raise if it was not connected

        try:
            self.learner.learner_update_complete.disconnect(self._training_protocol_iteration_done)
        except TypeError:
            pass  # don't raise if it was not connected

        try:
            self.learner.updated_detection_model.disconnect(self._training_protocol_check_stopping_criteria)
        except TypeError:
            pass  # don't raise if it was not connected

        # hide oracle central widget
        self.toggle_central_widget_visible(False)
        self.oracle.reset()

        # if repeated experiments are active, repeat experiment
        if self._tp_reps_left is not None and self._tp_reps_left > 0:
            logger.info('TP repetitions left: {}'.format(self._tp_reps_left))
            self.emit_notify_app('Protocol',
                                 'Finished repetition {} of {}.'.format(self._tp['repetition_idx'], self.window_ctrl['training_protocol'].numRepetitions.value()))
            self._reset_learning_progress()
            self._tp = None
            self._trigger_training_protocol(continue_repetition=True)
        elif self._tp_reps_left is not None and self._tp_reps_left == 0:
            self.emit_notify_app('Protocol', 'Training protocol completed.')
            self._backup_progress()
            self._tp = None
            self.window_ctrl['training_protocol'].btnCancelProtocol.setEnabled(False)
            self.window_ctrl['training_protocol'].set_status_message('Ready')
            self.deactivate_busyBar()
            self.statusbar.showMessage('Training protocol completed.')
            QtWidgets.QMessageBox.information(self, 'Experiment protocol', self.protocol_settings['protocol_complete_message'])
        else:
            self._tp = None
            self.deactivate_busyBar()
            self.window_ctrl['training_protocol'].btnCancelProtocol.setEnabled(False)
            self.window_ctrl['training_protocol'].set_status_message('Ready')
            self.emit_notify_app('Protocol', 'Training protocol crashed.')
            self.statusbar.showMessage('Training protocol finished unexpectedly.')

    def _cancel_training_protocol(self):
        if self._tp is not None:
            self._tp['cancel_requested'] = True
            self.activate_busyBar(status_msg='Protocol will stop after the current query.')

    def _training_protocol_canceled(self, message=''):
        self._tp = None
        logger.info('Training protocol canceled: {}'.format(message))

        try:
            self.query_ctrl.query_error.disconnect(self._training_protocol_iteration_done)
        except TypeError:
            pass  # don't raise if it was not connected

        try:
            self.learner.learner_update_complete.disconnect(self._training_protocol_iteration_done)
        except TypeError:
            pass  # don't raise if it was not connected

        try:
            self.learner.updated_detection_model.disconnect(self._training_protocol_check_stopping_criteria)
        except TypeError:
            pass  # don't raise if it was not connected

        # hide oracle central widget
        self.toggle_central_widget_visible(False)
        self.oracle.reset()

        self.emit_notify_app('Protocol', 'Training protocol canceled.')
        self.deactivate_busyBar()
        self.window_ctrl['training_protocol'].btnCancelProtocol.setEnabled(False)
        self.window_ctrl['training_protocol'].set_status_message('Ready')
        self.statusbar.showMessage('Training protocol canceled.')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # LEARNER UPDATE CONTROL
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def _init_learner_update_background_worker(self):

        # move learner to background thread
        self.learner.moveToThread(self.bg_thread)

        # connect signals between main UI (self) and learner object

        # on error during learner update (not implemented)
        self.learner.update_error.connect(self._learner_update_error)

        # on update complete, update UI
        self.learner.learner_update_complete.connect(self._learner_update_complete)

        self.learner.updated_detection_model.connect(self._learner_updated_detection_model)

    @pyqtSlot(list)
    def trigger_learner_update(self, query_results):

        if not self._is_training_protocol:
            # some UI feedback that something is happening in the background
            self.activate_busyBar(status_msg='Updating learner...')

        # trigger
        self.learner.trigger_update.emit(query_results)

    def _learner_update_error(self, message):
        # catches query errors
        logger.error('Learner update failed: {}'.format(message))

        if not self._is_training_protocol:
            # disable progress bar
            self.deactivate_busyBar()

    def _learner_update_complete(self):
        # UI related response

        if not self._is_training_protocol:
            self.deactivate_busyBar()

    def _learner_updated_detection_model(self):
        self.project_ctrl.append_model_accuracy_history()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Misc.
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



########################################################################
########################################################################

if __name__ == '__main__':

    # parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', required=False,
                        type=str,
                        default='config/default_config.yaml',
                        help='YAML file with configuration of algorithms')
    parser.add_argument('-p', '--project', required=False,
                        type=str, default='PRSCA',
                        help='Name of project to load (name as in /projects)')
    parser.add_argument('-l', '--labels', required=False,
                        help='Path to initial labeling file/folder')
    parser.add_argument('-v', action='count', help='Increase verbosity level')
    args = parser.parse_args()

    # Set verbosity level
    if args.v is None or args.v <= 0:
        loglevel = logging.INFO
    elif args.v == 1:
        loglevel = 15
    else:
        loglevel = logging.DEBUG
    logging.basicConfig(level=loglevel, format=_LOGFORMAT, datefmt=_LOGDATEFORMAT)

    if os.name == 'nt':
        # work around windows issue of grouping together multiple python apps:
        import ctypes
        myappid = u'uu.itt.0.3'  # arbitrary string
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    app = QtWidgets.QApplication(sys.argv)
    app.setStyle('Fusion')

    # set app icon
    app_icon = QtGui.QIcon()
    app_icon.addFile('images/icons/desktop/icon_16.png', QtCore.QSize(16, 16))
    app_icon.addFile('images/icons/desktop/icon_32.png', QtCore.QSize(32, 32))
    app_icon.addFile('images/icons/desktop/icon_48.png', QtCore.QSize(48, 48))
    app_icon.addFile('images/icons/desktop/icon_128.png', QtCore.QSize(128, 128))
    app_icon.addFile('images/icons/desktop/icon_256.png', QtCore.QSize(256, 256))
    app.setWindowIcon(app_icon)

    # Go, start the application
    w = ITTProjectOverview(config_file=args.config,
                           default_project=args.project,
                           initial_labels=args.labels)

    systray_icon = SystemTrayIcon(app_icon, w)
    systray_icon.exit_requested.connect(app.closeAllWindows)
    w.notify_app.connect(systray_icon.showMessage)
    systray_icon.show()

    w.show()
    sys.exit(app.exec_())
