Interactive Training Tool (ITT)
===============================

![Interface](images/interface_user.jpg)

### Changelog

- 2020-02-22:
    + Added examples of `metadata.json` and `dinfo.json` files to Readme
- 2017-08-18:
    + Updated code to PyQt5/Qt5 mainly for better video support (does not depend direcly on Phonon module)
    + because of imcompatibility, the VideoList is temporarily disabled until I find a replacement for `qtpandas`

### Installation

* Create a virtual environment (assuming you're using [Anaconda](https://www.continuum.io/downloads)):
    - `$ conda create --name itt python=2.7`
    - `$ activate itt`
- And install dependency packages in that environment
    - `$ cd InteractiveTrainingTool`
    - `$ conda install --file requirements.txt`
* Link to MLUtilities package
    - Under Linux: add a `*.pth` file to the environment's site-package directory containing the absolute path to the folder with the [MLUtilities](https://gitlab.com/mlorbach/MLUtilities) package, e.g. (this may look slightly different on your machine):
        + echo "~/_libs/MLUtilities" > ~/anaconda2/envs/itt/lib/python2.7/site-packages/MLUtilities.pth
    - Under Windows: add the path to the folder with the MLUtilities package to your $PYTHONPATH variable
- (optional) Configure [Sublime Text 3](https://www.sublimetext.com) as code editor
    + install appropriate packages: e.g., ST3 anaconda
    + make a new project and include ITT folder to project
    + Project -> Edit project:

        ```json
        {
            "build_systems":
            [
                {
                    "file_regex": "^[ ]*File \"(...*?)\", line ([0-9]*)",
                    "name": "Anaconda Python Builder",
                    "selector": "source.python",
                    "shell_cmd": "\"C:\\Program Files\\Anaconda\\envs\\itt\\python.exe\" -u \"$file\""
                }
            ],
            "extra_paths":
            [
                "..\\_libs\\MLUtilities"
            ],
            "folders":
            [
                {
                    "path": "."
                }
            ],
            "settings":
            {
                "python_interpreter": "C:\\Program Files\\Anaconda\\envs\\itt\\python.exe"
            }
        }
        ```

### Troubles

* GStreamer problems under linux in conda environments:
    - for some unknown reason the gstreamer lib that comes with conda misses a lot of decoder plugins and therefore can't play all the videos. I don't know how to add them yet but here is a workaround:
        + start ITT with the environment variable set to the *system's* gstreamer plugins folder: `GST_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/gstreamer-1.0/`
        + e.g., start the program with `GST_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/gstreamer-1.0/ python ITT.py -p test`
    - For a more permanent solution, make conda set the environment variable when we activate it. For that, we need to create two files; one that sets the variable and one set unsets it afterwards.
        1. create a file: `${PREFIX_ENV}/etc/conda/activate.d/env_vars.sh`, where `${PREFIX_ENV}` is the path to the `itt` environment (e.g. `~/anaconda2/envs/itt`). You may need to create the directories first. Put the following content in the file (path according to your system):

            ```sh
            #!/bin/sh
            export GST_PLUGIN_PATH="/usr/lib/x86_64-linux-gnu/gstreamer-1.0/"
            ```
        2. create a file `${PREFIX_ENV}/etc/conda/deactivate.d/env_vars.sh` for unsetting the variable on deactivating the environment, with the following content (path according to your system):

            ```sh
            #!/bin/sh
            unset GST_PLUGIN_PATH
            ```


### Usage

Start the application from Sublime Text by building it (`CTRL + b`) or from command line `$ python ITT.py`.

The first time you start the application, you will be asked to create a new project (for development purposes, the application loads the project called 'PRSCA' at startup if it exists).

You can pass several command line parameters to load a specific project, a specific configuration file and initial labels.

- `-p <project>`: Load a specific project at startup. `<project>` is the name of the project folder located within `ITT/projects`.
- `-c <config-file>`: Load a specific configuration file which sets a range of parameters such as the sampling algorithm and its parameters. Check the `ITT/config` directory for examples.
- `-l <initial_labels>`: Specify a directory with labels if you want to load a set of labels at the start of any experiment. If you don't specify initial labels, the user will be asked to provide one example for every behavior category.
- `-v`: enable more verbose output on the command line

#### User study mode (Experiments)
There is a stripped down version that we used for our user study. This version doesn't allow to alter the project configuration. It has a few extras that were handy for the experiments such as a pause-dialogue that pops up every N labeling iterations and an end-dialogue telling the user that the experiment is over. The dialogues can be configured in the experiment's config file. The experiments version also automatically creates backups of the results once the experiment is finished (stored in `<project_folder>/backups`).

To run user experiments, you first need to setup the project using the normal `ITT.py`. Then start `$ python ITT_UserExp.py -p <projectname>` to load the created project.

#### Creating a new project
Fill in the three fields in the *New project* dialog.
* Name: give it any project name
* Storage location: default is under the app directory `projects`
* Dataset: select the folder that contains the dataset you want to work with. This folder needs to conform with my dataset file structure and contain the metadata files `metadata.json` and `dinfo.json`.

##### Dataset structure
Datasets need to be accompanied by two files `metadata.json` and `dinfo.json` that contain meta-information about the dataset, its properties and the file structure. Below are two examples.

```json
# metadata.json
{
	"dataset_name": "RatSI",
	"n_animals": 2,
	"fps":25,
	"has_multi_subject_annotations": false,
	"subjects": ["subject_white", "subject_marked"],
	"index_type": "frame",
	"default_feature_file_suffix": "_thesis",
	"default_annotation_file_suffix": "_dau",

	"frame_annotations_folder":"annotations",
	"frame_annotation_file_pattern":"{0}{2}.csv",       # 0 = video name, 1 = subject name (if has_multi_subject_annotations == True), 2 = optional suffix
	"features_folder":"features",
	"features_file_pattern":"{0}{2}.h5",                # 0 = video name, 1 = subject name (if has_multi_subject_annotations == True), 2 = optional suffix

	"labels":
	[
		"Allogrooming",
		"Approaching",
		"Following",
		"Moving away",
		"Nape attacking",
		"Other",
		"Pinning",
		"Social Nose Contact",
		"Solitary",
		"Uncertain"
	]
}
```

```json
# dinfo.json
{
	"video_names" :
	[
		"Observation01",
		"Observation02",
		"Observation04",
		"Observation07",
		"Observation10",
		"Observation11",
		"Observation14"
	],
	"video_sources" :
	{
		"Observation01":"E:\\Datasets\\RatSI\\videos\\Observation01.mp4",
		"Observation02":"E:\\Datasets\\RatSI\\videos\\Observation02.mp4",
		"Observation04":"E:\\Datasets\\RatSI\\videos\\Observation03.mp4",
		"Observation07":"E:\\Datasets\\RatSI\\videos\\Observation04.mp4",
		"Observation10":"E:\\Datasets\\RatSI\\videos\\Observation05.mp4",
		"Observation11":"E:\\Datasets\\RatSI\\videos\\Observation06.mp4",
		"Observation14":"E:\\Datasets\\RatSI\\videos\\Observation07.mp4"
	}
}
```

#### Setting up your new project
With a new project there are a few things to set up. First open the project properties under `Project -> Properties...` and specify the label set. The label set is a complete list of all actions/behaviors you want the tool to learn.

Then head to `Preferences -> Detection model` and select the type and parameters of the detection model you would like to use.

#### Training the model
There are two options:
- labeling segments one-by-one --> Click the `Label` button to trigger a video query
- labeling segments in a batch --> Enable `Training protocol` and set the desired protocol settings. If you now click `Label`, the program will query you to label segments until it reaches some satisfactory quality. It will also stop after the maximum number of iterations. If you are using *automatic* labeling and want run learning experiments with several repetitions, set the `Experiment repetitions` to a value > 1. After the training protocol finishes, the program restarts the process and follows the protocol from scratch. The results of each repetition are stored in the `backups` folder of your project.

#### Exporting results
Under `Project --> Export` you can save the current labels, predictions and the detection model to your hard drive (default location: `exports` in your project folder).

#### Analyzing learning experiments
Once you have performed a number of learning experiments and saved the results in backup files, you can accumulate them for analysis. We will create a meta-file that serves as a pointer to the actual results. This meta-file makes it easy to include new results in the analysis or exclude old ones.

1. Create a new file `results_<some name>.yaml` in the `projects` folder.
2. Copy the following yaml-template to the empty file:

    ```YAML
    label_set:
      ["Approaching", "Contact", "Following", "Moving away", "Solitary"]
    data:
      PRSCA:
        ClusterLearner:
          name: "CL: k-Means"
          path: "PRSCA/experiment_results/ClusterLearner"
          only: []
          exclude: []

        ClusterSegmentLearner:
          name: "CSL: k-Means"
          path: "PRSCA/experiment_results/ClusterSegmentLearner"
          only: []
          exclude: []
    ```
3. The fields should be relatively self-explanatory. The `label_set` determines the names of the labels. Under `data` you'll find a hierarchical structure with three levels
    1. Dataset (example: PRSCA),
    2. Learning method (example: ClusterLearner) and
    3. Result data parameters.

   Both dataset and learning methods can be arbitrary names; they only serve as identifiers. Note that the dataset name may be used in plot legends. The results data parameters are as follows:
    1. `name`: Human readable name of this method (will be used in plot legends),
    2. `path`: (relative) path to the folder with the results (this is a folder with several sub-folders named by a date-time string, e.g., "20161018131310"). For example, the projects `backups` folder is such as folder.
    3. `only`: a list of sub-folder names to include in the analysis (if empty, include all sub-folders)
    4. `exclude`: a list of sub-folder names to exclude from the analysis

   Of course, you can add more datasets and methods if you want.
4. Open one of the analysis notebooks in `tools` directory. E.g. `performance_monitor.ipynb`, `evaluation_test.ipynb` or `query_monitor.ipynb`. Select the just created `results_<some name>.yaml` and execute the remaining plotting cells to visualize the results. The notebooks are currently a bit of a mess, but they should contain everything your need to build your own visualization notebook.
