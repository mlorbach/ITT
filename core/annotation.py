# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-14 16:53:15
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-05 14:15:07

import logging
import numpy as np
import pandas as pd
from MLUtilities.annotation.utils import binarize_mutual_exclusive

logger = logging.getLogger(__name__)


class Annotation(object):
    """docstring for Annotation"""

    INACTIVE = 0
    ACTIVE = 1
    UNLABELED = 2
    UNCERTAIN = 3

    LabelState = (
        (INACTIVE, 'Inactive'),
        (ACTIVE, 'Active'),
        (UNLABELED, 'Unlabeled'),
        (UNCERTAIN, 'Uncertain'),
    )

    @property
    def video_names(self):
        return sorted(self.df.index.get_level_values('video').unique())

    def __init__(self, label_set=[]):
        super(Annotation, self).__init__()
        self.df = pd.DataFrame()
        self.is_mutually_exclusive = False
        self.label_set = label_set

    def clear(self):
        self.df.loc[:, :] = self.UNLABELED

    def is_unlabeled(self):
        return (self.df == self.UNLABELED).all().all()

    def has(self, state):
        return (self.df == state).any().any()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # INSTANTIATION
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @classmethod
    def from_dataframe(cls, dfa,
                       is_mutually_exclusive=False,
                       label_col='action', unlabeled_labels=['Other'],
                       uncertain_labels=['Uncertain']):
        annotation = cls()

        if is_mutually_exclusive:
            annotation.df = binarize_mutual_exclusive(dfa, label_col=label_col)
        else:
            annotation.df = dfa

        annotation.df = annotation.df.sort_index(axis=1)

        unlabeled_labels = [c for c in unlabeled_labels if c in annotation.df.columns]
        uncertain_labels = [c for c in uncertain_labels if c in annotation.df.columns]

        # drop other and uncertain columns
        if len(unlabeled_labels) > 0:
            annotation.df.loc[(annotation.df.loc[:, unlabeled_labels] == cls.ACTIVE).any(axis=1), :] = cls.UNLABELED
        if len(uncertain_labels) > 0:
            annotation.df.loc[(annotation.df.loc[:, uncertain_labels] == cls.ACTIVE).any(axis=1), :] = cls.UNCERTAIN
        annotation.df = annotation.df.drop(unlabeled_labels + uncertain_labels,
                                           axis=1, errors='ignore')

        annotation.label_set = annotation.df.columns.tolist()

        # todo: convert time index to frame index if needed
        return annotation

    @classmethod
    def from_dataframe_index(cls, index, label_set=[]):
        annotation = cls(label_set)
        annotation.df = annotation.df.reindex(index)
        annotation.clear()
        return annotation

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # LABEL SET
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def modify_label_set(self, label_set):
        """
        Change the label set of the annotations to a new set.

        New labels will be added to the dataframe, removed ones will be removed, what stays stays.

        Note that by changing the label set, the order of labels (and therefore the numeric label represenation) may change.

        Parameters
        ----------
        label_set : list-like
            New label set
        """
        label_set = set(label_set)
        current_set = set(self.label_set)

        to_add = label_set - current_set
        to_remove = current_set - label_set

        self._add_label_columns(to_add)
        self._remove_label_columns(to_remove)
        self.label_set = sorted(list(label_set))

        if len(to_add) > 0 or len(to_remove) > 0:
            return True
        else:
            return False

    def rename_label_set(self, label_names):
        """
        Rename the names in the current label set.

        Note that renaming may change the order of the labels as the DataFrame columns are always sorted alphabetically.

        Parameters
        ----------
        label_names : list-like
            New label names

        Raises
        ------
        AttributeError
            If more or less new names are provided than we have labels.
        """
        if len(self.label_set) != len(label_names):
            raise AttributeError('label_names (len={}) must have same length as current label set (len={}).'.format(len(label_names), len(self.label_set)))

        if self.df.columns.tolist() == label_names:
            # nothing to change
            return False

        self.df.columns = label_names
        self.df = self.df.sort_index(axis=1)
        self.label_set = self.df.columns.tolist()
        return True

    def _add_label_columns(self, label_set):
        """
        Add new label columns to the DataFrame.

        Parameters
        ----------
        labels : list-like
            New labels
        """
        if len(label_set) == 0:
            return
        elif len(label_set) > 1:
            empty_data = np.ones((self.df.shape[0], len(label_set)),
                                 dtype=np.int) * self.UNLABELED
            new_cols = pd.DataFrame(index=self.df.index,
                                    columns=label_set,
                                    data=empty_data)
            self.df = self.df.join(new_cols).sort_index(axis=1)
            self.label_set = self.df.columns.tolist()
        else:
            self.df[label_set[0]] = np.ones((self.df.shape[0], 1),
                                            dtype=np.int) * self.UNLABELED
            self.df = self.df.sort_index(axis=1)

    def _remove_label_columns(self, label_set):
        """
        Remove label columns from the DataFrame.

        Labels that don't exist are ignored and do not raise an exception.

        Parameters
        ----------
        labels : list-like
            Labels to remove
        """
        if len(label_set) == 0:
            return
        self.df = self.df.drop(list(label_set),
                               axis=1,
                               errors='ignore')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # ACCESSING ANNOTATIONS
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_video(self, video):
        return self.df.loc[video, :]

    def get_labeled(self):
        return self.df.loc[(self.df.isin([self.ACTIVE, self.INACTIVE])).any(axis=1), :].astype(int)

    def get_unlabeled(self):
        return self.df.loc[(self.df == self.UNLABELED).all(axis=1), :].astype(int)

    def get_positives(self):
        return self.df.loc[(self.df == self.ACTIVE).any(axis=1), :].astype(int)

    def get_labeled_mask(self):
        return self.df.isin([self.ACTIVE, self.INACTIVE]).any(axis=1).values

    def get_unlabeled_mask(self):
        return (self.df == self.UNLABELED).all(axis=1).values

    def get_label(self, video, frame):
        return self.df.loc[video, frame, :].values

    def get_active(self, video, frame):
        label = self.get_label(video, frame)
        return [c for c in self.df.columns if label[c] == self.ACTIVE]

    def get_inactive(self, video, frame):
        label = self.get_label(video, frame)
        return [c for c in self.df.columns if label[c] == self.INACTIVE]

    def get_labels_by_index(self, index):
        return self.df.reindex(index).copy()

    def get_label_sequence(self, video, start, end):
        return self.df.loc[(video, slice(start, end)), :].copy()

    def get_inverse_label_set(self, label_set):
        if not isinstance(label_set, (list, tuple, np.ndarray)):
            label_set = [label_set]
        return [l for l in self.label_set if l not in label_set]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # MODIFYING ANNOTATIONS
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def _get_empty_segment_df(self, index):
        return pd.DataFrame(index=index, columns=self.label_set, dtype=int)

    def _filter_allow_overwrite_label(self, y):
        '''
        Returns true for items in y which can be overwritten by a label.
        '''
        return np.in1d(y, np.array([Annotation.UNCERTAIN, Annotation.UNLABELED]))

    def update(self, index, y, negative=False):
        if isinstance(index, (pd.MultiIndex, pd.Index)):
            dfupdate = self._get_empty_segment_df(index)

            if negative:
                dfupdate.loc[:, y] = self.INACTIVE
                # we keep the other columns as they are as they may already contain information such as other INACTIVES
            else:
                dfupdate.loc[:, y] = self.ACTIVE
                dfupdate.loc[index, self.get_inverse_label_set(y)] = self.INACTIVE
            self.update_from_dataframe(dfupdate)
        else:
            if negative:
                self.df.loc[index, y] = self.INACTIVE
                # we keep the other columns as they are as they may already contain information such as other INACTIVES
            else:
                self.df.loc[index, y] = self.ACTIVE
                self.df.loc[index, self.get_inverse_label_set(y)] = self.INACTIVE

    def set_uncertain(self, index):
        if isinstance(index, (pd.MultiIndex, pd.Index)):
            dfupdate = self._get_empty_segment_df(index)
            dfupdate.loc[:, :] = self.UNCERTAIN
            self.update_from_dataframe(dfupdate)
        else:
            self.df.loc[index, :] = self.UNCERTAIN

    def update_from_dataframe(self, df):
        self.df.update(df, filter_func=self._filter_allow_overwrite_label)
        self.df = self.df.astype(int)  # make sure to keep labels in the right dtype; see bug: https://github.com/pandas-dev/pandas/issues/4094

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # STATISTICS
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def count_labeled(self, include_uncertain=False):
        """
        Return absolute and relative amount of labeled frames.

        Returns
        -------
        int, float
            absolute, relative
        """

        if len(self.df) == 0 or self.df.shape[0] == 0:
            return 0, 0

        if include_uncertain:
            c = (self.df != self.UNLABELED).any(axis=1).sum()
        else:
            c = ((self.df != self.UNLABELED) & (self.df != self.UNCERTAIN)).any(axis=1).sum()
        return (c, c / float(self.n_frames()))

    def label_distribution(self):
        return (self.df == self.ACTIVE).sum(axis=0)

    def n_frames(self):
        return self.df.shape[0]

    def n_labels(self):
        return len(self.label_set)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Converting annotations
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @classmethod
    def frames_to_segment_events(cls, sr):
        """
        Convert a label sequence into segment representation with start and stop events by merging equal, consecutive labels.

        Input is a one dimensional sequence, for example, one column in a multilabel annotation DataFrame.

        Parameters
        ----------
        sr : pd.Series or 1d array
            Input label sequence

        Returns
        -------
        pd.DataFrame
            With frame index and columns ['label', 'Event type']
        """

        if isinstance(sr, (list, tuple)):
            sr = pd.Series(sr)
        elif isinstance(sr, np.ndarray) and (np.squeeze(sr).ndim == 1):
            sr = pd.Series(np.squeeze(sr))

        # a new segment starts either when the label changes:
        try:
            start_indices_labels = sr.loc[(sr.diff() != 0)].index
        except TypeError:
            # it's possible that we can't directly .diff() the datatype in
            # the column ("minus" may not be defined). In that case, we'll
            # compare the two subsequent values on equality:
            start_indices_labels = sr.loc[(sr != sr.shift(1)).any(axis=1)].index

        # or when the index increases more than usually (+1 frame):
        np_mask = np.zeros_like(sr.index.values, dtype=np.bool)
        np_mask[1:] = (np.diff(sr.index.values) > 1)
        start_indices_index = sr.loc[np_mask].index

        # join the segment starts
        start_indices = start_indices_labels.union(start_indices_index)

        stop_indices_labels = start_indices_labels[1:] - 1
        stop_indices_index = sr.loc[np_mask[1:, ]].index

        stop_indices = stop_indices_labels.union(stop_indices_index)

        # add very last stop index:
        stop_indices = stop_indices.insert(len(stop_indices), sr.index[-1])

        # kick out all start/stops that are not in the original index
        start_indices = start_indices.intersection(sr.index)
        stop_indices = stop_indices.intersection(sr.index)

        dfstart = pd.DataFrame(index=start_indices, data=sr.loc[start_indices].values, columns=['label'])
        dfstart['Event type'] = 'Start event'
        dfstop = pd.DataFrame(index=stop_indices, data=sr.loc[stop_indices].values, columns=['label'])
        dfstop['Event type'] = 'Stop event'

        return dfstart.append(dfstop).sort_index()

    @classmethod
    def frames_to_segments_multilabel(cls, df, action_columns=None):
        """
        Converts a multilabel annotation DataFrame to segment representation with Start and Stop events.

        Parameters
        ----------
        df : pd.DataFrame
            Input annotations (e.g., Annotation.df)
        action_columns : None, optional
            If None, use all columns in df, otherwise specify list of column names

        Returns
        -------
        pd.DataFrame
            with columns [frame, Behavior, Event type] and numerical index (no meaning, ensures correct order of events)
        """
        if action_columns is None:
            action_columns = df.columns.tolist()
        else:
            action_columns = [c for c in action_columns if c in df.columns]

        dfs = {}
        for col in action_columns:
            dfs[col] = cls.frames_to_segment_events(df[col])

            # remove inactive and unlabeled segments:
            dfs[col] = dfs[col].loc[~dfs[col]['label'].isin([cls.INACTIVE, cls.UNLABELED])]

            if len(dfs[col]) == 0:
                del dfs[col]

        if len(dfs) == 0:
            return pd.DataFrame()

        # merge segments per action class to one dataframe
        dfs = pd.concat(dfs, names=['Behavior', 'frame']).reset_index(drop=False).sort_values(by=['frame', 'Event type']).reset_index(drop=True)

        # handle uncertain segments --> exported as its own class
        dfs.loc[dfs['label'] == cls.UNCERTAIN, 'Behavior'] = cls.LabelState[cls.UNCERTAIN][1]
        dfs = dfs.drop_duplicates().drop('label', axis=1).reset_index(drop=True)

        return dfs
