# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 09:46:01
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-05 14:21:00

import os
import json
import logging
import pandas as pd
import numpy as np

from MLUtilities.dataset.io import load_features

from annotation import Annotation

logger = logging.getLogger(__name__)


class Dataset(object):

    @property
    def has_ground_truth(self):
        return self.gt_annotation is not None

    """docstring for Dataset"""
    def __init__(self, name=None, metadata={}):
        super(Dataset, self).__init__()
        self.name = name
        self.metadata = metadata
        self.videos = None
        self.annotation = Annotation()
        self.gt_annotation = None
        self._frame_index = None
        self.default_subject = None
        self.prediction = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # CREATION
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @classmethod
    def from_json(cls, metadata_file, dinfo_file=None):

        basepath = os.path.dirname(metadata_file)
        init_name = os.path.basename(basepath)
        dataset = cls(name=init_name)

        try:
            # load metadata and dataset info
            with open(metadata_file) as fp:
                dataset.metadata = json.load(fp)
        except IOError:
            raise RuntimeError('Invalid file "{}". Cannot load dataset metadata.'.format(metadata_file))

        if dinfo_file is not None:
            try:
                with open(dinfo_file) as fp:
                    _dinfo = json.load(fp)
            except IOError:
                raise RuntimeError('Invalid file "{}". Cannot load dataset information.'.format(dinfo_file))

        dataset.metadata.update(_dinfo)
        dataset.metadata['basepath'] = basepath
        if 'default_feature_file_suffix' not in dataset.metadata:
            dataset.metadata['default_feature_file_suffix'] = ''
        if 'default_annotation_file_suffix' not in dataset.metadata:
            dataset.metadata['default_annotation_file_suffix'] = ''
        if 'subject_default' in dataset.metadata:
            dataset.default_subject = dataset.metadata['subject_default']
        dataset._extract_video_sources_from_metadata()
        dataset._init_video_information()
        dataset.create_annotation()  # empty annotation object for labeling
        dataset.load_gt_annoation()  # ground truth for evaluation if available
        return dataset

    def __getstate__(self):
        odict = self.__dict__.copy()
        del odict['prediction']
        return odict

    def __setstate__(self, state):
        state['prediction'] = None
        self.__dict__.update(state)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # RESET STATE
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def clear_annotations(self):
        """
        Clear all present annotations (set all items to UNLABELED).

        Also empties the present predictions.
        """
        self.annotation.clear()
        self._frame_index = None
        self.prediction = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # VIDEOS
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_video_names(self):
        if self.videos is None:
            return []
        else:
            return sorted(self.videos.index.unique())

    def _extract_video_sources_from_metadata(self):

        if 'video_names' in self.metadata and 'video_sources' in self.metadata:

            videos = pd.DataFrame.from_dict(self.metadata['video_sources'], orient='index').sort_index()
            videos.columns = ['file']
            videos.index.name = 'name'

            # keep only the marked videos
            videos = videos.loc[self.metadata['video_names'], :]

            if self.videos is None:
                self.videos = videos
            else:
                self.videos = self.videos.concat(videos)

    def _init_video_information(self):
        if self.videos is None:
            raise RuntimeError('Need to know video sources before calling _init_video_information()')

        dff = self.get_features(keep_invalid=True)

        def first_valid_feature_vector(df, index_level=slice(None)):
            return df.apply(pd.Series.first_valid_index).max()[index_level]
        def last_valid_feature_vector(df, index_level=slice(None)):
            return df.apply(pd.Series.last_valid_index).min()[index_level]

        first_indices = dff.groupby(level='video').apply(first_valid_feature_vector, index_level=1)

        last_indices = dff.groupby(level='video').apply(last_valid_feature_vector, index_level=1)

        self.videos['first_valid_frame'] = first_indices.astype(int)
        self.videos['last_valid_frame'] = last_indices.astype(int)
        self.videos['n_valid_frames'] = (self.videos['last_valid_frame'] -
                                         self.videos['first_valid_frame']) + 1
        self.videos['valid_duration_sec'] = np.round(self.videos['n_valid_frames'] / float(self.metadata['fps']), decimals=2)

        self.videos['fps'] = float(self.metadata['fps'])

        # self.videos['tracked'] = self.videos.notnull().all(axis=1)
        self.videos = self.videos.fillna(0)

    def get_frame_index(self, force_update=False):

        if force_update or self._frame_index is None:
            idx_dict = {}
            for video in self.get_video_names():
                idx_dict[video] = pd.DataFrame(
                    index=np.arange(self.videos.loc[video, 'first_valid_frame'],
                                    self.videos.loc[video, 'last_valid_frame'] + 1,
                                    dtype=np.int))
            self._frame_index = pd.concat(idx_dict,
                                          names=['video', 'frame']).index
        return self._frame_index

    def get_valid_segment_tuple(self, video, duration, start=None, mid=None, end=None, ensure_duration=False):

        given = [start is not None, mid is not None, end is not None]
        if sum(given) > 1:
            raise ValueError('Can only specify one of start, mid or end.')

        if sum(given) == 0:
            raise ValueError('Must specifiy one of start, mid or end.')

        first_video_frame = self.videos.loc[video, 'first_valid_frame']
        last_video_frame = self.videos.loc[video, 'last_valid_frame']

        # make duration integer
        duration = int(np.round(duration, 0))

        if start is not None:
            start = int(np.round(start, 0))

            if ensure_duration:
                # shift start to first valid, then determine end
                start = max(start, first_video_frame)
                end = min(start + duration - 1, last_video_frame)
            else:
                # determine end, then shift start to first valid
                end = min(start + duration - 1, last_video_frame)
                start = max(start, first_video_frame)

        elif mid is not None:
            mid = int(np.round(mid, 0))
            before = int((duration - 1) / 2)
            after = int(duration / 2)

            if ensure_duration and (mid + after > last_video_frame):
                # shift end, then determine start
                end = min(mid + after, last_video_frame)
                start = max(end - duration + 1, first_video_frame)
            elif ensure_duration and (mid - before < first_video_frame):
                # shift start, then determine end
                start = max(mid - before, first_video_frame)
                end = min(start + duration - 1, last_video_frame)
            else:
                # determine start and end
                start = max(mid - before, first_video_frame)
                end = min(mid + after, last_video_frame)

        elif end is not None:
            end = int(np.round(end, 0))

            if ensure_duration:
                # shift end, then determine start
                end = min(end, last_video_frame)
                start = max(end - duration + 1, first_video_frame)
            else:
                # determine start, then shift end
                start = max(end - duration + 1, first_video_frame)
                end = min(end, last_video_frame)

        return (video, start, end)


    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # ANNOTATIONS
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def load_gt_annoation(self):
        gt_annotation_file = os.path.join(self.metadata['basepath'], self.metadata['frame_annotations_folder'], 'iTT_gt_annotations{}.h5'.format(self.metadata['default_annotation_file_suffix']))

        if os.path.isfile(gt_annotation_file):
            df = pd.read_hdf(gt_annotation_file, 'annotations')

            # drop videos that are not in the metadata
            df = df.loc[self.metadata['video_names'], :]

            self.gt_annotation = Annotation.from_dataframe(df, is_mutually_exclusive=(len(df.columns) == 1))
            logger.info('Loaded ground truth annotations for evaluation from: {}'.format(gt_annotation_file))

    def create_annotation(self, labels=[]):
        self.annotation = Annotation.from_dataframe_index(self.get_frame_index(), labels)

    def create_annotation_from_dataframe(self, df):
        """
        Import annotation dataframe. For possibilities to load such a dataframe from files, see MLUtilities.annotation module.

        Parameters
        ----------
        df : pd.DataFrame
            Annotation dataframe
        """
        is_mutually_exclusive = len(df.columns) == 1
        self.annotation = Annotation.from_dataframe(df,
                                                    is_mutually_exclusive=is_mutually_exclusive)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # FEATURES
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def set_default_feature_file_suffix(self, suffix):
        self.metadata['default_feature_file_suffix'] = suffix

    def get_features(self, videos=None, subject=None, keep_invalid=False):
        """
        Return feature set.

        Careful: these values are not cached but read from disk at every call!

        Parameters
        ----------
        videos : list of str, optional
            Video names; if None, load all videos
        subject : str, optional
            Subject name; if None, load default subject
        keep_invalid : bool, optional
            Drop or keep NaN rows; default = False: drop NaN rows

        Returns
        -------
        pd.DataFrame
            Feature set with same Index as annotations
        """
        if subject is None:
            subject = self.default_subject

        dff = load_features(self.metadata,
                            video_filter=videos,
                            subject=subject,
                            file_suffix=self.metadata['default_feature_file_suffix'] if 'default_feature_file_suffix'in self.metadata else '')

        logger.debug('Features raw: {}, NAs: {}'.format(dff.shape, dff.isnull().any(axis=1).sum()))

        if not keep_invalid:
            dff = dff.reindex(self.get_frame_index(), copy=False)

        logger.debug('Features (keep_invalid={}): {}, NAs: {}'.format(keep_invalid, dff.shape, dff.isnull().any(axis=1).sum()))

        # interpolate per video
        dff = dff.groupby(level='video').apply(pd.DataFrame.interpolate)

        logger.debug('Features after interpolation: {}, NAs: {}'.format(dff.shape, dff.isnull().any(axis=1).sum()))

        if not keep_invalid:
            dff = dff.dropna()

        logger.debug('Features returned: {}, NAs: {}'.format(dff.shape, dff.isnull().any(axis=1).sum()))

        return dff
