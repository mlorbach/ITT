# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-01-03 10:43:12
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-03 11:47:18

from datetime import datetime


class Experiment(object):
    """docstring for Experiment"""
    def __init__(self, project_name, participant_name, comment='', date=None):
        super(Experiment, self).__init__()
        self.date = date
        self.project_name = project_name
        self.participant_name = participant_name
        self.comment = comment

        if self.date is None:
            self.date = datetime.now().strftime('%Y-%m-%d %H:%M')
