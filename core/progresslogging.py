# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-04 17:29:39
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:52:32

import os
from shutil import rmtree
import logging
from time import time
from numpy import nan, save, uint8, ndarray
from pandas import DataFrame, Series
from MLUtilities.io_custom import mkdir_p

from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import QThread

from core.project_ctrl import ProjectController
from tools.export import export_sklearn_model
from ml.oracle import QueryItem, QueryResult
from ml.metrics import class_average, frame_average
from core.utils import now, now_iso, np2csv_string

# LOGGING
_LOGFORMAT = '%(asctime)-9s %(levelname)-6s %(name)s: %(message)s'
_LOGDATEFORMAT = '%Y-%m-%d %H:%M:%S'

logger = logging.getLogger(__name__)


class ProgressLogger(QObject):
    """docstring for ProgressLogger"""
    def __init__(self, project_ctrl, output_dir='', catch_app_log=True, filter_learner_state=10, clear_existing=False):
        super(ProgressLogger, self).__init__()
        self.project_ctrl = project_ctrl
        self.output_dir = os.path.normpath(output_dir)
        self.catch_app_log = catch_app_log
        self.filter_learner_state = filter_learner_state
        self._count_learner_state = -1

        # setup paths, files and handlers:
        self._init_files(clear_existing)

    def _init_files(self, clear_existing=False):

        # create output directory for log files
        if not os.path.isdir(self.output_dir):
            logger.debug('Create logging directory: {}'.format(self.output_dir))
            try:
                mkdir_p(self.output_dir)
            except IOError as e:
                logger.error("Could not create logging output directory: {}.".format(self.output_dir), exc_info=True)
                raise e

        # init data structure for multiple loggers
        self._logs = {}
        for log in ['queries', 'responses', 'model', 'suggestions']:
            self._logs[log] = {}
            self._logs[log]['path'] = os.path.join(self.output_dir, '{}.csv'.format(log))

        # create flex-type logs
        self._flexlogs = {}
        for log in ['learner', 'models']:
            self._flexlogs[log] = {}
            self._flexlogs[log]['path'] = os.path.join(self.output_dir, log)
            self._flexlogs[log]['filepattern'] = os.path.join(self._flexlogs[log]['path'], '{}.{}')

            try:
                mkdir_p(self._flexlogs[log]['path'])
            except IOError as e:
                logger.error("Could not create output directory: {}.".format(self._flexlogs[log]['path']), exc_info=True)
                raise e
        if clear_existing:
            self._delete_flex_logs()
        self._count_learner_state = -1

        # create and open files for loggers
        for log in self._logs.keys():
            if os.path.isfile(self._logs[log]['path']) and not clear_existing:
                logger.debug('Appending log {}'.format(self._logs[log]['path']))
                mode = 'ab'
            else:
                logger.debug('Create new log {}'.format(self._logs[log]['path']))
                mode = 'wb'
            self._logs[log]['fh'] = open(self._logs[log]['path'], mode=mode, buffering=0)  # buffering=0 means; write immediately to file without buffering

        # setup application logger, listening to the app's internal info log
        if self.catch_app_log:
            self.applogger = logging.FileHandler(os.path.join(self.output_dir, 'session.log'), mode='w' if clear_existing else 'a')
            self.applogger.setLevel(logging.DEBUG)
            self.applogger.setFormatter(logging.Formatter(_LOGFORMAT, _LOGDATEFORMAT))
            logging.getLogger().addHandler(self.applogger)
        else:
            self.applogger = None

    def get_log_file_path(self, name):
        """
        Get path to log file of the specified logger.

        Parameters
        ----------
        name : str
            Name of the logger {queries, responses, model, session}

        Returns
        -------
        str
            Absolute file path to log file

        Raises
        ------
        KeyError
            If no log with this name exists
        """
        if name in self._logs:
            return self._logs[name]['path']
        else:
            raise KeyError('No log named "{}" found.'.format(name))

    def get_log_directory(self):
        return self.output_dir

    def close_all_logs(self):
        try:
            for log in self._logs.keys():
                self._logs[log]['fh'].close()
        except IOError:
            logger.error('Failed to close log handler.', exc_info=True)
        finally:
            if self.applogger is not None:
                self.applogger.flush()
                self.applogger.close()
                logging.getLogger().removeHandler(self.applogger)
                # self.applogger = None

    def delete_logs(self):
        self.close_all_logs()

        for name, log in self._logs.viewitems():
            if os.path.isfile(log['path']):
                os.unlink(log['path'])

        self._delete_flex_logs()
        self._delete_other_dirs()

        if self.applogger is not None:
            if os.path.isfile(self.applogger.baseFilename):
                os.unlink(self.applogger.baseFilename)

    def _delete_flex_logs(self):
        for name, log in self._flexlogs.viewitems():
            if os.path.isdir(log['path']):
                rmtree(log['path'])
                mkdir_p(log['path'])

    def _delete_other_dirs(self):
        for name in ['initial_labels']:
            dpath = os.path.join(self.get_log_directory(), name)
            if os.path.isdir(dpath):
                rmtree(dpath)

    @pyqtSlot(list)
    def on_query_suggested(self, data):
        n = len(data)
        pattern = '{:s};' + ';'.join(['{}'] * n) + '\n'
        fh = self._logs['suggestions']['fh']
        fh.write(pattern.format(now_iso(),
                                *data))

    @pyqtSlot(QueryItem)
    def on_queried_item(self, query):
        # only log label-kind queries
        if query.kind == 'sample':
            return
        fh = self._logs['queries']['fh']
        if query.kind == 'binary':
            # query response is of type 'weak'
            fh.write('{:s};{};{:s};{:d};{:d};{:s};{:.6f}\n'.format(
                    now_iso(),
                    query.identifier,
                    query.value.min()[0],
                    query.value.min()[1],
                    query.value.max()[1],
                    query.parameter[0],
                    query.parameter[1]))
        elif query.parameter is None:
            # query response is of type 'strong_unordered'
            fh.write('{:s};{};{:s};{:d};{:d}\n'.format(
                    now_iso(),
                    query.identifier,
                    query.value.min()[0],
                    query.value.min()[1],
                    query.value.max()[1]))
        else:
            # query response is of type 'strong_ranked'
            fh.write('{:s};{};{:s};{:d};{:d};{}\n'.format(
                    now_iso(),
                    query.identifier,
                    query.value.min()[0],
                    query.value.min()[1],
                    query.value.max()[1],
                    np2csv_string(query.parameter)))

    @pyqtSlot(QueryResult)
    def on_query_result(self, result):
        try:
            # the identifier is a timestamp, compute the latency of the response
            latency = time() - result.query.identifier
        except (KeyError, TypeError):
            latency = nan
            logger.warning('Missing timing information for query: {}'.format(result.query.identifier))

        fh = self._logs['responses']['fh']
        fh.write('{:s};{};{};{};{:.3f}\n'.format(
                    now_iso(),
                    result.query.identifier,
                    QueryResult.ResponseState[result.state][1],
                    result.response_value,
                    latency))

    @pyqtSlot()
    def on_update_detection_model(self):
        fh = self._logs['model']['fh']

        n_queries = self.project_ctrl.get_project().learning_stats.n_query_iterations

        n_queries_labeled = self.project_ctrl.get_project().learning_stats.query_response_stats[QueryResult.LABELED]

        secs_labeled, _ = self.project_ctrl.get_annotation().count_labeled()
        secs_labeled /= float(self.project_ctrl.get_metadata()['fps'])

        label_dist = self.project_ctrl.get_annotation().label_distribution().values

        dfscores = self.project_ctrl.get_accuracies()
        dftestscores = self.project_ctrl.get_test_accuracies()
        class_avg = class_average(dfscores)
        frame_avg = frame_average(dfscores)
        class_avg_test = class_average(dftestscores)
        frame_avg_test = frame_average(dftestscores)

        fh.write('{:s};{:d};{:d};{:.3f};{};{:f};{:f};{:f};{:f};{:f};{:f};{};{};{};{:f};{:f};{:f};{:f};{:f};{:f};{};{};{}\n'.format(
                    now_iso(),
                    n_queries,
                    n_queries_labeled,
                    secs_labeled,
                    np2csv_string(label_dist),
                    frame_avg[0],
                    frame_avg[1],
                    frame_avg[2],
                    class_avg[0],
                    class_avg[1],
                    class_avg[2],
                    np2csv_string(dfscores.loc[:, 'Precision'].values),
                    np2csv_string(dfscores.loc[:, 'Recall'].values),
                    np2csv_string(dfscores.loc[:, 'F1'].values),
                    frame_avg_test[0],
                    frame_avg_test[1],
                    frame_avg_test[2],
                    class_avg_test[0],
                    class_avg_test[1],
                    class_avg_test[2],
                    np2csv_string(dftestscores.loc[:, 'Precision'].values),
                    np2csv_string(dftestscores.loc[:, 'Recall'].values),
                    np2csv_string(dftestscores.loc[:, 'F1'].values)))

        if 'models' in self._flexlogs:

            filename = self._flexlogs['models']['filepattern'].format(now(),
                                                                      'gz')
            export_sklearn_model(self.project_ctrl.get_detection_model().model,
                                 filename)

    def _determine_extension_from_type(self, a):
        if isinstance(a, ndarray):
            return 'np'
        if isinstance(a, (DataFrame, Series)):
            return 'h5'
        return 'csv'

    def _write(self, a, filename):
        if isinstance(a, ndarray):
            with open(filename, 'wb') as fh:
                save(fh, a)
        elif isinstance(a, (DataFrame, Series)):
            a.to_hdf(filename, 'data', mode='w')
        else:
            with open(filename, 'wb') as fh:
                fh.write(a)

    @pyqtSlot(object)
    def on_learner_update(self, state):

        self._count_learner_state += 1
        if self._count_learner_state > 0 and self._count_learner_state < self.filter_learner_state:
            return

        if state is None:
            logger.warning('Cannot log learner state. State is None.')
            return

        if 'learner' in self._flexlogs:

            extension = self._determine_extension_from_type(state)
            filename = self._flexlogs['learner']['filepattern'].format(now(), extension)

            try:
                self._write(state, filename)
            except IOError:
                logger.error('Failed to log learner state.', exc_info=True)

            self._count_learner_state = 0
