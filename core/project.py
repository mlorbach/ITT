# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-14 13:00:02
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-12-19 18:24:19

import os
import logging
import pandas as pd

from core.dataset import Dataset
from ml.detection_model import DetectionModel
from ml.feature_processor import FeatureProcessor
from ml.oracle import QueryResult

logger = logging.getLogger(__name__)


class Project(object):
    """
    Model class of a learning project.

    Attributes
    ----------
    name : str
        Project name (id)
    save_filename : str
        Full path to filename where we would (by default) save this project.
    title : str
        A human readable title for this project.
    """
    def __init__(self, name, dataset_folder=None, storage_location=''):
        super(Project, self).__init__()
        self.name = name
        self.dataset = Dataset('empty')
        self.testset = None
        self.detection_model = DetectionModel()
        self.feature_processor = FeatureProcessor()
        self.learning_stats = LearningStatistics()
        self._storage_location = os.path.normpath(storage_location)
        self._local_project_file = None

        if dataset_folder is not None:
            self.dataset = self.propagate_dataset_info(dataset_folder)

            try:
                self.testset = self.propagate_dataset_info(dataset_folder,
                                                           dinfo_suffix='_test')
            except IOError:
                # okay, no test set then
                logger.info('No test set definition found. Not initializing test set.')

    def propagate_dataset_info(self, dataset_folder, dinfo_suffix=''):

        if not os.path.isdir(dataset_folder):
            raise IOError("Couldn't find dataset folder: {}.".format(dataset_folder))

        _metadata_file = os.path.join(dataset_folder, 'metadata.json')
        _dinfo_file = os.path.join(dataset_folder, 'dinfo{}.json'.format(dinfo_suffix))

        if not os.path.isfile(_dinfo_file):
            raise IOError('Could not find dataset info: {}.'.format(_dinfo_file))

        return Dataset.from_json(_metadata_file, _dinfo_file)

    def clear_dataset_info(self):
        self.dataset = None
        self.testset = None


class LearningStatistics(object):
    """
    Class to keep track of the learning progress in compressed form (e.g. accuracy averages across classes/frames) as well as statistics over query responses (e.g. how often the oracle rejects queries).

    Attributes
    ----------
    accuracy_history : pd.DataFrame
        Learning progress (has column 'Iteration', other columns are arbitrary)
    n_query_iterations : int
        Current iteration counter
    query_response_stats : dict
        Counters for arbitrary response types
    """
    @property
    def current_iteration(self):
        return self.n_query_iterations

    def __init__(self):
        super(LearningStatistics, self).__init__()
        self.n_query_iterations = 0
        self.query_response_stats = {}
        self.accuracy_history = pd.DataFrame(columns=['Iteration'])
        self.reset_learning_stats()

    def incr_iterations(self):
        self.n_query_iterations += 1

    def incr_response(self, response_state):
        if response_state not in self.query_response_stats:
            self.query_response_stats[response_state] = 0
        self.query_response_stats[response_state] += 1

    def add_accuracy_history_item(self, iteration, **kwargs):
        """
        Adds a row to the accuracy history or updates the values of an iteration if there is already such a row.

        Parameters
        ----------
        iteration : int
            Iteration number at which to add/update history item
        **kwargs : dict
            key is history type, value is value to store
        """

        # nothing to do without data
        if len(kwargs) == 0:
            return

        # create dataframe from arguments
        newitem = pd.DataFrame(kwargs, index=[0])

        # don't add this item if there is no data
        if len(newitem) == 0:
            return

        # store new data
        if iteration in self.accuracy_history['Iteration'].values:
            # update existing values:

            # add new columns if there are any:
            for c in newitem.columns:
                if c not in self.accuracy_history.columns:
                    self.accuracy_history[c] = 0  # default value is 0

            # update items with new values
            self.accuracy_history.loc[self.accuracy_history['Iteration'] == iteration, newitem.columns.tolist()] = newitem.values
        else:
            # create new row with default value 0 (for columns not given)
            newitem['Iteration'] = iteration
            self.accuracy_history = self.accuracy_history.append(newitem, ignore_index=True).fillna(0)

    def reset_learning_stats(self):
        self.n_query_iterations = 0
        self.query_response_stats = {}
        for state, name in QueryResult.ResponseState:
            self.query_response_stats[state] = 0
        self.accuracy_history = pd.DataFrame(columns=['Iteration'])

