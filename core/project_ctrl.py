# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-20 17:45:05
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-21 17:25:13

import os
import logging
import time
from copy import copy
from core.project import Project
from core.annotation import Annotation
from PyQt5.QtCore import pyqtSignal, QObject, QMutex, QThread
import pandas as pd
import numpy as np

from ml.metrics import class_average
from ml.oracle import QueryResult
from ml.feature_processor import FeatureProcessor

from sklearn.metrics import precision_recall_fscore_support
from MLUtilities.annotation.utils import binarize_mutual_exclusive
from MLUtilities.semi_supervised.utils import precision_recall_fscore_support_semi_supervised

logger = logging.getLogger(__name__)
mutex_predcache = QMutex()
mutex_tpredcache = QMutex()


class ProjectController(QObject):
    """
    Controller class handling access to and modification of all learning project related data (project metadata, dataset, annotations, statistics, ...).

    Attributes
    ----------
    changed : pyqtSignal
        Emitted if anything changes
    project : core.Project
        Underlying project instance
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Signals and properties
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    changed = pyqtSignal(str)

    @property
    def name(self):
        return self.project.name

    @name.setter
    def name(self, new_name):
        if self.project.name != new_name:
            self.project.name = new_name
            self.emit_changed('name')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Initialization
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def __init__(self, project):
        super(ProjectController, self).__init__()
        self.project = project
        self._use_feature_cache = True
        self._cached_test_features = None
        self._cached_features = None
        self._cached_dfscores = None
        self._cached_testscores = None

    def emit_changed(self, what=''):
        logger.debug('Emit: {}'.format(what))
        self.changed.emit(what)

    def set_model(self, project):
        self.project = project
        self.emit_changed('project')

    def _reset_cache(self):
        logger.debug('Reset pred and score cache')
        self._cached_dfscores = None
        self.get_dataset().prediction = None
        self._cached_testscores = None
        if self.has_testset():
            self.get_testset().prediction = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Quick access to common properties
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_project(self):
        return self.project

    def get_project_file(self):
        if self.project._local_project_file is None:
            return None
        return os.path.join(self.project._storage_location,
                            self.project._local_project_file)

    def get_project_directory(self):
        return self.project._storage_location

    def get_dataset(self):
        return self.project.dataset

    def has_testset(self):
        return self.project.testset is not None

    def get_testset(self):
        return self.project.testset

    def get_annotation(self):
        return self.get_dataset().annotation

    def get_gt_annotation(self):
        if self.get_dataset().has_ground_truth:
            return self.get_dataset().gt_annotation
        else:
            return None

    def get_video_names(self):
        return self.get_dataset().get_video_names()

    def get_video_collection(self):
        return self.get_dataset().videos

    def get_label_set(self):
        return copy(self.get_dataset().annotation.label_set)

    def get_metadata(self):
        return self.get_dataset().metadata

    def get_detection_model(self):
        return self.project.detection_model

    def get_feature_processor(self):
        return self.project.feature_processor

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Features
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def _check_reset_feature_cache(self, cache, videos, subject, keep_invalid):
        """
        Check if resetting the feature cache is needed if we were to query the features with above parameters.

        Not intended for external calls. Is called by get_features().

        Parameters
        ----------
        videos : list of str
            Video names
        subject : str
            Subject name
        keep_invalid : bool
            Drop or keep NaN rows

        Returns
        -------
        bool
            True if you need call _reset_feature_cache() and load feature set from disk (again).
        """
        if cache is None:
            logger.log(15, 'Need to reset features cache because cache is not set.')
            return True

        if (cache[0] == videos and
                cache[1] == subject and
                cache[2] == keep_invalid):
            return False
        else:
            logger.log(15, 'Need to reset features cache because {}, {}, {}'.format(cache[0] == videos,
                       cache[1] == subject,
                       cache[2] == keep_invalid))
            return True

    def _reset_feature_cache(self, which):
        """
        Remove the currently cached feature set if available.
        """
        logger.log(15, "Reset {} feature cache.".format(which))

        if isinstance(which, (list, tuple)):
            if 'data' in which or 'train' in which:
                self._cached_features = None
            if 'test' in which:
                self._cached_test_features = None
        else:
            if which == 'data' or which == 'train':
                self._cached_features = None
            if which == 'test':
                self._cached_test_features = None


    def get_features(self, videos=None, subject=None, keep_invalid=False):
        """
        Return feature set.

        Returns a cached DataFrame if the parameters are the same as previously. Note that we do not check whether the DataFrame has been changed in the meantime. So make sure you work on a copy if you change anything in the returned DataFrame.

        Parameters
        ----------
        videos : list of str, optional
            Video names; if None, load all videos
        subject : str, optional
            Subject name; if None, load default subject
        keep_invalid : bool, optional
            Drop or keep NaN rows; default = False: drop NaN rows

        Returns
        -------
        pd.DataFrame
            Feature set with same Index as annotations
        """
        if subject is None:
            subject = self.get_dataset().default_subject

        if self._check_reset_feature_cache(self._cached_features, videos, subject, keep_invalid):
            self._reset_feature_cache('data')

        if not self._use_feature_cache or self._cached_features is None:

            dff = self.get_dataset().get_features(videos, subject, keep_invalid)

            if self.get_feature_processor() is not None:
                idx = dff.index
                dff_fp = self.get_feature_processor().transform(dff)

                # fill data into old dataframe or create new if dimensions changed
                if dff.shape == dff_fp.shape:
                    dff[:] = dff_fp
                else:
                    dff = pd.DataFrame(data=dff_fp, index=idx)
            else:
                logger.debug('Get dataset features: no feature processor set.')

            if self._use_feature_cache:
                # cache features
                self._cached_features = (videos, subject, keep_invalid, dff)
            else:
                return dff

        return self._cached_features[-1]

    def get_testset_features(self, videos=None, subject=None, keep_invalid=False):
        """
        Return test set feature set.

        Returns a cached DataFrame if the parameters are the same as previously. Note that we do not check whether the DataFrame has been changed in the meantime. So make sure you work on a copy if you change anything in the returned DataFrame.

        Parameters
        ----------
        videos : list of str, optional
            Video names; if None, load all videos
        subject : str, optional
            Subject name; if None, load default subject
        keep_invalid : bool, optional
            Drop or keep NaN rows; default = False: drop NaN rows

        Returns
        -------
        pd.DataFrame
            Feature set with same Index as annotations
        """

        if self.has_testset():
            dset = self.get_testset()
        else:
            dset = self.get_dataset()

        if subject is None:
            subject = dset.default_subject

        if self._check_reset_feature_cache(self._cached_test_features, videos, subject, keep_invalid):
            self._reset_feature_cache('test')

        if not self._use_feature_cache or self._cached_test_features is None:

            dff = dset.get_features(videos, subject, keep_invalid)

            if self.get_feature_processor() is not None:
                idx = dff.index
                dff_fp = self.get_feature_processor().transform(dff)

                # fill data into old dataframe or create new if dimensions changed
                if dff.shape == dff_fp.shape:
                    dff[:] = dff_fp
                else:
                    dff = pd.DataFrame(data=dff_fp, index=idx)
            else:
                logger.debug('Get testset features: no feature processor set.')

            if self._use_feature_cache:
                # cache features
                self._cached_test_features = (videos, subject, keep_invalid, dff)
            else:
                return dff

        return self._cached_test_features[-1]

    def replace_feature_processing(self, feature_processor):
        self.project.feature_processor = FeatureProcessor(feature_processor)
        self._reset_feature_cache(['data', 'test'])
        self.get_detection_model()._is_fitted = False
        self.emit_changed('feature_processing')

    def change_feature_processing_parameters(self, **kwargs):
        self.get_feature_processor().model.set_params(**kwargs)
        self._reset_feature_cache(['data', 'test'])
        self.get_detection_model()._is_fitted = False
        self.emit_changed('feature_processing')

    def fit_feature_processing(self, videos=None, subject=None):
        self.get_feature_processor().fit(self.get_dataset().get_features(videos, subject, keep_invalid=False))
        self._reset_feature_cache(['data', 'test'])
        self.emit_changed('feature_processing')


    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Prediction output
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_prediction(self, force_update=False):
        mutex_predcache.lock()
        if self.get_dataset().prediction is None or force_update:
            if self.get_detection_model().is_fitted():
                logger.info('Predicting labels of dataset...')
                tic = time.clock()
                dff = self.get_features()
                #logger.debug('min/max of dataset features: \n{}\n{}'.format(dff.min(axis=0), dff.max(axis=0)))
                y = self.get_detection_model().predict(dff.values)
                # logger.debug('a few predictions: {}'.format(y[:50]))
                self.set_prediction(pd.Series(index=dff.index,
                                              data=y,
                                              name='detection'))
                logger.log(15, 'Predicting labels of dataset... Done. {:.3f}s'.format(time.clock() - tic))
            else:
                mutex_predcache.unlock()
                return None
        mutex_predcache.unlock()
        return self.get_dataset().prediction

    def get_testset_prediction(self, force_update=False):

        if not self.has_testset():
            raise AttributeError('Project has no test set defined.')

        mutex_tpredcache.lock()
        if self.get_testset().prediction is None or force_update:
            if self.get_detection_model().is_fitted():
                logger.info('Predict labels of testset...')
                tic = time.clock()
                dff = self.get_testset_features()
                # logger.debug('min/max of testset features: \n{}\n{}'.format(dff.min(axis=0), dff.max(axis=0)))
                y = self.get_detection_model().predict(dff.values)
                logger.debug('a few test predictions: {}'.format(y[:50]))
                self.set_testset_prediction(pd.Series(index=dff.index,
                                              data=y,
                                              name='detection'))
                logger.log(15, 'Predict labels of testset... Done. {:.3f}s'.format(time.clock() - tic))
            else:
                mutex_tpredcache.unlock()
                return None
        mutex_tpredcache.unlock()
        return self.get_testset().prediction

    def set_prediction(self, dfpred):
        self._cached_dfscores = None
        self.get_dataset().prediction = dfpred
        self.emit_changed('prediction')

    def set_testset_prediction(self, dfpred):
        if not self.has_testset():
            return

        self._cached_testscores = None
        self.get_testset().prediction = dfpred
        self.emit_changed('prediction')

    def get_accuracies(self, force_update=False):

        if self._cached_dfscores is None or force_update:

            dfpred = self.get_prediction(force_update)

            if dfpred is None:
                # e.g., no detection model trained yet
                return pd.DataFrame(data=0,
                                    index=self.get_label_set(),
                                    columns=[
                                     'Precision', 'Recall', 'F1', 'Support'
                                    ])

            # all true/false labeled
            dfl = self.get_annotation().get_labeled()

            dfpred_labeled = dfpred.reindex(dfl.index).copy()  # work on copy
            dfpred_labeled = binarize_mutual_exclusive(dfpred_labeled,
                                                       labels=self.get_label_set())

            # # check if we are dealing with partly unlabeled indicator matrix:
            # if (dfl == Annotation.UNLABELED).any().any():
            # we'll use a custom version for computing the scores.
            # this version can deal with indiciator matrices that contains
            #  partly labeled instances (i.e., instances for which some classes are labeled as negative, but none as positive).
            prfs = precision_recall_fscore_support_semi_supervised(
                        dfl.values,
                        dfpred_labeled.values,
                        pos_label=Annotation.ACTIVE,
                        neg_label=Annotation.INACTIVE)
            # else:
            #     # standard score implementation (can deal with indicator matrix but only if they only contain 0/1)
            #     prfs = precision_recall_fscore_support(
            #                 dfl.values,
            #                 dfpred_labeled.values,
            #                 pos_label=None,
            #                 average=None)
            #     prfs = np.vstack(prfs).T

            self._cached_dfscores = pd.DataFrame(data=prfs,
                                                 index=self.get_label_set(),
                                                 columns=[
                                                 'Precision', 'Recall', 'F1', 'Support'
                                                 ])
            self.emit_changed('accuracy')

        return self._cached_dfscores

    def get_test_accuracies(self, force_update=False):

        if self._cached_testscores is None or force_update:

            logger.debug('Has testset: {}'.format(self.has_testset()))

            if self.has_testset():
                dset = self.get_testset()
            else:
                dset = self.get_dataset()

            if not dset.has_ground_truth:
                logger.debug('Test set has not ground truth.')
                # no test scores without ground truth annotations
                return pd.DataFrame(data=0,
                                    index=self.get_label_set(),
                                    columns=[
                                     'Precision', 'Recall', 'F1', 'Support'
                                    ])

            if self.has_testset():
                dfpred = self.get_testset_prediction(force_update)
            else:
                dfpred = self.get_prediction(force_update)

            if dfpred is None:
                logger.debug('Test set has not been predicted yet.')
                # e.g., no detection model trained yet
                return pd.DataFrame(data=0,
                                    index=self.get_label_set(),
                                    columns=[
                                     'Precision', 'Recall', 'F1', 'Support'
                                    ])

            # get ground truth test labels
            dfgt = dset.gt_annotation.get_labeled()

            # all unlabeled frames, get common index
            if not self.has_testset():
                # use unlabeled annotations for testing against GT:
                idx = self.get_annotation().get_unlabeled().index
                idx = idx.intersection(dfgt.index)
            else:
                # use test set annotations
                idx = dfgt.index

            # intersect with predictions
            idx = idx.intersection(dfpred.index)

            # get predictions of test set
            dfpred_test = binarize_mutual_exclusive(dfpred.reindex(idx),
                                                    labels=self.get_label_set())

            # we use the same score-computing version as for the training set, although we probably wouldn't need to. Our ground truth should not contain any partly labeled instances.
            prfs = precision_recall_fscore_support_semi_supervised(dfgt.reindex(idx).values,
                                                   dfpred_test.values,
                                                   pos_label=Annotation.ACTIVE,
                                                   neg_label=Annotation.INACTIVE)

            logger.debug(prfs)

            self._cached_testscores = pd.DataFrame(data=prfs,
                                                 index=self.get_label_set(),
                                                 columns=[
                                                 'Precision', 'Recall', 'F1', 'Support'
                                                 ])

            self.emit_changed('test_score')

        return self._cached_testscores

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Updating and modifying project model
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def reset_project(self):
        self.get_dataset().clear_annotations()
        if self.has_testset():
            self.get_testset().clear_annotations()
        self.get_project().learning_stats.reset_learning_stats()
        self.get_detection_model()._is_fitted = False
        self._reset_cache()
        self._reset_feature_cache(['data', 'test'])
        self.emit_changed('project')

    def set_project_file_location(self, filepath):
        """
        Updates the file path to the locally stored project file.

        Note that this is just a reference to the file; updating this does not copy or move any real data on the disk.

        Parameters
        ----------
        filepath : str
            Location of the project file (*.ittproject)
        """

        if filepath.endswith(os.path.extsep + 'ittproject'):
            self.project._storage_location = os.path.dirname(os.path.normpath(filepath))
            self.project._local_project_file = os.path.basename(filepath)
        else:
            self.project._storage_location = os.path.normpath(filepath)

    def modify_label_set(self, new_label_set):
        if self.get_annotation().modify_label_set(new_label_set):
            # if self.has_testset():
            #     self.get_testset().annotation.modify_label_set(new_label_set)
            self._reset_cache()
            self.emit_changed('label_set')

    def increment_learning_statistics(self, response_state):
        self.project.learning_stats.incr_iterations()
        self.project.learning_stats.incr_response(response_state)
        self.emit_changed('statistics')

    def get_current_learning_iteration(self):
        return self.project.learning_stats.current_iteration

    def append_model_accuracy_history(self):
        current_iteration = self.project.learning_stats.n_query_iterations
        cavg = class_average(self.get_accuracies(), return_pdseries=True)
        ctavg = class_average(self.get_test_accuracies(), return_pdseries=True)
        ctavg = ctavg.rename(lambda x: x + '_test')

        self.project.learning_stats.add_accuracy_history_item(current_iteration,
                                                              **cavg.to_dict())
        self.project.learning_stats.add_accuracy_history_item(current_iteration,
                                                              **ctavg.to_dict())
        self.emit_changed('history')

    def update_annotation(self, index, labels, negative=False):
        logger.log(logging.DEBUG,'Update annotation at {} with {}{}.'.format(index, 'not ' if negative else '', labels))
        self.project.dataset.annotation.update(index, labels, negative)
        # self._reset_cache()
        self.emit_changed('annotation')

    def set_annotation_uncertain(self, index):
        logger.debug('Set annotation to uncertain at {}.'.format(index))
        self.project.dataset.annotation.set_uncertain(index)
        # self._reset_cache()
        self.emit_changed('annotation')

    def load_annotations_from_files(self, files, replace=False, ignore_unknown_file_formats=False):

        logger.info('Loading annotations to {} the current from {} files.'.format('replace' if replace else 'update', len(files)))

        videonames = self.get_video_names()

        df = {}
        dfh = []
        # iterate over files
        for filename in files:

            # check file type
            ext = os.path.splitext(filename)[1]
            if ext == '.csv':

                # check if valid video
                basename = os.path.splitext(os.path.basename(filename))[0]

                video = None
                for v in videonames:
                    if v in basename:
                        video = v
                        break

                if video is None:
                    logger.warning('Could not match label file {} to any video. Skipping for update.'.format(basename))
                    continue

                # load file
                df[video] = pd.read_csv(filename, sep=';', index_col=0)

            elif ext == '.h5':
                dfh.append(pd.read_hdf(filename, key='labels'))

            else:
                if not ignore_unknown_file_formats:
                    raise ValueError('Unknown file fromat: {}'.format(ext))

        # merge dataframes

        if len(df) > 0:
            df = pd.concat(df, names=['video', 'frame'])

        if len(dfh) > 0:
            dfh = pd.concat(dfh)
            dfh.index.names = ['video', 'frame']
            dfh = dfh.sort_level(0)

            # drop unknown videos
            dfh = dfh.loc[videonames, :]

        if len(df) > 0 and len(dfh) > 0:
            df = df.append(dfh).sort_index(0)
        elif len(dfh) > 0:
            df = dfh
        elif len(df) == 0 and len(dfh) == 0:
            return False

        # update labels
        self.get_annotation().update_from_dataframe(df)

        self.emit_changed('annotation')

        return True

    def replace_detection_model(self, detection_model):
        self.project.detection_model.model = detection_model
        self.project.detection_model._is_fitted = False
        self._reset_cache()
        self.emit_changed('detection_model')

    def change_detection_model_parameters(self, **kwargs):
        self.project.detection_model.model.set_params(**kwargs)
        self.project.detection_model._is_fitted = False
        self._reset_cache()
        self.emit_changed('detection_model')
