# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 17:36:20
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:52:45

import logging
from time import time
import pandas as pd
from Queue import Queue, Empty

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject

from ml.oracle import QueryItem, QueryResult
from ml.learner.learner import BaseLearner
from core.utils import now

logger = logging.getLogger(__name__)


class QueryController(QObject):
    """
    The QueryController handles queries suggested by a learner and responded to by an oracle.

    The QueryController is called externally (e.g., by the main UI) to emit a query. It does so by emitting QtSignal `queried_item` whereupon it receives the query response via its QtSlot `on_query_result`. The connections between the QueryController and the Oracle need to established by an external controller. The signal/slot construction allows for easy replacement of the oracle (e.g., can be a UI interacting with a human, some object accessing a database, or simply a random guesser).

    It is in the QueryController's responsibility to pass on received query results for further processing. It does so by emitting the `update_queued` signal. The QueryController can choose to collect several query results and forwarding them in a batch later if that is more appropriate or efficient.

    Note that the learner (passed init attribute) is only used for suggesting queries. It is not automatically triggered for processing query results. You need to connect the learner (or another one) to the `update_queued` signal externally.

    Attributes
    ----------
    learner : ml.learner.BaseLearner
        The learner who suggests queries and processes the query results
    queried_item : pyqtSignal
        Emitted when QueryController asks for a label of some query item
    result_queue : Queue
        Collects received QueryResults for further processing
    update_cycle : int
        Amount of QueryResults to receive before triggering a full update cycle
    update_queued : pyqtSignal
        Description
    """
    trigger_query = pyqtSignal(str)
    queried_item = pyqtSignal(QueryItem)
    query_error = pyqtSignal(str)
    update_queued = pyqtSignal(list)

    def __init__(self, learner):
        super(QueryController, self).__init__()

        self.learner = learner
        self.result_queue = Queue()

        self.trigger_query.connect(self.query_item)

    def len_queue(self):
        return self.result_queue.qsize()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Sending and receiving queries
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @pyqtSlot(str)
    def query_item(self, parameter='strong_unordered'):
        """
        Pick a new query item and emit the query.

        Emits
        -----
        self.queried_item
        """

        try:
            try:
                if not self.learner._is_fitted:
                    self.learner.fit()
                vid, start, end, extra = self.learner.suggest_query(parameter)
            except RuntimeError as e:
                logger.error('Suggesting query failed.', exc_info=True)
                self.query_error.emit(str(e))
                return

            idx = pd.MultiIndex.from_product([[vid], range(start, end+1)],
                                             names=['video', 'frame'],
                                             sortorder=0)
        except Exception as e:
            logger.error('Suggesting query failed.', exc_info=True)
            self.query_error.emit(str(e))
            return

        kind_mapping = {
            'strong_unordered': 'singlelabel',
            'strong_ranked': 'singlelabel',
            'weak': 'binary'
        }

        query = QueryItem(time(), idx,
                          kind=kind_mapping[parameter],
                          parameter=extra)
        logger.debug('Emit query')
        logger.debug('  Query: {}'.format(query))
        self.queried_item.emit(query)

    @pyqtSlot(QueryResult)
    def on_query_result(self, query_result):
        """
        Slot to receive query result.

        Parameters
        ----------
        query_result : QueryResult
            Query result
        """
        logger.debug('Received query result')
        logger.debug('  Query: {}'.format(query_result))

        if query_result.state == QueryResult.CANCELED:
            # don't process canceled queries
            return

        self.result_queue.put(query_result)
        self.trigger_update_cycle()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Processing query results
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def trigger_update_cycle(self):
        to_process = []
        try:
            while True:
                to_process.append(self.result_queue.get(timeout=0.5))
        except Empty:
            pass

        logger.debug('{} queries gathered for update cycle.'.format(len(to_process)))

        if len(to_process) > 0:
            self.update_queued.emit(to_process)
