# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-05 10:40:03
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-21 16:06:35

from datetime import datetime
from glob import glob
import os, errno
import logging
from shutil import move, rmtree
from distutils.dir_util import copy_tree, remove_tree

logger = logging.getLogger(__name__)


def now_iso():
        return datetime.now().isoformat()


def now(microsec=True):
    if microsec:
        return datetime.now().strftime('%Y%m%d%H%M%S%f')
    else:
        return datetime.now().strftime('%Y%m%d%H%M%S')


def np2csv_string(a, precision=6, sep=';'):
    fmtitem = '{:.%dg}' % precision
    fmt = sep.join([fmtitem] * len(a))
    return fmt.format(*a)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def is_project_folder(path):
    return os.path.isdir(path) and len(glob(os.path.join(path, '*.ittproject'))) > 0


def clear_project_folder(path, keep_project_file=True):
    if not is_project_folder(path):
        return

    logger.info('Clear project folder: {}'.format(path))
    remove_tree(os.path.join(path, 'logs'))

    if not keep_project_file:
        for prjfile in glob(os.path.join(path, '*.ittproject')):
            logger.info('Delete project file: {}'.format(prjfile))
            os.unlink(prjfile)


def move_project_storage(src, dst):
    if not os.path.isdir(src):
        raise IOError('Cannot move directory: directory does not exist: {}'.format(src))

    if is_project_folder(dst):
        raise ValueError('Cannot move to existing project storage: {}'.format(dst))

    move(src, dst)


def copy_project_storage(src, dst):
    if not os.path.isdir(src):
        raise IOError('Cannot copy directory: directory does not exist: {}'.format(src))

    if is_project_folder(dst):
        raise ValueError('Cannot copy to existing project storage: {}'.format(dst))

    for folder in ['logs']:
        copy_tree(os.path.join(src, folder), os.path.join(dst, folder))


def copy_folder(src, dst):
    copy_tree(src, dst)


def is_backup_folder(path, require_models=False):
    return (os.path.isdir(path) and
            len(glob(os.path.join(path, '*.json'))) > 0 and
            os.path.isdir(os.path.join(path, 'exports')) and
            len(glob(os.path.join(path, 'exports', 'labels_*'))) > 0 and
            os.path.isdir(os.path.join(path, 'logs')) and
            (not require_models or os.path.isdir(os.path.join(path, 'models'))))
