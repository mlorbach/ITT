# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-22 16:01:11
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-10 09:42:40

import os
import logging
import pandas as pd
import numpy.random
from Queue import Queue

from ml.oracle import BaseOracle, QueryItem, QueryResult
from core.dataset import Dataset
from core.annotation import Annotation

logger = logging.getLogger(__name__)


class DatasetOracle(BaseOracle):
    """
    The DatasetOracle is connected to an annotation dataset from which it retrieves the labels it will return.

    The most common application of the DatasetOracle is to connect it to the "ground truth" annotation dataset that has been obtained sometime before. With that dataset we can simulate the human in the loop without needing the human actually be present at runtime.

    Another useful application is the replication of previously obtained results. We can record the human responses, make a dataset from them and then hook this oracle up with the recorded response dataset. If we execute the same queries, we can replicate previous learning results (or tweak the learner parameters to see the differences).

    Attributes
    ----------
    conflict_strategy : str, default = 'majority'
        The strategy that is used to resolve labeling conflicts; choices are {'majority' (default), 'reject', 'multilabel'}
    conflict_threshold : float, default = 0.2
        A threshold determining the fraction of frames that a label has to be active within the queried segment, for it to be considered ACTIVE.
    dataset : core.Dataset
        Dataset with annotations that will be returned upon query
    project_ctrl : core.ProjectController
        Controller providing access to project related information
    query_queue : Queue
        Queue storing incoming queries (for possible batch processing). Batch processing is not implemented yet.
    """
    def __init__(self,
                 project_ctrl,
                 conflict_strategy='majority',
                 conflict_threshold=0.2):
        super(DatasetOracle, self).__init__()
        self._provides_query_kinds = ['label', 'sample', 'binary']

        self.project_ctrl = project_ctrl
        self.dataset = None

        possible_conflict_strategies = ['majority', 'reject', 'multilabel']
        if conflict_strategy not in possible_conflict_strategies:
            raise ValueError('conflict_strategy must be one of {} but is {}.'.format(possible_conflict_strategies, conflict_strategy))
        self.conflict_strategy = conflict_strategy
        self.conflict_threshold = conflict_threshold

        # to be future-proof, let a queue handle all incoming queries
        self.query_queue = Queue()

    def _initialize_dataset(self):
        """
        Initialize "ground truth" dataset by loading labels from disk.

        Returns
        -------
        None
        """
        self.dataset = Dataset(self.project_ctrl.name + '_GT',
                                   self.project_ctrl.get_metadata())
        dsetpath = os.path.join(self.dataset.metadata['basepath'], self.dataset.metadata['frame_annotations_folder'], 'iTT_gt_annotations{}.h5'.format(self.dataset.metadata['default_annotation_file_suffix']))
        logger.info('Loading GT dataset: {}'.format(dsetpath))
        df = pd.read_hdf(dsetpath, 'annotations')

        self.dataset.create_annotation_from_dataframe(df)

    def _determine_response(self, query_item):

        if self.dataset is None:
            # if we haven't setup the "ground truth" dataset yet,
            # create it from loading the dataset from disk
            self._initialize_dataset()

        if query_item.kind == 'sample':
            try:
                return self._determine_sample(query_item)
            except Exception as e:
                return QueryResult(query_item, QueryResult.CANCELED, e.message)
        elif query_item.kind == 'binary':
            return self._determine_binary(query_item)
        else:  # label
            return self._determine_label(query_item)

    def _determine_label(self, query_item):

        y = self.dataset.annotation.get_labels_by_index(query_item.value)
        y_dist = (y == Annotation.ACTIVE).sum(axis=0)
        y_ratio = y_dist / float(y.shape[0])

        result = QueryResult(query_item)

        if not any(y_ratio > self.conflict_threshold):
            # if no labels at all
            result.state = QueryResult.UNCERTAIN
        elif (y_ratio > self.conflict_threshold).sum() == 1:
            # if exactly one label
            result.state = QueryResult.LABELED
            result.response_value = y_dist.index[y_ratio > self.conflict_threshold].tolist()
        else:
            # multiple labels are active in the segment (at least two reach the conflict threshold)
            #  --> resolve conflict depending on strategy

            if self.conflict_strategy == 'majority':
                # pick majority label
                result.state = QueryResult.LABELED
                result.response_value = y_dist.idxmax()  # == argmax

            elif self.conflict_strategy == 'reject':
                # reject if conflict is higher than conflict threshold
                #  means: if more than one label is active more than r percent of the segment, then reject
                result.state = QueryResult.SEGMENTATION
                result.response_value = y_dist.index[y_ratio > self.conflict_threshold].tolist()

            elif self.conflict_strategy == 'multilabel':
                # accept multilabel annotation including all labels that are active at least r percent of the segment
                result.state = QueryResult.LABELED
                result.response_value = y_dist.index[y_ratio > self.conflict_threshold].tolist()

        logger.info('Oracle has spoken: {} = {}'.format(
                QueryResult.ResponseState[result.state][1],
                result.response_value))

        return result


    def _determine_binary(self, query_item):

        y = self.dataset.annotation.get_labels_by_index(query_item.value)
        y_dist = (y == Annotation.ACTIVE).sum(axis=0)
        y_ratio = y_dist / float(y.shape[0])

        result = QueryResult(query_item)
        suggested_label = query_item.parameter[0]
        result.response_value = [suggested_label]

        if y_ratio.loc[suggested_label] > self.conflict_threshold:
            result.state = QueryResult.LABELED
        else:
            # logger.log(15, 'y_ratio[{}] = {}'.format(suggested_label, y_ratio.loc[suggested_label]))
            result.state = QueryResult.NEGATIVE_LABEL

        logger.info('Oracle has spoken: {} = {}'.format(
                QueryResult.ResponseState[result.state][1],
                result.response_value))

        return result


    def _determine_sample(self, query_item):
        # randomly pick one sample per requested class
        # picks the mid-frame of a random segment

        classes = query_item.value

        valid_features_idx = self.project_ctrl.get_features().index

        sample_dict = {}
        for label in classes:
            # pick a random frame of the requested label
            idx = numpy.random.choice(self.dataset.annotation.df.loc[self.dataset.annotation.df[label] == Annotation.ACTIVE].index.intersection(valid_features_idx), 1)[0]

            # find the segment of which the frame is part of
            dfa_video = self.dataset.annotation.get_video(idx[0])
            segs = dfa_video.loc[:, label].diff().abs().fillna(0).cumsum()
            seg = segs.loc[segs == segs.loc[idx[1]]]

            # pick the mid-frame of that segment
            mid_frame = seg.index[len(seg) / 2]

            # store answer
            sample_dict[label] = (idx[0], mid_frame)

        result = QueryResult(query=query_item)
        result.state = QueryResult.LABELED
        result.response_value = sample_dict

        return result
