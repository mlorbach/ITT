# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 17:35:54
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-06 17:02:55

import logging
import time

import numpy as np

from sklearn.preprocessing import LabelEncoder
from sklearn.dummy import DummyClassifier

from core.dataset import Dataset
from core.annotation import Annotation

logger = logging.getLogger(__name__)

class DetectionModel(object):
    """docstring for DetectionModel"""
    def __init__(self, model=DummyClassifier()):
        super(DetectionModel, self).__init__()
        self.model = model
        self._is_fitted = False
        self._le = LabelEncoder()

    def get_type(self):
        return self.model.__class__.__name__

    def is_fitted(self):
        return self._is_fitted

    def predict(self, X):
        if self.is_fitted():
            return self._le.inverse_transform(self.model.predict(X))
        else:
            return np.random.choice(len(self._le.classes_))

    def predict_proba(self, X):
        if self.is_fitted():
            return self.model.predict_proba(X)
        else:
            return np.ones((X.shape[0], len(self._le.classes_)), dtype=float) / float(len(self._le.classes_))

    def decision_function(self, X):
        if self.is_fitted():
            return self.model.decision_function(X)
        else:
            return np.zeros((X.shape[0], len(self._le.classes_)), dtype=float)

    def fit(self, X, y):

        self._is_fitted = False
        self.model.fit(X, y)
        self._is_fitted = True

    def train(self, project_ctrl):

        logger.info('Training detection model...')
        tic = time.clock()

        dfa = project_ctrl.get_annotation().get_labeled()

        self._le.fit(project_ctrl.get_label_set())

        try:

            if hasattr(self.model, 'supports_indicator_matrix') and self.model.supports_indicator_matrix:
                logger.debug('Detection model supports learning from indicator matrices.')
                y = dfa.values
                lidx = dfa.index
            else:
                # ignore overlapping and negative samples:
                dfy = dfa.loc[(dfa == Annotation.ACTIVE).sum(axis=1) == 1, :]
                y = self._le.transform(dfy.idxmax(axis=1).values)
                lidx = dfy.index
                logger.debug('Detection model does not support learning from indicator matrices. Ignoring partly negative samples.')

            # get features
            dff = project_ctrl.get_features().loc[lidx, :]
            X = dff.values

            logger.debug('Training set size (features) = {}'.format(X.shape))
            logger.debug('Training set size (labels)   = {}'.format(y.shape))

            self.fit(X, y)

        except ValueError:
            # raised if could not fit, e.g., because no annotated data yet
            logger.info('Could not train detection model (yet). Using Random classifier for now.')

        logger.log(15, 'Training detection model... Done. {:.3f}s'.format(time.clock() - tic))
