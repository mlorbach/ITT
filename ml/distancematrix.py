# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-31 14:45:23
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-12-20 14:37:52

import numpy as np
import pandas as pd
from core.base import BaseObject
from sklearn.metrics.pairwise import pairwise_distances
# from scipy.sparse import lil_matrix

import logging
logger = logging.getLogger(__name__)


class DistanceMatrix(BaseObject):
    """docstring for DistanceMatrix"""

    @property
    def shape(self):
        return self.D.shape

    @property
    def index(self):
        return self.D.index

    @property
    def columns(self):
        return self.D.columns

    def __init__(self):
        super(DistanceMatrix, self).__init__()
        self.D = None

class MinDistanceMatrix(DistanceMatrix):
    """docstring for MinDistanceMatrix"""
    def __init__(self, n_classes, index=None):
        super(MinDistanceMatrix, self).__init__()

        if index is None:
            index = []
        self.D = pd.DataFrame(data=np.nan, index=index, columns=range(n_classes), dtype=float)
        self.n_classes = n_classes

    @classmethod
    def from_labeled_features(cls, dff, yL, Lidx, Uidx, n_classes=None):
        if n_classes is None:
            n_classes = len(np.unique(yL))

        MDM = cls(n_classes=n_classes,
                  index=Lidx.append(Uidx).sort_values())

        D = cls._compute_distances(dff.loc[Lidx.sort_values(), :], dff.loc[Uidx.sort_values(), :])
        Dmin = cls._extract_min_distances(D, yL)

        Dmin = pd.DataFrame(data=Dmin, index=Uidx.sort_values(),
                            columns=range(n_classes))
        MDM.D = Dmin

        return MDM

    @staticmethod
    def _compute_distances(X, Y=None):
        return pairwise_distances(X, Y)

    @staticmethod
    def _extract_min_distances(D, y, n_classes=None):
        """
        Extract for each column in distance matrix D, the distances to the closest labeled sample of each class.

        The distance matrix D must be arranged in the form [labeled x unlabeled] and y must correspond to the rows in D (i.e., be of length |labeled| = D.shape[0]).

        Parameters
        ----------
        D : np.ndarray (|L|, |U|)
            Distance matrix between labeled and unlabeled samples.
        y : array-like, (|L|, )
            Labels of the labeled samples.
        n_classes : int, optional
            Number of expected classes (specify if y may not contain all classes). If None, infer from the unique elements in y.

        Returns
        -------
        np.ndarray
            Distances between unlabeled samples and the closest sample of each class, shape = (|U|, n_classes)
        """
        if n_classes is None:
            n_classes = len(np.unique(y))
        Dmin = []
        for c in range(n_classes):
            y_idx = np.where(y == c)[0]
            if len(y_idx) > 0:
                Dmin.append(np.min(D.take(y_idx, axis=0), axis=0))
            else:
                # no samples of this class
                Dmin.append([np.inf] * D.shape[1])
        return np.stack(Dmin).T  # (len(Uidx) x n_classes)

    def remove(self, Uidx):
        """
        Drop information on Uidx rows.

        Parameters
        ----------
        Uidx : TYPE
            Description
        """
        self.D = self.D.drop(Uidx, axis=0, errors='ignore')

    def update(self, dff, yL, Lidx):
        # compute distances from all labeled Lidx to all unlabeled Uidx
        D = self._compute_distances(dff.loc[Lidx.sort_values(), :], dff.loc[self.D.index, :])
        Dmin = self._extract_min_distances(D, yL, n_classes=self.n_classes)
        Dmin = pd.DataFrame(data=Dmin, index=self.D.index, columns=range(self.n_classes))

        # update distances in D if new distance is < old distance
        mask = Dmin < self.D
        self.D[mask] = Dmin[mask]
