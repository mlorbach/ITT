# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-19 16:06:54
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-30 16:49:59

import logging
import time

from MLUtilities.sklearn_extensions.transformers import DummyTransformer

logger = logging.getLogger(__name__)

class FeatureProcessor(object):
    """docstring for FeatureProcessor"""
    def __init__(self, model=DummyTransformer()):
        super(FeatureProcessor, self).__init__()
        self.model = model
        self._is_fitted = False

    def is_fitted(self):
        return self._is_fitted

    def transform(self, X):
        return self.model.transform(X)

    def inverse_transform(self, X):
        return self.model.inverse_transform(X)

    def get_type(self):
        return self.model.__class__.__name__

    def fit(self, X):

        logger.info('Fitting feature processing...')
        tic = time.clock()
        self._is_fitted = False

        logger.debug('Training set size (features) = {}'.format(X.shape))

        self.model.fit(X)

        logger.log(15, 'Fitting feature processing... Done. {:.3f}s'.format(time.clock() - tic))
        self._is_fitted = True

    def fit_transform(self, X):
        self.fit(X)
        return self.transform(X)
