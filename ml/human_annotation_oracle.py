# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-16 14:47:51
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 16:43:31

import logging
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject
# from ui.annotationDialog import AnnotationDialog
from ui.humanFindLabelDialog import HumanFindLabelDialog

from ui.annotation import AnnotationWidget
# from ui.humanFinalLabelWidget import humanFinalLabelWidget

from Queue import Queue

from ml.oracle import BaseOracle, QueryItem, QueryResult

logger = logging.getLogger(__name__)

class HumanAnnotationOracle(BaseOracle):
    """
    Ask a human to provide a label using some UI.

    Needs to be connected to `Learner`'s `queried_item` signal (inherited from `BaseOracle`). Typically, some external controller should take care of this.

    On a received query, `_determine_response()` is automatically called (see `BaseOracle`). In `_determine_respone()`: launch UI dialog, and initialize with query, connect to dialog's response signal.

    On received dialog response, process response and emit reponded signal (inherited from BaseOracle)

    Note that although queries are stored in a Queue, there is no queue processing implemented as of yet. The queue is only there for possible extenstions in the future.

    Attributes
    ----------
    annotator : ui.AnnotationWidget
        Reference to the widget that we create
    dialog : ui.HumanFindLabelDialog
        Reference to the dialog that we create
    main_window : ITTProjectOverview
        Reference to parent window in which we can place the widget
    project_ctrl : core.ProjectController
        Reference to the current project (dataset, videos, etc.)
    query_queue : queue.Queue()
        A queue storing the incoming queries
    """

    def __init__(self, project_ctrl, main_window):
        super(HumanAnnotationOracle, self).__init__()

        self._provides_query_kinds = ['label', 'sample', 'binary']

        # we'll need a reference to the current project (dataset, videos, etc.)
        self.project_ctrl = project_ctrl

        # and a reference to the dialog that we'll create later on
        self.main_window = main_window
        self.annotator = None
        self.dialog = None

        # to be future-proof, let a queue handle all incoming queries
        self.query_queue = Queue()

    def reset(self):
        super(HumanAnnotationOracle, self).reset()
        self.annotator = None
        self.dialog = None
        self.query_queue = Queue()

    @pyqtSlot(QueryItem)
    def receive_query(self, query_item):
        """
        Qt slot receiving labeling queries.

        Query is passed to self._determine_response() which handles the actual processing.

        Parameters
        ----------
        query_item : QueryItem
            Incoming query
        """
        logger.debug('Received query {}'.format(query_item))

        ret = self._determine_response(query_item)
        if ret is not None:
            self.provide_result(ret)


    def _determine_response(self, query_item):

        if query_item.kind == 'sample':
            return self._determine_sample(query_item)
        else:  # label or binary
            # in case of normal labeling action, _determine_label doesn't return any results right away but waits for the dialogue's response. The response function (return_human_response()) that is called will call provide_result() itself.
            self._determine_label(query_item)
            return None

    def _determine_label(self, query_item):

        # put query into query queue
        self.query_queue.put(query_item)

        # make sure we can access the widget and it has not been destroyed
        if self.annotator is not None:
            try:
                self.annotator.poke()
            except RuntimeError:
                # if the widget instance has been destroyed in the meantime, we reset it and create a new one
                logger.log(15, "Annotator was destroyed. Set to None and create new one.")
                self.annotator = None

        if self.annotator is None or self.annotator.response_mode != query_item.kind:
            # create a new annotation widget for user interaction
            self.annotator = AnnotationWidget(self.project_ctrl,
                                              query_item,
                                              response_mode=query_item.kind,
                                              parent=self.main_window)

            # the annotator widget will be hidden in the main window if not used; retain its size so that we don't screw up the layout
            sizePolicy = self.annotator.sizePolicy()
            sizePolicy.setRetainSizeWhenHidden(True)
            self.annotator.setSizePolicy(sizePolicy)
        else:
            # re-use an existing widget
            self.annotator.set_query(query_item)

        # connect to dialog response (reject, accept)
        self.annotator.rejected.connect(self._reject_canceled)
        self.annotator.rejected_extra.connect(self._reject_extra)
        self.annotator.accepted_labels.connect(self._accept)

        # set as central widget in main window (if it's not there yet)
        self.main_window.show_central_widget(self.annotator)

        # we return nothing here as we will wait for the user to click something in the widget
        return None

    def _determine_sample(self, query_item):
        # randomly pick one sample per requested class

        classes = query_item.value

        videos = self.project_ctrl.get_video_collection().index.unique().tolist()

        sample_dict = {}
        for label in classes:
            self.dialog = HumanFindLabelDialog(self.project_ctrl, videos, label)
            self.dialog.exec_()
            sample_dict[label] = self.dialog.get_selected_sample()
            logger.info('Human selected a {} sample: {}'.format(label, sample_dict[label]))

        result = QueryResult(query=query_item)
        result.state = QueryResult.LABELED
        result.response_value = sample_dict

        return result

    def _reject_extra(self, response_state):
        self.return_human_response(response_state)

    def _reject_canceled(self):
        self.return_human_response(QueryResult.CANCELED)

    def _accept(self, labels):
        self.return_human_response(QueryResult.LABELED, labels)

    def return_human_response(self, response_state, response_value=None):
        """
        Construct a QueryResponse from the human input and emits the response object.

        Parameters
        ----------
        response_state : QueryResult.ResponseState
            The response state
        response_value : None, optional
            Optional additional value attached to the state, e.g., the chosen labels.
        """
        # disconnect from dialog responses
        self.annotator.rejected.disconnect(self._reject_canceled)
        self.annotator.rejected_extra.disconnect(self._reject_extra)
        self.annotator.accepted_labels.disconnect(self._accept)

        # self.annotator.clear_query()

        # retrieve original query from queue to combine with result
        query_item = self.query_queue.get()
        logger.info('Human has spoken: {} = {}.'.format(
            QueryResult.ResponseState[response_state][1],
            response_value
        ))

        # change response if it was a binary query:
        if query_item.kind == 'binary' and response_state == QueryResult.LABELED:
            if response_value[0] == 'Yes':
                response_value = [query_item.parameter[0]]
            else:
                response_state = QueryResult.NEGATIVE_LABEL
                response_value = [query_item.parameter[0]]

        # construct query result from query item and user reponse
        result = QueryResult(query_item,
                             response_state,
                             response_value)

        # provide query result on BaseOracle's signal
        self.provide_result(result)
