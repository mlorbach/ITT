# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-22 13:53:23
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:53:03

import numpy as np
import pandas as pd
import logging

from PyQt5.QtCore import pyqtSlot

from scipy.stats import entropy
from sklearn.preprocessing import LabelEncoder
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.utils.validation import NotFittedError
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from MLUtilities.sklearn_extensions.scaling import RobustMinMaxScaler
from MLUtilities.annotation.utils import frame_to_segment_annotations, binarize_mutual_exclusive

from core.annotation import Annotation
from ml.learner.learner import BaseLearner

logger = logging.getLogger(__name__)


class ClusterLearner(BaseLearner):
    """
    Cluster once in the beginning, then pick cluster, then pick random start frame, query x seconds following

    Attributes
    ----------
    C : np.ndarray
        Current cluster assignments (per frame)
    cluster_alg : BaseEstimator
        The clustering algorithm (can be Pipeline)
    cluster_selection_strategy : str
        how to select cluster? {'random', 'fewest_labels', 'most_heterogeneous', 'lowest_accuracy'}
    init : str
        How to initialize clustering?
          - 'auto': unsupervised (kmeans default)
          - 'labeled_mean': use labeled instances for initialization, then perform clustering on unlabeled data
          - 'labeled_only': use only labeled instances to determine cluster centers, no further clustering
    n_classes : int
        Number of target classes
    n_clusters : int
        Internally set
    segment_selection_strategy : str
        How to select a segment? (only random is supported by ClusterLearner)
          - 'random': pick a random frame within the chosen cluster
    """
    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2,
                 cluster_alg=None, init='auto',
                 cluster_selection_strategy='random',
                 segment_selection_strategy='random'):
        super(ClusterLearner, self).__init__(project_ctrl,
                                             update_cycle=update_cycle,
                                             min_duration=min_duration,
                                             max_duration=max_duration)
        self.n_classes = len(self.project_ctrl.get_label_set())

        if init not in ['auto', 'labeled_mean', 'labeled_only']:
            raise ValueError('Unknown initialization parameter init = {}'.format(init))
        self.init = init

        if cluster_alg is None:
            self.cluster_alg = Pipeline([
                                        ('scaler', RobustMinMaxScaler((-1, 1))),
                                        # ('clusterer', KMeans(2*self.n_classes, n_jobs=4))
                                        ('clusterer', MiniBatchKMeans(self._get_default_n_clusters(self.n_classes), batch_size=1000,
                                                        compute_labels=False))
                                        ])
        else:
            self.cluster_alg = cluster_alg

        if cluster_selection_strategy not in ['random', 'fewest_labels', 'most_heterogeneous', 'lowest_accuracy']:
            logger.warning('Unknown cluster selection strategy: {}. Falling back to "random".'.format(cluster_selection_strategy))
            cluster_selection_strategy = 'random'
        self.cluster_selection_strategy = cluster_selection_strategy
        self.segment_selection_strategy = segment_selection_strategy
        self.C = None
        self._is_fitted = False

    def reset(self):
        super(ClusterLearner, self).reset()
        self.C = None
        self.n_clusters = None
        self._le = None

    def _get_default_n_clusters(self, n_classes):
        ''' to be overwritten in sub-classes '''
        return n_classes

    def get_learner_state(self):
        ''' to be overwritten in sub-classes '''
        return self.C.astype(np.uint8)

    @pyqtSlot(str)
    def project_update(self, what):
        if what in ['project', 'label_set']:
            self.n_classes = len(self.project_ctrl.get_label_set())
            self.cluster_alg.set_params(clusterer__n_clusters=self._get_default_n_clusters(self.n_classes))
            self._is_fitted = False

        # do whatever super class wants to do
        super(ClusterLearner, self).project_update(what)

    def _get_labeled_means(self, dfl=None):
        if dfl is None:
            dfl = self.project_ctrl.get_annotation().get_labeled()

        # remove samples with more than one label
        dfl = dfl.loc[(dfl == Annotation.ACTIVE).sum(axis=1) == 1, :].copy()

        dff = self.project_ctrl.get_features()

        means = []
        for c, label in enumerate(dfl.columns):
            idx = dfl.loc[dfl[label] == Annotation.ACTIVE].index

            idx = idx.intersection(dff.index)

            if len(idx) > 0:
                X = dff.loc[idx, :].values
                means.append(np.mean(X, axis=0))
            else:
                # no instances of that label yet
                # make a random guess, TODO: could do something better?
                means.append(np.random.rand(dff.shape[1]) * 2 - 1)

        return np.stack(means, axis=0)

    def fit(self):
        self._is_fitted = False

        # workaround (silencing) for issue https://github.com/scikit-learn/scikit-learn/issues/6370
        #  which generates loads of deprecation warnings
        import warnings
        warnings.filterwarnings('ignore', message='Changing the shape of non-C contiguous array')
        warnings.filterwarnings('ignore', category=DeprecationWarning)

        if self.init == 'labeled_mean' and self.project_ctrl.get_annotation().has(Annotation.ACTIVE):
            # initialize clustering by means of all labeled examples so far
            means = self._get_labeled_means()
            logger.info('Using labeled instances for cluster initialization.')
            self.cluster_alg.set_params(clusterer__init=means,
                                        clusterer__n_init=1)

        elif self.init == 'labeled_only' and self.project_ctrl.get_annotation().has(Annotation.ACTIVE):
            # use means of all labeled examples as cluster centroids without using the unlabeled data at all
            means = self._get_labeled_means()
            self.cluster_alg.set_params(clusterer__init=means,
                                        clusterer__n_init=1,
                                        clusterer__max_iter=1)
            logger.info('Perform clustering on labeled frames only...')
            # logger.info('Means = {}'.format(means))
            self.cluster_alg.fit(means)
            self._is_fitted = True

        else: # auto
            self.cluster_alg.set_params(clusterer__init='k-means++',
                                        clusterer__n_init=3,
                                        clusterer__max_iter=300)

        self._le = LabelEncoder()

        X = self.project_ctrl.get_features()
        # X = X.interpolate()  # fill nan values
        logger.log(logging.DEBUG, 'Dataset shape: {}'.format(X.shape))

        if not self._is_fitted:  # e.g., if initialization involved fitting
            logger.info('Perform clustering...')
            self.cluster_alg.fit(X.iloc[::3, :])  # fitting on every third frame is more than enough

        self.C = self.cluster_alg.predict(X)
        self.C = self._le.fit_transform(self.C)  # cluster assignment
        self.n_clusters = len(self._le.classes_)
        logger.info('Found {} clusters.'.format(self.n_clusters))

        super(ClusterLearner, self).fit()
        self._is_fitted = True
        self.emit_learner_state_changed()

    def _get_cluster_distributions(self, include_unlabeled=True):
        # returns dataframe

        dfY = self.project_ctrl.get_annotation().df  # [N x n_classes]

        # count of labeled
        c_labeled = (dfY == Annotation.ACTIVE).groupby(self.C).sum().astype(int)

        # count of unlabeled
        c_unlabeled = (dfY == Annotation.UNLABELED).all(axis=1).groupby(self.C).sum()
        c_unlabeled.name = Annotation.LabelState[Annotation.UNLABELED][1]

        # count of uncertain
        c_uncertain = (dfY == Annotation.UNCERTAIN).all(axis=1).groupby(self.C).sum()
        c_uncertain.name = Annotation.LabelState[Annotation.UNCERTAIN][1]

        if include_unlabeled:
            return pd.concat((c_labeled, c_unlabeled, c_uncertain), axis=1).astype(int)
        else:
            return c_labeled

    def _get_cluster_label_entropy(self):
        y_counts = self._get_cluster_distributions(include_unlabeled=False)
        H = np.apply_along_axis(entropy, 1, y_counts)
        return H

    def _select_cluster_fewest_labels(self, candidate_mask):
        # pick cluster with fewest labeled frames
        cluster_count = pd.Series(self.project_ctrl.get_annotation().get_labeled_mask(), dtype=int).groupby(self.C).sum()

        # drop non-candidates
        cluster_count = cluster_count.drop(np.where(~candidate_mask)[0].tolist(), errors='ignore')

        idxmin = cluster_count.idxmin()

        if np.sum(cluster_count == cluster_count[idxmin]) > 1:
            # if multiple minimum values, then pick at random
            return np.random.choice(cluster_count.index[cluster_count == cluster_count[idxmin]])
        else:
            return idxmin

    def _select_cluster(self, candidate_mask):
        """
        Picks a random cluster.

        Parameters
        ----------
        candidate_mask : array-like, bool
            Boolean array indicating cluster candidates to choose from.

        Returns
        -------
        int
            Index of chose cluster
        """
        logger.info('Select cluster with {}'.format(self.cluster_selection_strategy))

        if self.cluster_selection_strategy == 'fewest_labels':
            return self._select_cluster_fewest_labels(candidate_mask)

        elif self.cluster_selection_strategy == 'most_heterogeneous':
            # pick most heterogenous cluster (highest entropy)
            H = self._get_cluster_label_entropy()

            # prevent selecting non-candidate cluster;
            H[~candidate_mask] = -np.inf

            logger.log(logging.DEBUG, 'Cluster entropy: \n{}'.format(H))

            Hmax = np.argmax(H)

            if np.sum(H == H[Hmax]) > 1:
                # if multiple maximum values, then pick at random
                return np.random.choice(np.where(H == H[Hmax])[0])
            else:
                return Hmax

        elif self.cluster_selection_strategy == 'lowest_accuracy':
            # pick cluster with lowest accuracy?

            if not self.project_ctrl.get_detection_model().is_fitted():
                return np.random.choice(np.where(candidate_mask)[0])

            # predictions [N]
            dfpred = self.project_ctrl.get_prediction()

            # labels [nL x 5]
            Y = self.project_ctrl.get_annotation().get_labeled()

            # remove predictions of unlabeled frames
            dfpred = dfpred.reindex(Y.index).copy()
            dfpred = binarize_mutual_exclusive(dfpred, labels=self.project_ctrl.get_label_set())

            C_labeled = self.C[self.project_ctrl.get_annotation().get_labeled_mask()]

            acc = np.zeros(self.n_clusters)
            # groupby cluster, compute accuracy
            for c in range(self.n_clusters):
                if not candidate_mask[c]:
                    acc[c] = np.inf
                else:
                    acc[c] = accuracy_score(Y.loc[C_labeled == c], dfpred.loc[C_labeled == c])

            logger.log(logging.DEBUG, 'Cluster accuracies: {}'.format(acc))
            minacc = np.argmin(acc)
            if np.sum(acc == acc[minacc]) > 1:
                # multiple minimum values, then pick at random
                return np.random.choice(np.where(acc == acc[minacc])[0])
            else:
                return minacc

        else:
            # random
            return np.random.choice(np.where(candidate_mask)[0])

    def _select_sample(self, cluster_index):
        """
        Picks a random sample from the cluster.

        Parameters
        ----------
        cluster_index : int
            Index of cluster

        Returns
        -------
        int
            Index of chosen sample (location index [0, ..., N])
        """

        # get all indices of samples in this cluster
        cluster_mask = self.C == cluster_index
        labeled_mask = self.project_ctrl.get_annotation().get_labeled_mask()

        # only allow samples that we haven't sampled yet
        valid_indices = np.where(cluster_mask & ~labeled_mask)[0]

        logger.debug('Choose sample from {}'.format(valid_indices))

        # pick at random
        return np.random.choice(valid_indices)

    def suggest_query(self, kind):

        if not self._is_fitted:
            self.fit()

        # select cluster candidates
        # -------------------------

        # we know we can't query exhausted clusters anymore, so remove
        #  the clusters from c_count without unlabeled examples:
        u_count = np.bincount(self.C[self.project_ctrl.get_annotation().get_unlabeled_mask()],
                              minlength=self.n_clusters)

        c_candidate_mask = u_count > 0

        # determine cluster and sample
        # ----------------------------
        selected_c = self._select_cluster(c_candidate_mask)
        selected_idx = self._select_sample(selected_c)

        video, mid_frame = self.project_ctrl.get_annotation().df.index[selected_idx]

        video, start_frame, end_frame = self.project_ctrl.get_dataset().get_valid_segment_tuple(video, self.max_duration_frames, mid=mid_frame, ensure_duration=True)

        logger.log(15, 'Selected segment: ({}, {}, {}), m={}, d={}'.format(video, start_frame, end_frame, mid_frame, end_frame-start_frame+1))

        # return
        # ------
        return video, start_frame, end_frame, None


class IterativeClusterLearner(ClusterLearner):
    """
    Cluster from scratch in every iteration, then pick cluster, then pick random start frame, query x seconds following.

    Attributes
    ----------
    refit_after : int
        Recluster after n iterations
    """
    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2,
                 cluster_alg=None, init='auto',
                 cluster_selection_strategy='random',
                 segment_selection_strategy='random',
                 refit_after=1):
        super(IterativeClusterLearner, self).__init__(
                project_ctrl,
                update_cycle=update_cycle,
                min_duration=min_duration,
                max_duration=max_duration,
                cluster_alg=None,
                init=init,
                cluster_selection_strategy=cluster_selection_strategy,
                segment_selection_strategy=segment_selection_strategy)

        self.refit_after = refit_after
        self._k_iterations = 0

    def reset(self):
        super(IterativeClusterLearner, self).reset()
        self._k_iterations = 0

    def fit(self):
        super(IterativeClusterLearner, self).fit()
        self._k_iterations = self.refit_after

    def suggest_query(self, kind):

        # recluster after n iterations
        if not self._is_fitted or self._k_iterations <= 0:
            self.fit()

        self._k_iterations -= 1

        # do the same as CL
        return super(IterativeClusterLearner, self).suggest_query(kind)
