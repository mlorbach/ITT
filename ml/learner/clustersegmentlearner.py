# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-24 14:58:05
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-12-20 17:57:25

import numpy as np
import pandas as pd
import logging

from sklearn.preprocessing import LabelEncoder
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.pipeline import Pipeline
from MLUtilities.sklearn_extensions.scaling import RobustMinMaxScaler
from MLUtilities.annotation.utils import frame_to_segment_annotations, binarize_mutual_exclusive

from core.annotation import Annotation
from ml.learner.clusterlearner import ClusterLearner

logger = logging.getLogger(__name__)


class ClusterSegmentLearner(ClusterLearner):
    """
    Learner that samples segments that fall entirely within one cluster (i.e., segments that do not cross cluster borders).

    The sampler only allows segments to be sampled that have a minimum duration.

    (Cluster once in the beginning, then pick cluster, then pick start frame such that the following x seconds stay within cluster boundaries, query)


    Attributes
    ----------
    L : np.ndarray
        Mask of queried/not-queried frames
    S : pd.DataFrame
        Current segment structure (segmented based on cluster assignment C and L)
    segment_selection_strategy : str
        How to select a segment?
          - 'random': pick a random frame within the chosen cluster
          - 'min_sum_distance': pick the segment whose sum of squared distances to the cluster mean is smallest (sum over segment frames)
    """
    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2,
                 cluster_alg=None,
                 init='auto',
                 cluster_selection_strategy='random',
                 segment_selection_strategy='random'):
        super(ClusterSegmentLearner, self).__init__(
                project_ctrl,
                update_cycle=update_cycle,
                min_duration=min_duration,
                max_duration=max_duration,
                cluster_alg=cluster_alg,
                init=init,
                cluster_selection_strategy=cluster_selection_strategy,
                segment_selection_strategy=segment_selection_strategy
                )

        if segment_selection_strategy not in ['random', 'min_sum_distance']:
            logger.warning('Unknown segment selection strategy: {}. Falling back to "random".'.format(segment_selection_strategy))
            segment_selection_strategy = 'random'
        self.segment_selection_strategy = segment_selection_strategy
        self.S = None
        self.L = None

    def reset(self):
        super(ClusterSegmentLearner, self).reset()
        self.S = None
        self.L = None

    def _update_segments(self):
        # get what is labeled and make new segments
        self.L = ~self.project_ctrl.get_annotation().get_unlabeled_mask()

        logger.log(logging.DEBUG, 'Cluster assignment: {}, labeled mask: {}'.format(self.C.shape, self.L.shape))

        dfC = pd.DataFrame(np.stack((self.C, self.L), axis=1), index=self.project_ctrl.get_annotation().df.index, columns=['cluster', 'labeled'])

        self.S = frame_to_segment_annotations(dfC, action_col=['cluster', 'labeled'])

        self.S['duration'] = self.S['frame_stop'] - self.S.index.get_level_values('frame').values + 1

    def _select_cluster_fewest_labels(self, candidate_mask):
        # pick cluster with fewest labeled segments
        cluster_count = self.S.loc[self.S['cluster'].isin(np.where(candidate_mask)[0].tolist())].groupby('cluster')['labeled'].sum()
        return cluster_count.idxmin()

    def _select_segment(self, cluster_index):
        """
        Picks a random segment from the cluster.

        Parameters
        ----------
        cluster_index : int
            Index of cluster

        Returns
        -------
        int
            Index of chosen sample (location index of self.S)
        """

        # get all indices of samples in this cluster
        cluster_mask = ((self.S['cluster'] == cluster_index) &
                       (self.S['duration'] >= self.max_duration_frames))
        unlabeled_mask = self.S['labeled'] == 0

        # find segments that are in the chosen cluster and that are not labeled yet and that are at least as long as the desired duration
        valid_indices = np.where(cluster_mask & unlabeled_mask)[0]

        logger.log(logging.DEBUG, 'Found {} valid segments in total in this cluster'.format(len(valid_indices)))

        if self.segment_selection_strategy == 'min_sum_distance':

            # compute probability distribution over all valid start frames within the valid segments (sort of a sliding window)

            X = self.project_ctrl.get_features()
            meanC = self.cluster_alg.named_steps['clusterer'].cluster_centers_[cluster_index, :]

            D = []
            for segment_index in valid_indices:
                srSeg = self.S.iloc[segment_index]  # is series
                video, min_start = srSeg.name

                # squaerd distance of each frame to cluster center:
                d = (X.loc[(video, slice(min_start, srSeg['frame_stop'])), :].sub(meanC)**2).sum(axis=1)

                # sum of distances of every possible segment (rolling window)
                # we don't need to normalize the distance since all compared segments are of equal length (=max_duration_frames)
                D.append(d.rolling(window=self.max_duration_frames,
                                   min_periods=self.max_duration_frames).sum().shift(-(self.max_duration_frames-1)).dropna())

            D = pd.concat(D) # stitch results together
            video, start_frame = D.idxmin()
            end_frame = start_frame + self.max_duration_frames - 1

        else:  # random
            # pick at random
            seg = self.S.iloc[np.random.choice(valid_indices)]

            # latest start frame is end of segment minus duration
            video, min_start = seg.name
            max_start = seg['frame_stop'] - self.max_duration_frames + 1

            # pick a random start frame
            start_frame = np.random.randint(min_start, max_start + 1)
            end_frame = start_frame + self.max_duration_frames - 1

        return video, int(start_frame), int(end_frame)

    def suggest_query(self, kind):

        if not self._is_fitted:
            self.fit()

        # generate segment representation
        self._update_segments()

        # select cluster candidates
        # -------------------------
        # for each cluster, count how many unlabeled segments with the minimum duration there are

        u_count = np.bincount(self.S.loc[
                                (self.S['labeled'] == 0) &
                                (self.S['duration'] >= self.max_duration_frames),
                                'cluster'].values,
                                minlength=self.n_clusters)

        # don't sample from exhausted clusters
        c_candidate_mask = u_count > 0

        # determine cluster, segment, duration and start frame
        # ----------------------------
        selected_c = self._select_cluster(c_candidate_mask)
        logger.log(15, 'Selected cluster: {}'.format(selected_c))

        video, start_frame, end_frame = self._select_segment(selected_c)
        logger.log(15, 'Selected segment: {}, {}, {}'.format(video, start_frame, end_frame))

        # return
        # ------
        return video, start_frame, end_frame, None


class IterativeClusterSegmentLearner(ClusterSegmentLearner):
    """
    Cluster from scratch in every iteration, then pick cluster, then pick start frame such that the following x seconds stay within cluster boundaries, query.

    Attributes
    ----------
    refit_after : int
        Recluster after n iterations
    """
    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2,
                 cluster_alg=None,
                 init='auto',
                 cluster_selection_strategy='random',
                 segment_selection_strategy='random',
                 refit_after=1):
        super(IterativeClusterSegmentLearner, self).__init__(
                project_ctrl,
                update_cycle=update_cycle,
                min_duration=min_duration,
                max_duration=max_duration,
                cluster_alg=None,
                init=init,
                cluster_selection_strategy=cluster_selection_strategy,
                segment_selection_strategy=segment_selection_strategy)

        self.refit_after = refit_after
        self._k_iterations = 0

    def reset(self):
        super(IterativeClusterSegmentLearner, self).reset()
        self._k_iterations = 0

    def fit(self):
        super(IterativeClusterSegmentLearner, self).fit()
        self._k_iterations = self.refit_after

    def suggest_query(self, kind):

        # recluster after n iterations
        if not self._is_fitted or self._k_iterations <= 0:
            self.fit()

        self._k_iterations -= 1

        # do the same as CSL
        return super(IterativeClusterSegmentLearner, self).suggest_query(kind)
