# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 17:36:20
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:53:08

import logging

import numpy as np

from PyQt5.QtCore import pyqtSlot, pyqtSignal, QObject

from core.base import BaseObject
from core.project_ctrl import ProjectController
from ml.oracle import QueryResult

logger = logging.getLogger(__name__)


class BaseLearner(BaseObject, QObject):
    """
    A Learner learns a model by actively querying the label(s) of instances.

    The learner has two tasks:
      1. Suggest the next query (e.g., a video segment)
      2. Update the dataset and the detection model using the answered queries.

    The learner has access to the current detection model and to a dataset with unlabeled and potentially labeled instances from which it can generate queries.

    To suggest the next query, an external caller calls `suggest_query()`. The external caller is responsible for passing the query to the oracle.

    A model update is triggered on the `update()` slot. Again an external object needs to setup the corresponding connections.

    The learner emits an `updated_detection_model` signal, once the update is completed.

    This class is fully functional but it represents a very simple learner that suggests random video segments as queries. For more sophisticated learning strategies, sub-class this class and overwrite the appropriate methods (mainly `suggest_query()`, `_update_detection_model()`, and perhaps _update_dataset()`).

    Attributes
    ----------
    learner_state_changed : pyqtSignal
        Emitted if learner state changed
    learner_update_complete : pyqtSignal
        Emitted if learner update is completed
    max_duration : float
        Duration of segment to pick in seconds
    max_duration_frames : int
        Internally set
    min_duration : float
        Minimum duration of segment to pick in seconds
    min_duration_frames : int
        Internally set
    project_ctrl : ProjectController
        Controller instance giving access to dataset and detection model
    query_suggested : pyqtSignal
        Emit when learner suggests a query (note: only for logging purposes)
    trigger_update : pyqtSignal
        Triggers an update of the annotations and potentially detection model
    update_cycle : int
        After how many responses a detection model update is triggered
    update_error : pyqtSignal
        Emitted if update fails
    updated_detection_model : pyqtSignal
        Emitted after update of detection model
    """

    query_suggested = pyqtSignal(list)
    updated_detection_model = pyqtSignal()
    update_error = pyqtSignal(str)
    trigger_update = pyqtSignal(list)
    learner_state_changed = pyqtSignal(object)
    learner_update_complete = pyqtSignal()

    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2):
        super(BaseLearner, self).__init__()
        self.project_ctrl = project_ctrl
        self.update_cycle = update_cycle
        self._queued_responses_since_update = 0

        self.min_duration = min_duration  # in secs
        self.max_duration = max_duration  # in secs
        self._is_fitted = False
        self.trigger_update.connect(self.update)

    def get_learner_state(self):
        ''' to be overwritten in sub-classes '''
        return []

    def set_update_cycle(self, steps):
        self.update_cycle = max(1, steps)

    def reset(self):
        self._queued_responses_since_update = 0
        self._is_fitted = False

    def emit_learner_state_changed(self):
        self.learner_state_changed.emit(self.get_learner_state())

    def emit_query_suggested_log(self, data):
        self.query_suggested.emit(data)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Constructing queries
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def fit(self):
        self.min_duration_frames = int(np.round(self.min_duration * self.project_ctrl.get_metadata()['fps'], 0))
        self.max_duration_frames = int(np.round(self.max_duration * self.project_ctrl.get_metadata()['fps'], 0))
        self._is_fitted = True

    def suggest_query(self, kind):
        """
        Given the learner's current state, suggest the next item to query.

        Simple implementation of randomly selecting segments. Video and start frame are selected randomly (uniformly); duration is picked from normal distribution N(mu=75 frames, sigma=5 frames). Duration is at least 25 frames.

        Returns
        -------
        tuple(str, int, int)
            video name, start frame number, end frame number (incl.)
        """
        if not self._is_fitted:
            self.fit()

        videos = self.project_ctrl.get_video_collection()
        video_names = videos.loc[videos['n_valid_frames'] > 0].index.tolist()
        video = np.random.choice(video_names, 1)[0]

        info = videos.loc[video, :]
        start_frame = np.random.randint(info.first_valid_frame,
                                        info.last_valid_frame, 1)

        video, start_frame, end_frame = self.project_ctrl.get_dataset().get_valid_segment_tuple(video, self.max_duration_frames, start=start_frame, ensure_duration=True)

        logger.log(15, 'Selected segment: ({}, {}, {}), d={}'.format(video, start_frame, end_frame, end_frame-start_frame+1))

        self.emit_query_suggested_log([video, start_frame, end_frame])

        return video, start_frame, end_frame, None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Updating learning model (project)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @pyqtSlot(list)
    def update(self, query_results):
        self._update_dataset(query_results)
        self._queued_responses_since_update += len(query_results)

        # train detection model every n'th iteration
        if self._queued_responses_since_update >= self.update_cycle:
            self._update_detection_model()

        self.learner_update_complete.emit()

    @pyqtSlot(str)
    def project_update(self, what):
        if what in ['label_set', 'bootstrap']:
            self._update_detection_model()

    def _update_detection_model(self, query_results=None):
        """
        Update the referenced detection model after the dataset has been updated (e.g., after an oracle provided new label(s)).

        Returns
        -------
        TYPE
            Description
        """
        if self.project_ctrl.get_annotation().is_unlabeled():
            self._queued_responses_since_update = 0
            return

        self.project_ctrl.get_detection_model().train(self.project_ctrl)

        # force to update predictions and scores the next time we retrieve them
        self.project_ctrl._reset_cache()

        self._queued_responses_since_update = 0
        self.updated_detection_model.emit()

    def _update_dataset(self, query_results):
        """
        Update the referenced dataset with recently obtained new labels (e.g., from an oracle).

        Returns
        -------
        TYPE
            Description
        """

        # TODO: make more efficient for larger sets of results
        for result in query_results:
            self.project_ctrl.increment_learning_statistics(result.state)

            if result.state == QueryResult.LABELED:
                self.project_ctrl.update_annotation(result.query.value,
                                                    result.response_value)
            elif result.state == QueryResult.NEGATIVE_LABEL:
                self.project_ctrl.update_annotation(result.query.value,
                                                    result.response_value,
                                                    negative=True)

            elif result.state == QueryResult.UNCERTAIN:
                self.project_ctrl.set_annotation_uncertain(result.query.value)

            elif result.state == QueryResult.SEGMENTATION:
                pass  # don't know yet
