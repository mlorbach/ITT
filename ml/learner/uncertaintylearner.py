# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-28 13:40:31
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-30 23:33:52

import numpy as np
import pandas as pd
import logging

from sklearn.preprocessing import LabelEncoder
import scipy.stats
from MLUtilities.utils import argmin_set, argmax_k

from core.annotation import Annotation
from ml.metrics import softmax
from ml.oracle import QueryResult
from ml.learner.learner import BaseLearner
from ml.distancematrix import MinDistanceMatrix

logger = logging.getLogger(__name__)


class UncertaintyLearner(BaseLearner):
    """docstring for UncertaintyLearner"""
    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2,
                 class_selection_strategy='roundrobin'):
        super(UncertaintyLearner, self).__init__(project_ctrl,
                update_cycle=update_cycle,
                min_duration=min_duration,
                max_duration=max_duration)

        if class_selection_strategy not in ['random', 'roundrobin', 'min_labels']:
            raise ValueError('Unknown class selection strategy {}'.format(class_selection_strategy))
        self.class_selection_strategy = class_selection_strategy

        self._le = None

    def _update_internals(self):
        raise NotImplementedError('UncertaintyLearner is an abstract base class is not supposed to instantiated directly.')

    def fit(self):
        super(UncertaintyLearner, self).fit()
        self._le = LabelEncoder().fit(self.project_ctrl.get_label_set())
        self._n_classes = len(self._le.classes_)
        self._queries_per_class = [0] * len(self.project_ctrl.get_label_set())

    def _select_class(self):

        if self.class_selection_strategy == 'random':
            # pick a class at random
            picked_class = np.random.choice(range(len(self.n_classes)))

        elif self.class_selection_strategy == 'roundrobin':

            # pick class one-by-one (round robin)
            picked_class = argmin_set(self._queries_per_class)

            # if there are more one possible classes, pick randomly
            if picked_class.size > 1:
                picked_class = np.random.choice(picked_class)

        elif self.class_selection_strategy == 'min_labels':
            # pick the class with the least amount of labeled samples
            # picked_class = self._le.transform(self.project_ctrl.get_annotation().label_distribution().idxmin())

            dist = self.project_ctrl.get_annotation().label_distribution().values
            if np.sum(dist) > 0:
                p = dist / np.mean(dist, dtype=float)
                p = softmax(-p)
            else:
                p = None

            picked_class = np.random.choice(range(self._n_classes), p=p)

        self._queries_per_class[picked_class] += 1
        return int(picked_class)

    def _select_sample(self, class_label):
        raise NotImplementedError('UncertaintyLearner is an abstract base class is not supposed to instantiated directly.')

    def suggest_query(self, kind):

        self._to_log = []

        if not self._is_fitted:
            self.fit()

        # update internal data:
        # ----------------------
        self._update_internals()

        # pick class depending on selection strategy
        # ----------
        picked_class = self._select_class()

        logger.log(15, 'Target class: {} = {}'.format(picked_class, self._le.inverse_transform(picked_class)))

        # Pick sample
        # -----------
        video, mid_frame, extra = self._select_sample(picked_class, kind)

        video, start_frame, end_frame = self.project_ctrl.get_dataset().get_valid_segment_tuple(video, self.max_duration_frames, mid=mid_frame, ensure_duration=True)

        logger.log(15, 'Selected segment: ({}, {}, {}), m={}, d={}'.format(video, start_frame, end_frame, mid_frame, end_frame-start_frame+1))

        self.emit_query_suggested_log([video, start_frame, end_frame] + self._to_log)

        # return
        # ------
        return video, start_frame, end_frame, extra


##################################################
# Learners based on distance to labeled instances
##################################################


class MinDistanceLearner(UncertaintyLearner):
    """
    Make query decisions based on an uncertainty measure.
    """
    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2,
                 class_selection_strategy='roundrobin',
                 uncertainty_metric='min2',
                 uncertainty_level=0.5,
                 utility_metric='mask_label'):
        super(MinDistanceLearner, self).__init__(
                project_ctrl,
                update_cycle=update_cycle,
                min_duration=min_duration,
                max_duration=max_duration,
                class_selection_strategy=class_selection_strategy)

        if uncertainty_metric not in ['min2', 'sum_dist']:
            raise ValueError('Unknown uncertainty metric {}.'.format(uncertainty_metric))
        self.uncertainty_metric = uncertainty_metric

        if utility_metric not in ['mask_label']:
            raise ValueError('Unknown utility metric {}.'.format(utility_metric))
        self.utility_metric = utility_metric
        self.uncertainty_level = uncertainty_level

        self._level_sigma = 0.025
        self._need_update = []

        self._D = None

    def get_learner_state(self):
        ''' to be overwritten in sub-classes '''
        if self._D is not None and self._D.D is not None:
            return self._D.D.astype(np.float16) # make it a bit smaller
        else:
            return None

    def reset(self):
        super(MinDistanceLearner, self).reset()
        self._queries_per_class = [0] * len(self.project_ctrl.get_label_set())
        self._D = None

    def _compute_distance_matrix(self):
        # get labeled data
        dff = self.project_ctrl.get_features()

        dfa = self.project_ctrl.get_annotation().get_labeled().copy()

        dfa = dfa.reindex(dfa.index.intersection(dff.index)) # remove where dff has no data
        dfa = dfa.loc[(dfa == Annotation.ACTIVE).sum(axis=1) == 1, :]

        yL = self._le.transform(dfa.idxmax(axis=1).values)  # to numeric labels

        # unlabeled data
        U_idx = self.project_ctrl.get_annotation().get_unlabeled().iloc[::3].index

        # only samples with features
        U_idx = U_idx.intersection(dff.index)

        logger.info('Initialize distance matrix...')

        return MinDistanceMatrix.from_labeled_features(
                        dff=dff.loc[dfa.index.append(U_idx).sort_values(), :],
                        yL=yL,
                        Lidx=dfa.index,
                        Uidx=U_idx,
                        n_classes=self._n_classes)

    def _update_distance_matrix(self, srUpdate):
        logger.log(15, 'Update distance matrix. len(update) == {}'.format(len(srUpdate)))

        if len(srUpdate) == 0:
            logger.debug('Nothing to update.')
            return

        Lidx = srUpdate.index

        # drop the frames that are now labeled
        self._D.remove(Lidx)

        # remove UNCERTAIN frames from update set
        srUpdate = srUpdate.loc[srUpdate != -1]

        if len(srUpdate) > 0:
            Lidx = srUpdate.index # new index contains only labeled samples
            yL = self._le.transform(srUpdate.values)

            dff = self.project_ctrl.get_features()

            # update the distance matrix with the newly labeled frames
            self._D.update(dff, yL, Lidx)

        self.emit_learner_state_changed()

    def fit(self):
        super(MinDistanceLearner, self).fit()

        # initialize distance matrix
        self._D = self._compute_distance_matrix()

        self._need_update = []
        self.emit_learner_state_changed()

    def _retrieve_queued_updates(self):

        srList = []
        while(True):
            try:
                index, y = self._need_update.pop()
            except IndexError:
                break

            if y is None:
                y = -1  # using -1 as UNCERTAIN flag as we can't use UNCERTAIN because it maps to some integer number (I think 3)

            if isinstance(index, (pd.Index, pd.MultiIndex)):
                srList.append(pd.Series(data=y, index=index))
            elif hasattr(index, '__len__') and len(index) > 1:
                if isinstance(index, (list)):
                    srList.append(pd.Series(data=y, index=index))
                else:
                    srList.append(pd.Series(data=y, index=[index]))
            else:
                # scalar
                srList.append(pd.Series(data=y, index=index))

        if len(srList) == 0:
            # nothing to update
            logger.log(15, 'Update triggered but nothing to update.')
            return pd.Series()

        return pd.concat(srList, axis=0).sort_index()

    def _dist_entropy_sum(self, D_closest):
        """
        H = 1 / SUM_k( 1 / d_k )

        $$
        H = \frac{1}{\sum_k^C{\frac{1}{d_k}}}
        $$

        Parameters
        ----------
        D_closest : np.ndarray [n_classes, N]
            The distances of each of the N samples to the closest sample of each class k.

        Returns
        -------
        np.ndarray [N, ]
            H
        """
        return 1. / (1. / D_closest.D).sum(axis=0)

    def _dist_entropy_min2(self, D_closest):
        """
        Uncertainty measure is ratio between the two smallest distances (min := smallest; min_2 := the second smallest).

        H = min(D) / min_2(D)

        $$
        H = \frac{\min(D)}{\min_2(D)}
        $$

        Parameters
        ----------
        D_closest : ml.MinDistanceMatrix
            Description

        Returns
        -------
        np.ndarray [N, ]
            H
        """
        min_values = D_closest.D.min(axis=1)  # == min distances
        # min distances / second min distances =
        return min_values / D_closest.D.where(D_closest.D.gt(min_values, axis=0),).min(axis=1)

    def _utility_class_mask(self, D_closest, y):
        return D_closest.D.idxmin(axis=1).values == y

    def _update_internals(self):
        # update distance matrix if needed:
        # ----------------------
        if len(self._need_update) > 0:
            self._update_distance_matrix(self._retrieve_queued_updates())

    def _select_sample(self, class_label, query_type):

        # Compute uncertainty metric / Entropy
        # ------------------------------------
        if self.uncertainty_metric == 'sum_dist':
            H = self._dist_entropy_sum(self._D)
        elif self.uncertainty_metric == 'min2':
            H = self._dist_entropy_min2(self._D)

        # transform entropy to probability given desired uncertainty_level
        H = scipy.stats.norm.pdf(H, loc=self.uncertainty_level, scale=self._level_sigma)

        # compute class mask for unlabeled X
        if self.utility_metric == 'mask_label':
            M = self._utility_class_mask(self._D, class_label)

        # create sampling weights for unlabeled X
        p = H * M
        p = p / p.sum()

        logger.log(15, 'Pick sample from {} samples.'.format(np.sum(p > 0)))

        # pick a sample given weights
        # ---------------------------
        idx = np.random.choice(xrange(self._D.shape[0]), p=p)
        # logger.log(15, 'Picked sample idx {}'.format(idx))
        logger.log(15, 'Picked sample has an H = {:.3f}, p = {}'.format(H[idx], p[idx]))

        # construct segment index
        video, mid_frame = self._D.index[idx]
        extra = None
        return video, mid_frame, extra

    def _update_dataset(self, query_results):
        ''' overloaded from super class '''
        for result in query_results:
            if result.state in [QueryResult.LABELED, QueryResult.UNCERTAIN]:
                self._need_update.append((result.query.value, result.response_value))
        super(MinDistanceLearner, self)._update_dataset(query_results)


##################################################
# Learners based on confidence of detection model
##################################################


class ClassifierUncertaintyLearner(UncertaintyLearner):
    """docstring for ClassifierUncertaintyLearner"""
    def __init__(self, project_ctrl, update_cycle=3,
                 min_duration=1.5, max_duration=2,
                 class_selection_strategy='roundrobin',
                 score_normalization='softmax',
                 uncertainty_level=0.5,
                 uncertainty_level_sigma=0.025,
                 average_entropy_window=1):
        super(ClassifierUncertaintyLearner, self).__init__(
              project_ctrl,
              update_cycle=update_cycle,
              min_duration=min_duration,
              max_duration=max_duration,
              class_selection_strategy=class_selection_strategy)

        if score_normalization not in [None, 'None', 'softmax', 'cutoff']:
            raise ValueError('Invalid score_normalization option: {}'.format(score_normalization))

        self.score_normalization = score_normalization
        self.uncertainty_level = uncertainty_level
        if isinstance(self.uncertainty_level, str):
            self.ul_scheme = self.uncertainty_level
        else:
            self.ul_scheme = None
        self.average_entropy_window = average_entropy_window
        self.uncertainty_level_sigma = uncertainty_level_sigma
        self._P = None
        self._invalid_Ps = []
        self._P_needs_update = True

        self.updated_detection_model.connect(self._queue_P_update)

    def _queue_P_update(self):
        logger.debug('Detection model changed. Need to recompute P next update cycle.')
        self._P_needs_update = True

    def get_learner_state(self):
        ''' to be overwritten in sub-classes '''
        if self._P is not None:
            return self._P.astype(np.float16)  # make it a bit smaller
        else:
            return None

    def reset(self):
        super(ClassifierUncertaintyLearner, self).reset()
        self._P = None
        self._P_needs_update = True
        self._invalid_Ps = []

    def _update_dataset(self, query_results):
        ''' overloaded from super class '''
        for result in query_results:
            if result.state in [QueryResult.LABELED, QueryResult.NEGATIVE_LABEL, QueryResult.UNCERTAIN]:
                self._invalid_Ps += result.query.value.tolist()
        super(ClassifierUncertaintyLearner, self)._update_dataset(query_results)

    def fit(self):
        super(ClassifierUncertaintyLearner, self).fit()
        if not self.project_ctrl.get_detection_model().is_fitted():
            self.project_ctrl.get_detection_model().train(self.project_ctrl)

    def _update_internals(self):

        if not self.project_ctrl.get_detection_model().is_fitted():
            self.project_ctrl.get_detection_model().train(self.project_ctrl)

        if self._P_needs_update:

            logger.debug('Updating confidence matrix P.')

            # get index of every third unlabeled data point
            Pindex = self.project_ctrl.get_annotation().get_unlabeled().iloc[::3].index

            # get features
            dff = self.project_ctrl.get_features()

            # only samples with features
            Pindex = Pindex.intersection(dff.index)

            # obtain the detection model's certainty in the unknown samples
            try:
                # try if the model supports giving us properly normalized probabilities directly:
                self._P = pd.DataFrame(data=self.project_ctrl.get_detection_model().predict_proba(dff.loc[Pindex, :]),
                                       index=Pindex,
                                       columns=self._le.classes_)
            except (NotImplementedError, AttributeError):
                # if that fails, we'll use the decision function values and normalize them ourselves
                logger.debug('No normalized probabilities available. Falling back to decision_function().')
                self._P = pd.DataFrame(data=self.project_ctrl.get_detection_model().decision_function(dff.loc[Pindex, :]),
                                       index=Pindex,
                                       columns=self._le.classes_)

                # normalize decision function values using some chosen strategy
                if self.score_normalization == 'softmax':
                    self._P = softmax(self._P)
                elif self.score_normalization == 'cutoff':
                    self._P = self._P.clip(-1, 1)
                    self._P += 1
                    self._P /= 2.

            # convert seconds to number of frames
            roll_l = int(np.round(self.average_entropy_window * self.project_ctrl.get_metadata()['fps'], 0))
            if roll_l > 1:
                # compute the rolling mean of the probabilities in a triangle window around the center frame
                self._P = self._P.groupby(level='video',
                                          group_keys=False).rolling(roll_l,
                                          min_periods=1, center=True,
                                          win_type='triang').mean()

            self._invalid_Ps = []
            self._P_needs_update = False
            self.emit_learner_state_changed()

        elif len(self._invalid_Ps) > 0:
            logger.debug('Refresh P by removing the now labeled samples.')
            # only check for changes index, and kick out the old ones from P
            self._P = self._P.drop(self._invalid_Ps, errors='ignore')
            self._invalid_Ps = []

    @staticmethod
    def _sigmoid(x):
        return 1. / (1. + np.exp(-x))

    def _get_sample_weights(self, H, level, std):
        if std > 0:
            # transform entropy to probability given desired certainty_level
            p = scipy.stats.norm.pdf(H, loc=level, scale=std)
            p = p / p.sum()
        else:
            # when std == 0, do deterministic selection
            # set sample closest to level to 1, all others to 0
            p = np.zeros_like(H)
            p[np.argmin(np.abs(H.values - level))] = 1.
        return p

    def _select_sample(self, class_label, query_type):
        # select a sample with a particular uncertainty level, whose current prediction is the given class_label
        # if no sample for that class_label can be found, we fall back to selecting a random instance

        # samples for which the highest confidence is class_label:
        class_idx = np.where(np.argmax(self._P.values, axis=1) == class_label)[0]

        # sample for which the confidence in class_label is one of the two highest confidences
        # class_idx = np.where(self._P.values[:, class_label] >= argmax_k(self._P.values, k=2))[0]

        if len(class_idx) > 0:

            if self.ul_scheme is not None:

                if self.ul_scheme == 'increasing':
                    class_str = self._le.inverse_transform(class_label)
                    n = self.project_ctrl.get_annotation().label_distribution()
                    if class_str in n:
                        n = n[class_str]
                    ulevel = self._sigmoid((n - 200) / 100.) * .9
                elif self.ul_scheme == 'alternating':

                    if self._queries_per_class[class_label] % 2 == 0:
                        if self._queries_per_class[class_label] < 10:
                            ulevel = .4
                        else:
                            ulevel = .4
                    else:
                        ulevel = 1.
                elif self.ul_scheme == 'switch':

                    if self._queries_per_class[class_label] >= 10:
                        ulevel = .4
                    else:
                        if self._queries_per_class[class_label] % 2 == 0:
                            ulevel = .4
                        else:
                            ulevel = 1.

                logger.info('Setting ulevel for class {} accoring to scheme {:s} to {:.5f}.'.format(class_label, self.ul_scheme, ulevel))
            else:
                ulevel = self.uncertainty_level

            if ulevel is not None and ulevel >= 0:

                # entropy of those samples:
                # H = np.apply_along_axis(scipy.stats.entropy, 1, self._P.iloc[class_idx])
                # H /= np.log(len(self._le.classes_))  # scale to [0, 1]
                # p = self._get_sample_weights(H, ulevel, self.uncertainty_level_sigma)

                ## based on class confidence
                H = self._P.iloc[class_idx, class_label]
                p = self._get_sample_weights(H, ulevel, self.uncertainty_level_sigma)
                ##

                ## Breaking tie strategy
                # H = np.squeeze(np.abs(np.diff(np.sort(self._P.iloc[class_idx])[:, -2:])))
                # p = self._get_sample_weights(H, np.min(H), self.uncertainty_level_sigma)
                ##

                ## top 2
                # H = np.apply_along_axis(scipy.stats.entropy, 1, np.sort(self._P.iloc[class_idx])[:, -2:])
                # H /= np.log(2)  # scale to [0, 1]
                # p = np.ones_like(H)
                # p[H < ulevel] = 0.

                # if p.sum() == 0:
                #     logger.warning('No suitable samples where H < {}. Pick any.'.format(ulevel))
                #     p = None
                # else:
                #     p = p / p.sum()
                ## top2

                ## ratio
                # p = np.argmax(self._P.iloc[class_idx].values, axis=1) == class_label
                # #p[ diff(p1, p0) > .7 ] == 0  # todo: add filter
                # N = np.sum(p)
                # p[p == 1] = len(p) - N
                # p[p == 0] = N
                # p = p / float(np.sum(p))
                ##

                sample_idx = np.random.choice(len(class_idx), p=p)

                Hsample = H[sample_idx]
                # Hsample = p[sample_idx]

                # map back to original dataset index
                sample_idx = class_idx[sample_idx]

            else:
                # uncertainty level is disabled, pick random form target class
                sample_idx = np.random.choice(class_idx)
                Hsample = self._P.iloc[sample_idx, class_label]

        else:

            if self.uncertainty_level is not None and self.uncertainty_level >= 0:
                # fallback to max entropy of all samples
                logger.info('Failed to pick suitable class sample. Fall back to uncertainy level = 0.5.')

                # entropy of those samples:
                H = np.apply_along_axis(scipy.stats.entropy, 1, self._P)

                H /= np.log(len(self._le.classes_))  # scale to [0, 1]
                p = self._get_sample_weights(H, .5,
                                             self.uncertainty_level_sigma)
                sample_idx = np.random.choice(H.shape[0], p=p)

                Hsample = H[sample_idx]

            else:
                # uncertainty level is disabled, fallback to random
                logger.info('Failed to pick suitable class sample. Fall back to random.')

                sample_idx = np.random.choice(self._P.shape[0])
                Hsample = 1.

        # construct suggestion result
        extra = None
        confidence = self._P.iloc[sample_idx, :].tolist()

        # info for log
        self._to_log += [self._le.inverse_transform(class_label), Hsample]
        self._to_log += confidence

        if query_type == 'strong_ranked':
            extra = confidence  # return all class confidences
        elif query_type == 'weak':
            extra = (self._le.inverse_transform(class_label), confidence[class_label])  # return suggested label and its confidence

        logger.log(15,
               'Picked sample has an H = {:.3f}, p = {}.'.format(Hsample,
                                                                 confidence))

        return self._P.iloc[sample_idx].name + (extra, )
