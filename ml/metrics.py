# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-05 12:19:15
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-04 16:29:52


import pandas as pd
import numpy as np


def class_average(df_prfs, return_pdseries=False):
    if return_pdseries:
        if df_prfs['Support'].sum() == 0:
            return pd.Series(data=np.nan, index=df_prfs.columns[:3])
        else:
            return df_prfs.iloc[:, :3].mean(axis=0)
    else:
        if df_prfs['Support'].sum() == 0:
            return np.asarray([np.nan] * 3)
        else:
            return np.mean(df_prfs.values[:, :3], axis=0)


def frame_average(df_prfs, return_pdseries=False):
    if return_pdseries:
        if df_prfs['Support'].sum() == 0:
            return pd.Series(data=np.nan, index=df_prfs.columns[:3])
        else:
            return pd.Series(data=np.average(df_prfs.values[:, :3],
                                             weights=df_prfs.values[:, 3],
                                             axis=0),
                             index=df_prfs.columns[:3])
    else:
        if df_prfs['Support'].sum() == 0:
            return np.asarray([np.nan] * 3)
        else:
            return np.average(df_prfs.values[:, :3],
                              weights=df_prfs.values[:, 3], axis=0)


def softmax(Z, axis=-1):
    Zexp = np.exp(Z)
    Zsum = np.sum(Zexp, axis=axis, keepdims=True)
    return Zexp / Zsum
