# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-16 10:58:13
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:52:58

import logging
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject

from core.base import BaseObject

logger = logging.getLogger(__name__)


class QueryItem(object):
    """
    Rather universal item object with an identifier and a value.

    Both identifier and value can be of any type, e.g., `identifier` could be the frame number in a video and `value` the feature vector at that frame. They may also represent entire DataFrames, just do what you like.

    Attributes
    ----------
    identifier : any
        Some identifier that helps another class to determine which dataset item this item refers to.
    kind : str
        Specifies the type of this query, default is 'label'.
    value : any
        Some value attached to this query item.
    """
    def __init__(self, identifier, value, kind='label', parameter=None):
        super(QueryItem, self).__init__()
        self.identifier = identifier
        self.value = value
        self.kind = kind
        self.parameter = parameter

    def __str__(self):
        return '{}: {}({})'.format(self.__class__.__name__,
                                   self.identifier,
                                   self.value,
                                   self.kind,
                                   self.parameter)

    def __unicode__(self):
        return u'{}: {}({})'.format(self.__class__.__name__,
                                    self.identifier,
                                    self.value,
                                    self.kind,
                                   self.parameter)

    def __repr__(self):
        return '{}: {}({})'.format(self.__class__.__name__,
                                   self.identifier,
                                   self.value,
                                   self.kind,
                                   self.parameter)


class QueryResult(object):
    """
    Universal object representing the result of a query.

    QueryResult is an extension of the QueryItem with an additional result field. `result` can be of any type.

    Note that `value` is optional in instantiation as we may not need to pass on the value of the query item any further.

    Attributes
    ----------
    query : QueryItem
        The original query
    state : ResponseState
        The state of the response, see ResponseState
    response_value : any
        An attached value to the response
    """

    CANCELED = 0
    UNCERTAIN = 1
    LABELED = 2
    SEGMENTATION = 3
    NEGATIVE_LABEL = 4

    ResponseState = (
        (CANCELED, 'Canceled'),
        (UNCERTAIN, 'Uncertain'),
        (LABELED, 'Labeled'),
        (SEGMENTATION, 'SegmentationReject'),
        (NEGATIVE_LABEL, 'NegativeLabel')
    )

    def __init__(self, query=None, state=None, response_value=None):
        super(QueryResult, self).__init__()
        self.query = query
        self.state = state
        self.response_value = response_value

    def __str__(self):
        return '{}: {} = {}({})'.format(self.__class__.__name__,
                                        self.query,
                                        self.state,
                                        self.response_value)

    def __unicode__(self):
        return u'{}: {} = {}({})'.format(self.__class__.__name__,
                                         self.query,
                                         self.state,
                                         self.response_value)

    def __repr__(self):
        return '{}: {} = {}({})'.format(self.__class__.__name__,
                                        self.query,
                                        self.state,
                                        self.response_value)


class BaseOracle(QObject, BaseObject):
    """
    Abstract base class of an Oracle that is able to provide a labeling for a queried item.

    The derived class needs to overwrite _determine_response() to implement the actual processing of an incoming query.

    After processing, the oracle needs to call provide_label() with the corresponding query response.
    """

    responded = pyqtSignal(QueryResult)

    def __init__(self):
        super(BaseOracle, self).__init__()

        self._provides_query_kinds = []

    def reset(self):
        pass

    def provided_query_kinds(self):
        return self._provides_query_kinds

    def provides_query_kind(self, kind):
        return kind in self._provides_query_kinds

    def _determine_response(self, query_item):
        """
        Provides the functionality for processing an incoming query
        and respond to it in an approriate manner.

        Needs to be overwritten in sub-classes.

        Parameters
        ----------
        query_item : QueryItem
            Incoming query

        Returns
        -------
        QueryResult
            Outgoing query result
        """

        raise NotImplementedError('BaseOracle is an "abstract" base class and should be instantiated. Please sub-class and implement _determine_response().')

    @pyqtSlot(QueryItem)
    def receive_query(self, query_item):
        """
        Qt slot receiving labeling queries.

        Query is passed to self._determine_response() which handles the actual processing.

        Parameters
        ----------
        query_item : QueryItem
            Incoming query
        """
        logger.debug('Received query {}'.format(query_item))
        self.provide_result(self._determine_response(query_item))

    def provide_result(self, query_result):
        """
        Emits the oracle's response in form of a query result.

        Parameters
        ----------
        query_result : QueryResult
            Query response/result
        """
        logger.debug('Emit result: {}'.format(query_result))
        self.responded.emit(query_result)
