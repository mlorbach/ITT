# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-31 15:16:18
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-10-31 16:34:20

import pandas as pd
import numpy as np

from sklearn.datasets import make_blobs
from sklearn.cross_validation import ShuffleSplit

from ml.distancematrix import MinDistanceMatrix


def main():

    np.random.seed(0)

    centers = [[0, 1], [-2, -2], [1, -1]]
    n_samples = [10000, 15000, 5000]
    stds = [.8, 1.2, .5]
    n_clusters = len(centers)

    X = []
    y = []
    for c in range(n_clusters):
        xi, yi = make_blobs(n_samples=n_samples[c], centers=[centers[c]], cluster_std=stds[c], shuffle=False)
        X.append(xi)
        y.append(yi+c)
    X = np.concatenate(X)
    y = np.concatenate(y)

    # get some labeled data
    # L = np.random.randint(0, y.shape[0]+1, size=200)
    cv = ShuffleSplit(n=y.shape[0], n_iter=1, train_size=.1, test_size=None)
    for L, U in cv:
        pass

    # dataframe representation of features
    dff = pd.DataFrame(X)

    L = pd.Index(L).sort_values()
    U = pd.Index(U).sort_values()

    XL = dff.loc[L, :].values
    yL = y.take(L.values)
    XU = dff.loc[U, :].values
    yU = y.take(U.values)

    # XL, XU, yL, yU = train_test_split(X, y, train_size=.1)
    print 'Labeled size:', XL.shape
    print 'Unlabeled size:', XU.shape


    D = MinDistanceMatrix.from_labeled_features(dff, yL, L, U)

    print D.Dmin.iloc[:5, :5]
    print D.shape

    print 'Update!'
    print U[:5], yU[:5]

    D.update(dff, yU[:5], U[:5])
    print D.Dmin.iloc[:5, :5]
    print D.shape

    D.remove(U[:5])
    print D.Dmin.iloc[:5, :5]
    print D.shape


    print 'Done.'

if __name__ == '__main__':
    main()
