# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-11-30 17:24:07
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-04 11:54:54

import logging
import yaml

from ml.learner import *
from ml.dataset_oracle import DatasetOracle
from ml.human_annotation_oracle import HumanAnnotationOracle

logger = logging.getLogger(__name__)


class ConfigManager(object):
    """docstring for ConfigManager"""
    def __init__(self, config_file):
        super(ConfigManager, self).__init__()
        self.config_file = config_file
        self.cfg = {}

        if self.config_file is None:
            raise ValueError('config_file cannot be None.')

        self._read_config()

    def _read_config(self):
        with open(self.config_file, 'r') as fp:
            self.cfg = yaml.load(fp)

    def __getitem__(self, key):
        return self.cfg[key]

    def get_global_parameters(self):
        return {c: v for (c, v) in self.cfg.viewitems() if c not in ['Learner', 'Oracle', 'UI', 'Protocol']}

    def get_ui_parameters(self):
        if 'UI' in self.cfg:
            return self.cfg['UI']
        else:
            return {}

    def get_protocol_parameters(self):
        if 'Protocol' in self.cfg:
            return self.cfg['Protocol']['parameters']
        else:
            return {}

    def make_learner(self, project_ctrl):

        learner_type = self.cfg['Learner']['type']

        if learner_type == 'Random':

            # Random learner:
            learner = BaseLearner(
                            project_ctrl,
                            **self.cfg['Learner']['parameters'])

                            # min_duration=minimum_segment_duration,
                            # max_duration=desired_segment_duration)

        elif learner_type == 'ClusterLearner':

            # Cluster-based learner without boundary constraint
            learner = ClusterLearner(
                            project_ctrl,
                            **self.cfg['Learner']['parameters'])

                            # min_duration=minimum_segment_duration,
                            # max_duration=desired_segment_duration,
                            # init=cluster_initialization,
                            # cluster_selection_strategy=cluster_selection_strategy,
                            # segment_selection_strategy=segment_selection_strategy)

        elif learner_type == 'IterativeClusterLearner':

            # Cluster-based learner with cluster-refitting
            learner = IterativeClusterLearner(
                            project_ctrl,
                            **self.cfg['Learner']['parameters'])
                            # min_duration=minimum_segment_duration,
                            # max_duration=desired_segment_duration,
                            # init=cluster_initialization,
                            # cluster_selection_strategy=cluster_selection_strategy,
                            # segment_selection_strategy=segment_selection_strategy,
                            # refit_after=1)

        elif learner_type == 'ClusterSegmentLearner':

            # Cluster-based learner with boundary constraint (segments are not allowed to cross cluster boundaries)
            learner = ClusterSegmentLearner(
                            project_ctrl,
                            **self.cfg['Learner']['parameters'])
                            # min_duration=minimum_segment_duration,
                            # max_duration=desired_segment_duration,
                            # init=cluster_initialization,
                            # cluster_selection_strategy=cluster_selection_strategy,
                            # segment_selection_strategy=segment_selection_strategy)

        elif learner_type == 'IterativeClusterSegmentLearner':

            # Cluster-based learner with cluster-refitting and boundary constraint
            learner = IterativeClusterSegmentLearner(
                            project_ctrl,
                            **self.cfg['Learner']['parameters'])
                            # min_duration=minimum_segment_duration,
                            # max_duration=desired_segment_duration,
                            # init=cluster_initialization,
                            # cluster_selection_strategy=cluster_selection_strategy,
                            # segment_selection_strategy=segment_selection_strategy,
                            # refit_after=1)

        elif learner_type == 'MinDistanceLearner':

            # Distance-based learner
            learner = MinDistanceLearner(
                            project_ctrl,
                            **self.cfg['Learner']['parameters'])
                            # min_duration=minimum_segment_duration,
                            # max_duration=desired_segment_duration,
                            # class_selection_strategy=class_selection_strategy,
                            # uncertainty_metric='min2',
                            # uncertainty_level=uncertainty_level,
                            # utility_metric='mask_label')

        elif learner_type == 'ClassifierUncertaintyLearner':

            # Confidence-based learner
            learner = ClassifierUncertaintyLearner(
                            project_ctrl,
                            **self.cfg['Learner']['parameters'])

        else:
            raise ValueError('Unknown learner type: {}'.format(learner_type))

        return learner

    def make_oracle(self, project_ctrl, main_window=None):

        oracle_type = self.cfg['Oracle']['type']

        if oracle_type == 'HumanAnnotationOracle':
            oracle = HumanAnnotationOracle(project_ctrl, main_window)

        elif oracle_type == 'DatasetOracle':
            oracle = DatasetOracle(project_ctrl,
                                   **self.cfg['Oracle']['parameters'])

        else:
            raise ValueError('Unknown oracle type: {}.'.format(oracle_type))

        return oracle
