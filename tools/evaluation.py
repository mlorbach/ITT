# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-30 18:11:28
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-10-30 13:36:22

import pandas as pd
import numpy as np
from glob import glob
import os
from copy import copy
import logging
import json
from itertools import product
from sklearn.exceptions import NotFittedError

# data
from tools.local_datasets import get_metadata
from MLUtilities.dataset.io import load_features, load_frame_annotations
from sklearn.preprocessing import LabelEncoder
from MLUtilities.sklearn_extensions.transformers import LabelMergeTransform
from MLUtilities.annotation.utils import binarize_mutual_exclusive
from MLUtilities.utils import cast_to_number

# import
from tools.export import load_sklearn_model
from tools.stats_import import (read_query_log, read_response_log,
                                read_suggestions_log,
                                read_logged_label_sequence,
                                parse_session_log)

# evaluation
from sklearn.metrics import (precision_recall_fscore_support,
                             auc, confusion_matrix, log_loss,
                             coverage_error,
                             label_ranking_average_precision_score)
from MLUtilities.evaluation.metrics import top_k_recall

logger = logging.getLogger(__name__)


def get_groups_properties(groups):

    dfprops = {}
    group_names = []
    for group in groups:
        dfprops[group.name] = pd.Series(group.props)
        group_names.append(group.name)
    dfprops = pd.concat(dfprops, names=['group', 'property']).unstack()
    dfprops = dfprops.reindex(sorted(group_names))

    return dfprops


class EvaluationGroup(object):
    """docstring for EvaluationGroup"""
    def __init__(self, name, repetitions):
        super(EvaluationGroup, self).__init__()
        self.name = name
        self.repetitions = repetitions
        self.validation_data = None
        self.le_ = None
        self.props = self._parse_properties(self.name)

    @staticmethod
    def _parse_properties(name):
        try:
            return dict([
                        (k.strip(), cast_to_number(v.strip()))
                        for k, v in
                        (a.split('=') for a in name.split(','))
                        ])
        except:
            return {}

    def experiment_properties(self):
        d = {}
        repnames = []
        for rep in self.repetitions:
            if rep.exp_props is not None:
                d[rep.name] = rep.exp_props
            repnames.append(rep.name)
        df = pd.DataFrame(d).T
        if 'date' in df.columns:
            df['date'] = pd.to_datetime(df['date'])
        return df

    def __getitem__(self, i):
        return self.repetitions[i]

    def load_validation_data(self, dataset='PRSCA', suffix='_userexp_val', keep_uncertain=False):
        meta = get_metadata(dataset, suffix)

        dff = load_features(meta, file_suffix=meta['default_feature_file_suffix'])
        dfa = load_frame_annotations(meta, file_suffix=meta['default_annotation_file_suffix']).drop(['scorer', 'subject', 'Corrected by Malte', 'Genotype', 'Confidence'], errors='ignore', axis=1)
        lm = LabelMergeTransform(['Allogrooming', 'Nape attacking', 'Pinning', 'Social Nose Contact', 'Other'], 'Contact')
        dfa.loc[dfa['action'].isin(['Unknown']), 'action'] = 'Uncertain'
        if not keep_uncertain:
            dfa = dfa.loc[dfa['action'] != 'Uncertain', :]
        le = LabelEncoder()
        dfa['action'] = le.fit_transform(lm.fit_transform(dfa['action']))
        dfa = dfa['action']

        if not keep_uncertain:
            # interpolate max 1 sec, then drop any frame with invalid features
            dff = dff.interpolate(limit=meta['fps']).dropna(axis=0)

        dfindex = dfa.index.intersection(dff.index)
        if not keep_uncertain:
            dff = dff.reindex(dfindex)
            dfa = dfa.reindex(dfindex)
        else:
            dff = dff.reindex(dfindex).interpolate()
            dfa = dfa.reindex(dfindex).fillna(le.transform(['Uncertain'])[0]).astype(int)

        self.validation_data = (dff, dfa)
        self.le_ = le

    def set_feature_processing(self, featpr):
        for evitem in self.repetitions:
            evitem.featpr = featpr

    def compute_validation_scores(self):
        if self.validation_data is None:
            raise ValueError('Validation data not loaded.')

        for evitem in self.repetitions:
            evitem.compute_validation_scores(*self.validation_data)

    def validation_score(self, iteration=-1):
        if self.validation_data is None:
            raise ValueError('Validation data not loaded.')

        df = {}
        for evitem in self.repetitions:

            # check whether we have already computed them, then simply load, otherwise call validation_score(dff, dfa)

            if evitem.scores_ is not None:
                val_cols = [c for c in evitem.scores_.columns if (c.startswith('Val_') and '_classes' not in c and '_frames' not in c)]
            else:
                val_cols = []

            if len(val_cols) > 0:
                if iteration == -1:
                    iteration = evitem.scores_.tail(1).index[0]
                df[evitem.name] = evitem.scores_.loc[iteration, val_cols]
                df[evitem.name].index = df[evitem.name].index.str.split('_', expand=True).droplevel(0)
                df[evitem.name] = df[evitem.name].unstack(level=0).astype(float)
            else:
                df[evitem.name] = evitem.validation_score(*self.validation_data, iteration=iteration)
        return pd.concat(df, names=['rep', 'class'])

    def validation_score_metric(self, metric='log_loss', **kwargs):
        if self.validation_data is None:
            raise ValueError('Validation data not loaded.')

        df = {}
        for evitem in self.repetitions:
            df[evitem.name] = evitem.validation_score_metric(*self.validation_data, metric=metric, **kwargs)
        df = pd.Series(df, name=metric)
        df.index.name = 'rep'
        return df

    def validation_confusion_matrix(self, iteration=-1):

        if self.validation_data is None:
            raise ValueError('Validation data not loaded.')

        df = {}
        for evitem in self.repetitions:
            df[evitem.name] = evitem.validation_confusion_matrix(*self.validation_data, iteration=iteration)
        df = pd.concat(df, names=['rep', 'True'])
        return df

    def predictions(self, iteration=-1):

        if self.validation_data is None:
            raise ValueError('Validation data not loaded.')

        df = {}
        for evitem in self.repetitions:
            df[evitem.name] = evitem.predict(self.validation_data[0],
                                             iteration=iteration)
        df = pd.DataFrame(df)
        df.index.name = 'frame_index'
        df.columns.name = 'rep'
        return df

    def learning_curve(self, x='n_queries', y='F1', return_auc=False, n_iter=None):

        df = {}
        auc = {}
        for evitem in self.repetitions:
            if return_auc:
                df[evitem.name], auc[evitem.name] = evitem.learning_curve(x, y, return_auc, n_iter=n_iter)
            else:
                df[evitem.name] = evitem.learning_curve(x, y, return_auc, n_iter=n_iter)

        if return_auc:
            return (pd.concat(df, names=['rep', x]).sort_index(),
                    pd.concat(auc, names=['rep']).unstack(level=-1).sort_index())
        else:
            return pd.concat(df, names=['rep', x]).sort_index()

    def total_annotation_time(self, unit='minutes'):

        df = {}
        for evitem in self.repetitions:
            df[evitem.name] = evitem.total_annotation_time(unit=unit)
        return pd.Series(df, name='time')

    def target_response_matrix(self, include_uncertain=True):
        cm = []
        for evitem in self.repetitions:
            cm.append(evitem.target_response_matrix(include_uncertain))

        return np.stack(cm)

    def target_response_comparison(self, cumulative=False):
        df = {}
        for evitem in self.repetitions:
            df[evitem.name] = evitem.target_response_comparison(cumulative).reset_index(drop=True)
        return pd.concat(df, names=['rep', 'query_idx'])

    def queries(self):
        df = {}
        for evitem in self.repetitions:
            if evitem.queries_ is None:
                evitem.load_queries()
            df[evitem.name] = evitem.queries_
        return pd.concat(df, names=['rep', 'query_id'])

    def computational_time(self):
        df = {}
        for evitem in self.repetitions:
            if evitem.session_ is None:
                evitem.load_session_log()
            df[evitem.name] = evitem.session_.loc[:, ['training_time', 'prediction_time', 'prediction_time_test']].dropna(how='all')
        return pd.concat(df, names=['rep', 'iteration'])


class EvaluationItem(object):
    """docstring for EvaluationItem"""

    @property
    def name(self):
        return os.path.basename(self.input_path)

    def __init__(self, input_path, labels):
        super(EvaluationItem, self).__init__()
        self.input_path = input_path
        self.labels = labels
        self._le = LabelEncoder().fit(self.labels)
        self.featpr = None
        self.scores_ = None
        self.queries_ = None
        self.annotations_ = None
        self.predictions_ = None
        self.session_ = None
        self.exp_props = self._get_experiment_props()

    def clear_cache(self):
        self.scores_ = None
        self.queries_ = None
        self.annotations_ = None
        self.predictions_ = None

    def _get_experiment_props(self):
        expfile = os.path.join(self.input_path, 'experiment.json')
        if os.path.isfile(expfile):
            with open(expfile, 'r') as fp:
                return json.load(fp)
        else:
            return None

    def get_model(self, iteration=-1):
        try:
            if iteration == -1:
                dmodel_file = glob(os.path.join(self.input_path, 'models', 'detection_model_*.gz'))[0]
            else:
                dmodel_file = sorted(glob(os.path.join(self.input_path, 'logs', 'models', '*.gz')))[iteration]
            featpr_file = glob(os.path.join(self.input_path, 'models', 'feature_processor_*.gz'))[0]
            dmodel = load_sklearn_model(dmodel_file)
            featpr = load_sklearn_model(featpr_file)
        except IndexError:
            raise IndexError('Missing or invalid model files.')

        return featpr, dmodel

    def predict(self, dff, iteration=-1):
        featproc, dmodel = self.get_model(iteration)
        if self.featpr is not None: # use given feature pre-processor
            featproc = self.featpr
            logger.info('Using global feature pre-processor instead of trained model.')
        X = featproc.transform(dff)
        return dmodel.predict(X)

    def predict_proba(self, dff, iteration=-1):
        featproc, dmodel = self.get_model(iteration)
        if self.featpr is not None: # use given feature pre-processor
            featproc = self.featpr
            logger.info('Using global feature pre-processor instead of trained model.')
        X = featproc.transform(dff)
        return dmodel.predict_proba(X)

    def validation_score(self, dff, dfa, iteration=-1):
        # use trained model to predict novel videos
        # Steps:
        #  - load feature processing and detection model
        #  - load features and gt annotations
        #  - process features and predict
        #  - compute score

        y = dfa.values
        ypred = self.predict(dff, iteration=iteration)

        score = precision_recall_fscore_support(y, ypred, labels=range(len(self.labels)), pos_label=None)
        score = pd.DataFrame(data=np.vstack(score).T,
                             index=self.labels,
                             columns=['Precision', 'Recall', 'F1', 'Support'])
        return score

    def validation_score_metric(self, dff, dfa, metric='log_loss', **kwargs):

        y = dfa.values
        p = self.predict_proba(dff)

        if metric == 'log_loss':
            return log_loss(y, p, labels=range(len(self.labels)))

        elif metric == 'top_k':
            k = kwargs['k'] if 'k' in kwargs else 2
            return top_k_recall(y, p, labels=range(len(self.labels)), k=k).mean()

        elif metric == 'coverage_error':
            y_true_binary = binarize_mutual_exclusive(dfa, labels=range(len(self.labels))).iloc[:, :len(self.labels)]
            return coverage_error(y_true_binary, p)

        elif metric == 'label_ranking_average_precision_score':
            y_true_binary = binarize_mutual_exclusive(dfa, labels=range(len(self.labels))).iloc[:, :len(self.labels)]
            return label_ranking_average_precision_score(y_true_binary, p)

        else:
            return None

    def validation_confusion_matrix(self, dff, dfa, iteration=-1):

        y = dfa.values
        ypred = self.predict(dff, iteration)

        cm = confusion_matrix(y, ypred, labels=range(len(self.labels)))
        df = pd.DataFrame(cm, index=self.labels, columns=self.labels)
        df.index.name = 'True'
        df.columns.name = 'Predicted'
        return df

    def load_scores(self):
        """
        Load scores generated during learning process (train/test scores are from the dataset that was used in the framework.)
        """

        # specify data columns for dataframe
        columns = ['timestamp', 'n_queries', 'n_label_responses', 'secs_labeled']
        columns += ['frames_{}'.format(label) for label in self.labels]
        columns += ['Precision_frames', 'Recall_frames', 'F1_frames', 'Precision_classes', 'Recall_classes', 'F1_classes']
        classprec_cols = ['Precision_{}'.format(label) for label in self.labels]
        classrecall_cols = ['Recall_{}'.format(label) for label in self.labels]
        classf1_cols = ['F1_{}'.format(label) for label in self.labels]
        columns += classprec_cols
        columns += classrecall_cols
        columns += classf1_cols
        columns += ['Test_Precision_frames', 'Test_Recall_frames', 'Test_F1_frames',
                    'Test_Precision_classes', 'Test_Recall_classes', 'Test_F1_classes']
        columns += ['Test_' + c for c in classprec_cols]
        columns += ['Test_' + c for c in classrecall_cols]
        columns += ['Test_' + c for c in classf1_cols]

        filename = os.path.join(self.input_path, 'logs', 'model.csv')
        self.scores_ = pd.read_csv(filename, sep=';', header=None, names=columns, parse_dates=['timestamp'])

    def compute_validation_scores(self, dff, dfa):

        if self.scores_ is None:
            self.load_scores()

        val_scores = {}

        i = 0
        while(True):
            try:
                val_scores[i] = self.validation_score(dff, dfa, iteration=i)
                i += 1
            except IndexError:
                # IndexError means we have reached the last available model
                break
            except NotFittedError as err:
                logger.error("Model at iteration {} not fitted: {}.".format(i, self.input_path))
                break

        val_scores = pd.concat(val_scores, names=['iteration', 'class'])
        val_scores = val_scores.drop('Support', axis=1, errors='ignore')

        val_scores = val_scores.unstack(level=-1)
        cols = ['Val_{}_{}'.format(s, c) for s, c in product(*val_scores.columns.levels)]
        val_scores.columns = cols

        self.scores_ = self.scores_.join(val_scores)

    def load_queries(self):

        try:
            filename = os.path.join(self.input_path, 'logs', 'queries.csv')
            queries = read_query_log(filename).reset_index().set_index('query_id')

            filename = os.path.join(self.input_path, 'logs', 'responses.csv')
            responses = read_response_log(filename).reset_index().set_index('query_id')
            responses = responses.rename(columns={'timestamp': 'timestamp_response'})

            cols = ['target_label', 'normalized_entropy'] + ['p({})'.format(l) for l in self.labels]
            filename = os.path.join(self.input_path, 'logs', 'suggestions.csv')
            suggestions = read_suggestions_log(filename, extra_columns=cols)

            df = queries.join(responses).reset_index()
            df = df.merge(suggestions, on=['video', 'start', 'end'])

            df = df.set_index('query_id')
            self.queries_ = df
        except:
            logger.error('Failed to load queries for {}'.format(self.input_path))
            raise

    def load_session_log(self):
        try:
            filename = os.path.join(self.input_path, 'logs', 'session.log')
            self.session_ = parse_session_log(filename)
        except:
            logger.error('Failed to session log for {}'.format(self.input_path))
            raise

    def load_annotations(self):

        path = glob(os.path.join(self.input_path, 'exports', 'labels_*'))
        if len(path) > 0:
            path = path[0]
        else:
            raise IOError('Could not locate exported labels.')

        self.annotations_ = read_logged_label_sequence(path, self.labels)

    def learning_curve(self, x='n_queries', y='F1', return_auc=False, n_iter=None):
        # Learning curve (mean F1 over classes)

        if self.scores_ is None:
            self.load_scores()

        scores = self.scores_.set_index(x).copy()

        training_cols = [c for c in scores.columns if (c.startswith(y) and
                                                       '_classes' not in c and
                                                       '_frames' not in c)]
        test_cols = [c for c in scores.columns if (c.startswith('Test_{}'.format(y)) and
                                                   '_classes' not in c and
                                                   '_frames' not in c)]

        val_cols = [c for c in scores.columns if (c.startswith('Val_{}'.format(y)) and
                                                   '_classes' not in c and
                                                   '_frames' not in c)]

        df = pd.concat([scores.loc[:, training_cols].mean(axis=1),
                        scores.loc[:, test_cols].mean(axis=1),
                        scores.loc[:, val_cols].mean(axis=1)],
                       axis=1, keys=['Training', 'Test', 'Val'])

        if n_iter is not None:
            df = df.loc[:n_iter, :]

        if return_auc:
            return df, self.auc_learning_curve(df)
        else:
            return df

    @staticmethod
    def auc_learning_curve(df_lcurve, norm=True):
        # Area under learning curve
        area = df_lcurve.apply(lambda x: auc(x.index.values, x.values), axis=0)
        if norm:
            return area / (df_lcurve.index.max() - df_lcurve.index.min())
        else:
            return area

    @staticmethod
    def convert_timedelta(t, unit='minutes'):

        if unit not in ['seconds', 'minutes', 'hours', 'split']:
            raise ValueError('Units must be one if seconds, minutes, hours or split, but is {}.'.format(unit))

        try:
            seconds = t.total_seconds()
        except AttributeError:
            seconds = t

        if unit == 'minutes':
            minutes = seconds // 60
            seconds = (seconds % 60) / 60.
            return minutes + seconds  # floating point rep, e.g. 5.8min
        elif unit == 'hours':
            hours = seconds // 3600
            minutes = (seconds % 3600) / 3600.
            return hours + minutes  # floating point rep, e.g. 2.75 hours
        elif unit == 'seconds':
            return seconds
        else:
            # split in components
            hours = int(seconds // 3600)
            minutes = int((seconds % 3600) // 60)
            seconds = seconds % 3600 % 60
            return hours, minutes, seconds

    def total_annotation_time(self, unit='minutes'):
        # total annotation time
        # if self.scores_ is None:
        #     self.load_scores()

        if self.queries_ is None:
            self.load_queries()

        # total time is time between the first issued query and the response to the last query
        # t = self.queries_['timestamp_response'].max() - self.queries_['timestamp'].min()

        # total time is sum of all latencies
        t = self.queries_['response_latency'].sum()

        if unit is None:
            return t

        return self.convert_timedelta(t, unit=unit)

    def annotation_distribution(self):

        if self.annotations_ is None:
            self.load_annotations()

        dist1 = (self.annotations_ == 1).sum(axis=0)
        dist0 = (self.annotations_ == 0).sum(axis=0)

        return pd.concat((dist1, dist0), axis=1, keys=['positive', 'negative'])

    def target_response_matrix(self, include_uncertain=True):

        if self.queries_ is None:
            self.load_queries()

        labels = copy(self.labels)

        response = self.queries_.loc[:, 'response_value'].copy()

        is_weak_oracle = (self.queries_['response_state'] == 'NegativeLabel').any()

        if is_weak_oracle:
            labels += ['Not']
            response.loc[self.queries_['response_state'] == 'NegativeLabel'] = 'Not'
        elif include_uncertain:
            labels += ['None']

        if 'target_label' not in self.queries_.columns:
            # target_label is not available for learners that do not query for specific target classes (such as a Random Learner)
            # in this case, we treat every query as target label = "None"
            cm = confusion_matrix(['None']*len(response),
                                  response,
                                  labels=labels)
        else:
            # for all other learners
            cm = confusion_matrix(self.queries_.loc[:, 'target_label'],
                                  response,
                                  labels=labels)
        return cm

    def target_response_comparison(self, cumulative=False):

        if self.queries_ is None:
            self.load_queries()

        if 'target_label' not in self.queries_.columns:
            # target_label is not available for learners that do not query for specific target classes (such as a Random Learner)
            # in this case, we cannot compare target and response and return an empty DataFrame
            return pd.DataFrame()
        else:
            retdf = (self.queries_.loc[:, 'target_label'] == self.queries_.loc[:, 'response_value']) & (self.queries_.loc[:, 'response_state'] == 'Labeled')
            retdf.name = 'hit'
            if cumulative:
                return retdf.cumsum()
            else:
                return retdf
