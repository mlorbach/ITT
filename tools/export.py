# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-10-06 17:01:13
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-16 09:20:48

import pandas as pd
import os
import json
import logging
from copy import copy
from sklearn.externals import joblib

from core.project_ctrl import ProjectController
from core.annotation import Annotation
from core.utils import mkdir_p

logger = logging.getLogger(__name__)


def export_project_properties(project_ctrl, filename, extra_data=None):
    data = project_properties_to_dict(project_ctrl)

    if extra_data is not None and isinstance(extra_data, dict):
        data.update(extra_data)

    with open(filename, mode='wb') as fp:
        json.dump(data, fp, indent=4, sort_keys=True)


def project_properties_to_dict(project_ctrl):
    p = project_ctrl
    data = {}

    data['name'] = p.name
    data['storage'] = p.get_project_directory()
    data['project_file'] = p.get_project_file()
    data['videonames'] = p.get_video_names()
    data['label_set'] = p.get_label_set()
    data['detection_model_type'] = p.get_detection_model().get_type()
    data['detection_model_repr'] = repr(p.get_detection_model().model)
    data['feature_preprocessing_type'] = str(p.get_feature_processor().get_type())
    data['feature_preprocessing_repr'] = repr(p.get_feature_processor().model)
    data['videos'] = p.get_dataset().videos.to_dict('list')

    return data


def export_experiment_properties(experiment, filename, extra_data=None):
    data = {}
    data['project_name'] = experiment.project_name
    data['participant_name'] = experiment.participant_name
    data['date'] = experiment.date
    data['comment'] = experiment.comment

    if extra_data is not None and isinstance(extra_data, dict):
        experiment.update(extra_data)

    with open(filename, mode='wb') as fp:
        json.dump(data, fp, indent=4, sort_keys=True)


def export_sklearn_model(model, output_file, compress=True):
    mkdir_p(os.path.dirname(output_file))
    return joblib.dump(model, output_file, compress=compress)


def load_sklearn_model(input_file):
    return joblib.load(input_file)


def export_annotation_to_csv(annotation, output_dir,
                             filepattern, fps,
                             event_format='segments',
                             export_unlabeled=True):

    if not isinstance(annotation, Annotation):
        raise TypeError('Can only export from Annotation object, but been given a {}.'.format(type(annotation)))

    valid_event_formats = ['segments', 'frames']
    if event_format not in valid_event_formats:
        raise KeyError('event_format must be one of {}, but is {}.'.format(valid_event_formats, event_format))

    files_written = []

    for video in annotation.video_names:

        if not export_unlabeled and (annotation.get_video(video) == Annotation.UNLABELED).all().all():
            logger.debug('Skipping export of video {} because it is entirely unlabeled.'.format(video))
            continue

        if event_format == 'segments':

            dfs = Annotation.frames_to_segments_multilabel(annotation.get_video(video))
            if len(dfs) == 0:
                continue

            # before converting frames to time, we need to increment the frame count of the stop event; this is to prevent "one-frame-gaps" between two segments that would occur because the times represent intervals whereas the frames represent indexes.
            dfs.loc[dfs['Event type'] == 'Stop event', 'frame'] += 1
            dfs['time'] = dfs['frame'] / float(fps)
            # dfs = dfs.set_index('time').drop('frame', axis=1)
            dfs = dfs.drop('frame', axis=1)
            dfs = dfs.reindex_axis(['time', 'Behavior', 'Event type'], axis=1)

        elif event_format == 'frames':
            dfs = annotation.get_video(video)

        mkdir_p(output_dir)

        outfile = os.path.join(output_dir, filepattern.format(video=video))
        dfs.to_csv(outfile, sep=';', index=True,
                   date_format='%H:%M:%S.%f')
        files_written.append(outfile)

    return files_written
