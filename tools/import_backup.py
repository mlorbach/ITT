# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-08-21 12:36:29
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-21 15:41:34

import os
from glob import glob

import pandas as pd

from tools.export import load_sklearn_model
from tools.stats_import import read_response_log
from ml.oracle import QueryResult
from core.utils import copy_folder


def import_detection_model(project_ctrl, filename):
    # load last from models/.. and replace current
    mdl = load_sklearn_model(filename)
    project_ctrl.replace_detection_model(mdl)


def import_feature_processor(project_ctrl, filename):
    # load last from models/.. and replace current
    mdl = load_sklearn_model(filename)
    project_ctrl.replace_feature_processing(mdl)


def import_labeling_stats(project_ctrl):
    # parse statistics from log files

    # reset current stats
    project_ctrl.get_project().learning_stats.reset_learning_stats()

    # read model.csv for accuracies
    acc_file = os.path.join(project_ctrl.get_project_directory(), 'logs', 'model.csv')
    acc_cols = ['Iteration', 'Precision', 'Recall', 'F1', 'Precision_test', 'Recall_test', 'F1_test']
    dfacc = pd.read_csv(acc_file, sep=';', header=None, usecols=[1, 8, 9, 10, 29, 30, 31])
    dfacc.columns = acc_cols

    project_ctrl.get_project().learning_stats.accuracy_history = dfacc.copy()

    # read query responses
    resp_file = os.path.join(project_ctrl.get_project_directory(), 'logs', 'responses.csv')
    dfr = read_response_log(resp_file)
    rcounts = dfr['response_state'].value_counts()

    # map to response states (numeric)
    state_map = dict((y, x) for x, y in QueryResult.ResponseState)
    rcounts.index = rcounts.index.to_series().map(state_map)
    project_ctrl.get_project().learning_stats.query_response_stats.update(rcounts.to_dict())
    project_ctrl.get_project().learning_stats.n_query_iterations = rcounts.sum()


def import_logs(project_ctrl, folder):
    # reset current logs, copy from backup to current
    copy_folder(folder, os.path.join(project_ctrl.get_project_directory(), 'logs'))


def import_annotations(project_ctrl, label_files):
    # reset current annotations, update from csvs
    project_ctrl.get_dataset().clear_annotations()
    project_ctrl.load_annotations_from_files(label_files)