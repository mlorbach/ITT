# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-22 16:56:15
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-10-27 16:22:57

import os
import json
from glob import glob

_basepath_json = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), r"../../phd_code/.datasets"))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def get_basepaths(filename=None):

    if filename is not None:
        filename = os.path.join(_basepath_json, filename)
        if not os.path.isfile(filename):
            raise IOError('Basepath config file not found: "{}".'.format(filename))
    else:
        path_files = glob(os.path.join(_basepath_json, '*.json'))

        if len(path_files) == 0:
            raise IOError('Could not find any basepath config files in "{}".'.format(_basepath_json))
        elif len(path_files) == 1:
            filename = path_files[0]
        elif len(path_files):
            # remove "default.json"

            path_files = [p for p in path_files if os.path.basename(p) != "default.json"]

            if len(path_files) > 1:
                raise ValueError('Multiple basepath config files found: {}. Please specify which one you want to use.'.format(path_files))
            else:
                filename = path_files[0]

    with open(filename, 'rb') as fp:
        return json.load(fp)


def get_metadata(dataset, dinfo_suffix=''):
    """
    Load metadata of dataset.

    Parameters
    ----------
    dataset : str
        Name of dataset for which to load metadata

    Returns
    -------
    dict
        Dataset metadata dictionary

    Raises
    ------
    NotImplementedError
        Unsupported dataset
    ValueError
        Unknown dataset
    """

    basepaths = get_basepaths()

    if dataset.lower() in basepaths:
        _basepath = basepaths[dataset.lower()]
    else:
        raise ValueError('Unknown dataset: {}.'.format(dataset))

    # specify and load metadata and dataset info
    _metadata_file = os.path.join(_basepath, 'metadata.json')
    _dinfo_file = os.path.join(_basepath, 'dinfo{}.json'.format(dinfo_suffix))
    with open(_metadata_file) as fp:
        _metadata = json.load(fp)

    if os.path.isfile(_dinfo_file):
        with open(_dinfo_file) as fp:
            _dinfo = json.load(fp)
        _metadata.update(_dinfo)
    else:
        raise IOError('Could not find dinfo file: {}'.format(_dinfo_file))

    _metadata['basepath'] = _basepath

    return _metadata

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
