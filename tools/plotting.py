# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-30 18:43:08
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-01-31 15:18:12

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

from itertools import cycle, combinations

from MLUtilities.visualization.matrix import plot_matrix
from MLUtilities.utils import cast_to_number

from scipy.stats import ttest_ind

from .evaluation import EvaluationGroup, EvaluationItem, get_groups_properties


def plot_groups_validation_scores(groups, kind='bar', sort=False, **kwargs):

    sns.set('talk', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1)

    df = {}
    group_names = []
    for group in groups:
        df[group.name] = group.validation_score()
        group_names.append(group.name)
    df = pd.concat(df, names=['group', 'rep', 'class'])

    df = df.groupby(level=['group', 'rep']).mean()

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)

    if sort is True or sort == 'asc':
        order = df.groupby(level='group').mean()['F1'].sort_values().index.tolist()
    elif sort == 'desc':
        order = df.groupby(level='group').mean()['F1'].sort_values().index.tolist()[::-1]
    else:
        order = None
    g = sns.factorplot(x='group', y='F1', data=df.reset_index(), units='rep',
                       kind=kind, order=order, **kwargs)
    ax = g.ax

    # fig = plt.figure(figsize=(8, 5))
    # ax = fig.gca()
    # sns.barplot(x='group', y='F1', data=df.reset_index(), ax=ax, **kwargs)

    if kind in ['bar']:
        # mean_values = df.groupby(level='group')['F1'].mean()
        for i, _ in enumerate(group_names):
            p = ax.patches[i]
            x = p.get_x() + p.get_width() / 2.
            y = p.get_height()
            ax.text(x, y + 0.05, '{0:.3f}'.format(y),
                    horizontalalignment='center',
                    verticalalignment='bottom',
                    fontdict={'size': 12})

    ax.set_ylim(0,)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=20, ha='right',
                       fontdict={'size':10})
    ax.set_xlabel('')
    ax.set_ylabel('Avg. F1')
    sns.despine()

    return g


def plot_groups_validation_score_metric(groups, metric='log_loss', metric_kwargs={}, **kwargs):

    sns.set('poster', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1)

    df = {}
    group_names = []
    for group in groups:
        df[group.name] = group.validation_score_metric(metric, **metric_kwargs)
        group_names.append(group.name)
    df = pd.concat(df, names=['group', 'rep']).reset_index()

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)

    if 'ax' in kwargs:
        ax = kwargs['ax']
        del kwargs['ax']
    else:
        fig = plt.figure(figsize=(8, 5))
        ax = fig.gca()

    sns.barplot(x='group', y=metric, data=df, ax=ax, **kwargs)

    ax.set_ylim(0,)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=20, ha='right',
                       fontdict={'size':10})
    ax.set_xlabel('')
    ax.set_ylabel(metric.replace('_', ' '))
    sns.despine()

    return ax


def plot_groups_confusion_matrices(groups, iteration=-1, **kwargs):
    sns.set('poster', 'whitegrid',
            rc={"grid.linestyle": ":", 'legend.frameon': True},
            font_scale=.8)

    n_groups = len(groups)
    label_set = groups[0][0].labels

    ncols = 2
    nrows = sum(divmod(n_groups, ncols))
    fig, axs = plt.subplots(nrows, ncols, sharex=False, sharey=True,
                            figsize=(ncols * 6, nrows * 6))

    for i, group in enumerate(groups):

        ax = axs.flat[i]
        plot_matrix(group.validation_confusion_matrix(iteration=iteration).groupby(level='True').sum().values,
                    norm='recall', cmap='Greys', ax=ax, colorbar=None,
                    target_names=label_set,
                    label_rotation_deg=20)
        ax.set_title('')
        ax.set_xlabel(group.name)

        if i % ncols == 0:
            ax.set_ylabel('True')
        else:
            ax.set_ylabel('')

        if i >= ncols:  # hide xticklabels in all but first row
            ax.set_xticklabels([])

    for i in range(n_groups % ncols):
        # delete additional axes
        fig.delaxes(axs.flat[-1])

    fig.subplots_adjust(hspace=.15, wspace=.1)

    return fig, axs


def plot_groups_learning_curves(groups, x='n_queries', y='F1',
                                plot=['Training', 'Test'],
                                plot_markers=False,
                                baselines=None,
                                clip_iter=None,
                                order=None,
                                **kwargs):

    sns.set('poster', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1)

    if 'err_style' not in kwargs:
        kwargs['err_style'] = 'ci_band'

    if 'linestyle' not in kwargs:
        kwargs['linestyle'] = '-'

    dflc = {}
    group_names = []
    for group in groups:
        dflc[group.name] = group.learning_curve(x, y, n_iter=clip_iter)
        group_names.append(group.name)
    dflc = pd.concat(dflc, names=['group', 'rep', x])

    if order is not None:
        dflc = dflc.reindex(order, level='group')
    dflc = dflc.reset_index()

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)
    fig = plt.figure(figsize=(8, 5))
    ax = fig.gca()

    for pname in plot:
        sns.tsplot(dflc, time=x, unit='rep', condition='group', value=pname,
                   marker='o' if plot_markers else '', markevery=5, clip_on=False, zorder=5,
                   **kwargs)

    ax.set_ylabel(y.replace('_', ' '))
    ax.set_ylim(0,)
    sns.despine()

    if plot_markers:
        # for each model chose a unique marker
        markers = cycle(['s', 'o', '^', 'v', '*', '<', '>', 'd', 'p', 'D'])

        # map the same marker to the same group (e.g., train and test)
        marker_map = {}
        for group in group_names:
            marker_map[group] = markers.next()

        # apply markers to each line
        for iline in range(len(ax.lines)):
            if ax.lines[iline].get_label() in marker_map:
                ax.lines[iline].set_marker(marker_map[ax.lines[iline].get_label()])

    if baselines is not None:
        lstyles = cycle(['--', ':', '-.'])
        bl_keys = sorted(baselines.keys())
        for k in bl_keys:
            v = baselines[k]
            ax.hlines(v, *ax.get_xlim(), linestyles=lstyles.next(),
                      label=k, zorder=0)

    # remove duplicate legend entries (train/test)
    handlers, labels = ax.get_legend_handles_labels()
    if len(plot) > 1:
        handlers = handlers[:n_groups]
        labels = labels[:n_groups]
    ax.legend(handlers, labels, bbox_to_anchor=(1, 1), loc='upper left',
              title='Groups:')

    return ax


def plot_groups_learning_curves_variance(groups, x='n_queries', y='F1',
                                         plot='Test',
                                         plot_markers=False,
                                         **kwargs):

    sns.set('poster', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1)

    if 'linestyle' not in kwargs:
        kwargs['linestyle'] = '-'

    dflc = {}
    group_names = []
    for group in groups:
        dflc[group.name] = group.learning_curve(x, y)
        group_names.append(group.name)
    dflc = pd.concat(dflc, names=['group', 'rep', x])

    dflc = dflc.groupby(level=['group', x])[plot].var()

    print 'Average variance: '
    print dflc.groupby(level='group').mean()

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)
    fig = plt.figure(figsize=(6, 4))
    ax = fig.gca()

    if plot_markers:
        # for each model chose a unique marker
        markers = cycle(['s', 'o', '^', 'v', '*', '<', '>', 'd', 'p', 'D'])

        for g, df in dflc.groupby(level='group'):

            df.loc[g, :].plot(ax=ax, marker=markers.next(), markevery=5, label=g, **kwargs)
    else:
        dflc.groupby(level='group').plot(ax=ax, **kwargs)

    ax.set_ylim(0,)
    ax.set_ylabel('Variance ({})'.format(y))
    sns.despine()

    # remove duplicate legend entries (train/test)
    handlers, labels = ax.get_legend_handles_labels()
    if len(plot) > 1:
        handlers = handlers[:n_groups]
        labels = labels[:n_groups]
    ax.legend(handlers, labels, bbox_to_anchor=(1, 1), loc='upper left',
              title='Groups:')

    return ax


def plot_groups_auc(groups, x='group', y='F1', plot='Test',
                    kind='bar', sort=False, n_iter=None, **kwargs):

    sns.set('talk', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1)

    auc = {}
    group_names = []
    for group in groups:
        _, auc[group.name] = group.learning_curve('n_queries', y, return_auc=True, n_iter=n_iter)
        group_names.append(group.name)
    auc = pd.concat(auc, names=['group', 'rep'])

    dfprop = get_groups_properties(groups)
    auc = auc.reset_index().merge(dfprop.reset_index(), left_on='group', right_on='group').set_index(['group', 'rep'])

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)

    if 'order' in kwargs:
        order = kwargs['order']
        del kwargs['order']
    else:
        order = None

    if sort is True or sort == 'asc':
        order = auc.groupby(level=x).mean()[plot].sort_values().index.tolist()
    elif sort == 'desc':
        order = auc.groupby(level=x).mean()[plot].sort_values().index.tolist()[::-1]

    g = sns.factorplot(x=x, y=plot, data=auc.reset_index(), units='rep',
                       kind=kind, order=order, **kwargs)
    ax = g.ax
    ax.set_ylim(0,)

    if kind == 'bar':
        # mean_values = auc.groupby(level=x).mean()[plot]
        for i, _ in enumerate(group_names):
            p = ax.patches[i]
            xp = p.get_x() + p.get_width() / 2.
            yp = p.get_height()
            ax.text(xp, yp + 0.05, '{0:.3f}'.format(yp),
                    horizontalalignment='center',
                    verticalalignment='bottom',
                    fontsize=12)

    # if n_groups == 2:
    #     a = auc.loc[group_names[0], :]
    #     b = auc.loc[group_names[1], :]
    #     print 'p = {:.6f}'.format(ttest_ind(a, b, equal_var=False)[1])

    ax.set_ylabel('AUC')
    ax.set_xlabel('')
    sns.despine()

    return g


def plot_groups_auc_numeric(groups, x, y='F1', plot='Test', **kwargs):

    sns.set('talk', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=.9)

    auc = {}
    group_names = []
    for group in groups:
        _, auc[group.name] = group.learning_curve('n_queries', y, return_auc=True)
        group_names.append(group.name)
    auc = pd.concat(auc, names=['group', 'rep'])

    dfprop = get_groups_properties(groups)
    auc = auc.reset_index().merge(dfprop.reset_index(), left_on='group', right_on='group').set_index(['group', 'rep'])

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)

    # ax = sns.tsplot(time=x, value=plot, data=auc.reset_index(), unit='rep', **kwargs)

    fig = plt.figure(figsize=(4,3))
    ax = fig.gca()
    ax.set_xscale('log')
    ax.errorbar(auc.loc[:, x].unique(), auc.groupby(x)[plot].mean().values, yerr=auc.groupby(x)[plot].std().values, fmt='-o', **kwargs)

    # g = sns.factorplot(x=x, y=plot, data=auc.reset_index(), units='rep',
                       # kind=kind, **kwargs)
    # ax = g.ax
    ax.set_ylim(0,)

    # if kind == 'bar':
    #     mean_values = auc.groupby(level='group').mean()
    #     for i, v in enumerate(mean_values):
    #         p = ax.patches[i]
    #         x = p.get_x() + p.get_width() / 2.
    #         y = p.get_height() + 0.05
    #         ax.text(x, y, '{0:.3f}'.format(y),
    #                 horizontalalignment='center',
    #                 verticalalignment='bottom')

    if n_groups == 2:
        a = auc.loc[group_names[0], :]
        b = auc.loc[group_names[1], :]
        print 'p = {:.6f}'.format(ttest_ind(a, b, equal_var=False)[1])

    ax.set_ylabel('AUC')
    ax.set_xlabel('')
    sns.despine()

    return ax


def plot_groups_auc_pvalue(groups, x='n_queries', y='F1', tset='Test'):
    auc = {}
    group_names = []
    for group in groups:
        _, auc[group.name] = group.learning_curve(x, y, return_auc=True)
        group_names.append(group.name)
    auc = pd.concat(auc, names=['group', 'rep']).loc[:, tset]

    group_names = sorted(group_names)
    n_groups = len(group_names)

    pmat = np.zeros((n_groups, n_groups), dtype=float)
    for g1, g2 in combinations(range(n_groups), 2):
        a = auc.loc[group_names[g1], :]
        b = auc.loc[group_names[g2], :]
        pmat[g1, g2] = ttest_ind(a, b, equal_var=False)[1]

    fig, ax, _ = plot_matrix(pmat, target_names=group_names, cmap='Greys',
                             hide_threshold=0.00001,
                             colorbar=None,
                             label_rotation_deg=20)

    ax.set_title('')
    ax.set_ylabel('')
    ax.set_xlabel('')

    return ax


def plot_groups_total_annotation_time(groups, unit='minutes', hue=None, kind='box', **kwargs):

    sns.set('talk', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1.2)

    df_tot_anno_time = {}
    group_names = []
    for group in groups:
        df_tot_anno_time[group.name] = group.total_annotation_time(unit=unit)
        group_names.append(group.name)
    df_tot_anno_time = pd.concat(df_tot_anno_time, names=['group', 'rep']).reset_index()

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)

    g = sns.factorplot(x='group', y='time',
                       data=df_tot_anno_time, hue=hue,
                       kind=kind, **kwargs)

    ax = g.ax
    if kind == 'bar':
        for i, _ in enumerate(ax.patches):
            p = ax.patches[i]
            xp = p.get_x() + p.get_width() / 2.
            yp = p.get_height()
            ax.text(xp, yp * 1.3, '{0:.2f}'.format(yp),
                    horizontalalignment='center',
                    verticalalignment='bottom',
                    fontsize=12)

    g.set_axis_labels('', 'Duration ({})'.format(unit))
    g.ax.set_ylim(0, )
    sns.despine()

    return g


def plot_groups_target_response_curve(groups, plot_markers=False, **kwargs):

    sns.set('poster', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1.1)

    if 'err_style' not in kwargs:
        kwargs['err_style'] = 'ci_band'

    if 'linestyle' not in kwargs:
        kwargs['linestyle'] = '-'


    df = {}
    group_names = []
    for group in groups:
        df[group.name] = group.target_response_comparison(cumulative=True)
        group_names.append(group.name)
    df = pd.concat(df, names=['group', 'rep', 'query_idx']).reset_index()

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)
    fig = plt.figure(figsize=(8, 5))
    ax = fig.gca()

    sns.tsplot(time='query_idx', value='hit', data=df, condition='group',
               unit='rep', ax=ax, **kwargs)

    maxqidx = df['query_idx'].max()
    ax.set_ylabel('Correctly predicted target labels')
    ax.set_xlabel('Queries')
    ax.set_ylim(0, maxqidx)
    ax.set_aspect('equal')
    sns.despine()

    if plot_markers:
        # for each model chose a unique marker
        markers = cycle(['s', 'o', '^', 'v', '*', '<', '>'])

        # map the same marker to the same group (e.g., train and test)
        marker_map = {}
        for group in group_names:
            marker_map[group] = markers.next()

        # apply markers to each line
        for iline in range(len(ax.lines)):
            if ax.lines[iline].get_label() in marker_map:
                ax.lines[iline].set_marker(marker_map[ax.lines[iline].get_label()])

    ax.plot([0, maxqidx], [0, maxqidx], 'k--')

    ax.legend(bbox_to_anchor=(1, 1), loc='upper left',
              title='Groups:')

    return ax


def plot_groups_target_response_precision(groups, x='CL', hue=None,
                                          kind='point', **kwargs):

    sns.set('poster', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1)

    dfprop = get_groups_properties(groups)
    df = {}
    group_names = []
    for group in groups:

        df[group.name] = pd.DataFrame(group.target_response_comparison(cumulative=True)).unstack(level='rep')
        df[group.name] = df[group.name].iloc[-1:].stack(level='rep')
        group_names.append(group.name)
    df = pd.concat(df, names=['group', 'query_idx', 'rep']).reset_index()
    df = df.merge(dfprop.reset_index(), left_on='group', right_on='group')

    df['hit'] /= df['query_idx'] + 1

    group_names = sorted(group_names)
    n_groups = len(group_names)

    sns.set_palette("Set1", n_colors=n_groups)
    g = sns.factorplot(x=x, y='hit', hue=hue, data=df,
                       kind=kind, **kwargs)
    ax = g.ax

    if kind == 'bar':
        # mean_values = auc.groupby(level=x).mean()[plot]
        for i, _ in enumerate(group_names):
            p = ax.patches[i]
            xp = p.get_x() + p.get_width() / 2.
            yp = p.get_height()
            ax.text(xp, yp + 0.05, '{0:.3f}'.format(yp),
                    horizontalalignment='center',
                    verticalalignment='bottom',
                    fontsize=12)

    ax.set_ylabel('Correlation')
    ax.set_ylim(0,)
    sns.despine()

    return g


def plot_groups_target_response_matrices(groups):

    sns.set('poster', 'whitegrid',
            rc={"grid.linestyle": ":", 'legend.frameon': True},
            font_scale=.8)

    n_groups = len(groups)
    label_set = groups[0][0].labels

    ncols = 2
    nrows = sum(divmod(n_groups, ncols))
    fig, axs = plt.subplots(nrows, ncols, sharex=False, sharey=True,
                            figsize=(ncols * 6, nrows * 6))

    for i, group in enumerate(groups):

        ax = axs.flat[i]
        plot_matrix(group.target_response_matrix().sum(axis=0),
                    norm='recall', cmap='Greys', ax=ax, colorbar=None,
                    target_names=label_set + ['Null'],
                    label_rotation_deg=20)
        ax.set_title('')
        ax.set_xlabel(group.name)

        if i % ncols == 0:
            ax.set_ylabel('Target')
        else:
            ax.set_ylabel('')

        if i >= ncols:  # hide xticklabels in all but first row
            ax.set_xticklabels([])

    for i in range(n_groups % ncols):
        # delete additional axes
        fig.delaxes(axs.flat[-1])

    fig.subplots_adjust(hspace=.15, wspace=.1)

    return fig, axs


def plot_groups_queries_factors(groups, x, y, hue=None, kind='point', **kwargs):

    sns.set('poster', 'whitegrid', rc={
                "grid.linestyle": ":", 'legend.frameon': True
            },
            font_scale=1)

    df = {}
    group_names = []
    for group in groups:
        df[group.name] = group.queries()
        group_names.append(group.name)
    df = pd.concat(df, names=['group', 'rep', 'query_id']).reset_index()

    g = sns.factorplot(x=x, y=y,
                        data=df, hue=hue,
                        kind=kind, **kwargs)
    sns.despine()

    return g


def plot_groups_latency(groups, x='response_value', hue=None, kind='swarm', **kwargs):

    g = plot_groups_queries_factors(groups, x, 'response_latency', hue, kind, **kwargs)

    g.set_xticklabels(rotation=60, ha='right')
    g.set_axis_labels('', 'Latency (s)')
    return g
