# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-11-01 11:05:09
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-09-13 12:19:02
#
# Functions for reading progress logs

import os
import re
from glob import glob
from collections import OrderedDict
from itertools import islice

import pandas as pd
import numpy as np

from MLUtilities.utils import cast_to_number


# ======================================================================


def read_learner_stats(path_or_filenames, **kwargs):

    if isinstance(path_or_filenames, str):
        ext = os.path.splitext(os.listdir(path_or_filenames)[0])[1]
    elif isinstance(path_or_filenames, (tuple, list)):
        ext = os.path.splitext(path_or_filenames[0])[1]
    else:
        raise TypeError('path_or_filenames must either be a string containing a path or a list of filenames, but is {}.'.format(type(path_or_filenames)))

    if ext == '.h5':
        return read_learner_stats_hdf(path_or_filenames, **kwargs)
    elif ext == '.np':
        return read_learner_stats_np(path_or_filenames, **kwargs)
    else:
        return read_learner_stats_csv(path_or_filenames, **kwargs)


def read_learner_stats_hdf(path_or_filenames, key='data', files_as_index_level=False, **kwargs):
    """
    Reads (multiple) learner stats files from disk and stitches them together to one large DataFrame.

    Parameters
    ----------
    path_or_filenames : str or list
        Either path to a folder containing *.h5 files, or the full filenames of the *.h5 files
    key : str, optional
        Key to use for reading the HDF file.
    files_as_index_level : bool, optional
        If True, the returned DataFrame's first index level is the filename of each read file. If False, all read DataFrames are concatenated.

    Returns
    -------
    pd.DataFrame
        Contains all read DataFrame concatenated.

    Raises
    ------
    TypeError
        Invalid path_or_filenames
    """
    if isinstance(path_or_filenames, str):
        filenames = sorted(glob(os.path.join(path_or_filenames, '*.h5')))
    elif isinstance(path_or_filenames, (tuple, list)):
        filenames = path_or_filenames
    else:
        raise TypeError('path_or_filenames must either be a string containing a path or a list of filenames, but is {}.'.format(type(path_or_filenames)))

    if len(filenames) == 0:
        return None

    if files_as_index_level:
        dfs = {}
    else:
        dfs = []

    for filename in filenames:

        df = pd.read_hdf(filename, key=key, **kwargs)

        if files_as_index_level:
            filekey = os.path.splitext(os.path.basename(filename))[0]
            dfs[filekey] = df
        else:
            dfs.append(df)

    if files_as_index_level:
        dfs = pd.concat(dfs, names=['file', 'video', 'frame'])
    else:
        dfs = pd.concat(dfs)

    dfs = dfs.sort_index(0)

    return dfs


def read_learner_stats_np(path_or_filenames, stack=False, **kwargs):

    if isinstance(path_or_filenames, str):
        filenames = sorted(glob(os.path.join(path_or_filenames, '*.np')))
    elif isinstance(path_or_filenames, (tuple, list)):
        filenames = path_or_filenames
    else:
        raise TypeError('path_or_filenames must either be a string containing a path or a list of filenames, but is {}.'.format(type(path_or_filenames)))

    if len(filenames) == 0:
        return None

    dfs = OrderedDict()
    for filename in filenames:
        dfs[filename] = np.load(filename)

    if stack:
        dfs = np.stack(dfs.itervalues())

    return dfs


def read_learner_stats_csv(path_or_filenames, **kwargs):

    if isinstance(path_or_filenames, str):
        filenames = sorted(glob(os.path.join(path_or_filenames, '*.txt')))
        filenames.append(sorted(glob(os.path.join(path_or_filenames, '*.csv'))))
    elif isinstance(path_or_filenames, (tuple, list)):
        filenames = path_or_filenames
    else:
        raise TypeError('path_or_filenames must either be a string containing a path or a list of filenames, but is {}.'.format(type(path_or_filenames)))

    if len(filenames) == 0:
        return None

    dfs = {}
    for filename in filenames:
        dfs[filename] = pd.read_csv(filename, **kwargs)

    return dfs


# ======================================================================


def _is_old_session_format(filename):

    pint = '[-+]?(0[xX][\dA-Fa-f]+|0[0-7]*|\d+)'
    new_pattern = re.compile(r'Learning iteration: (' + pint + ')',
                                re.IGNORECASE)
    old_pattern = re.compile(r'TP Iteration done: (' + pint + ')',
                                re.IGNORECASE)

    max_n = 50
    k_old = False
    with open(filename, mode='r') as fp:
        for il, line in enumerate(fp):
            if il >= max_n:
                break

            res = new_pattern.search(line)
            if res is not None:  # whenever there is a match means: new format
                return False

            res = old_pattern.search(line)
            if res is not None:
                if k_old:   # two matches without any new match means: old
                    return True
                k_old = True

    raise TypeError('Invalid file is in neither old nor new format.')


def parse_session_log(filename):

    # scanf() Token      Regular Expression
    # %e, %E, %f, %g     [-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?
    # %i                 [-+]?(0[xX][\dA-Fa-f]+|0[0-7]*|\d+)
    pfloat = '[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?'
    pint = '[-+]?(0[xX][\dA-Fa-f]+|0[0-7]*|\d+)'

    # example lines:
    # 2016-11-01 12:16:28 Level 15 ml.learner.uncertaintylearner: Target class: 0 = Approaching
    # 2016-11-01 12:16:29 INFO   __main__: TP Iteration done: 1
    # 2016-11-01 12:16:28 Level 15 ml.learner.uncertaintylearner: Picked sample has an H = 5.295, p = 0.320485977293

    is_old_format = _is_old_session_format(filename)

    if is_old_format:
        iteration_pattern = re.compile(r'TP Iteration done: (' + pint + ')',
                                re.IGNORECASE)
    else:
        iteration_pattern = re.compile(r'Learning iteration: (' + pint + ')',
                                    re.IGNORECASE)


    patterns = {
        'query_target': re.compile(r'Target class: (' + pint + ')',
                                   re.IGNORECASE),
        'entropy_H': re.compile(r'ml.learner.uncertaintylearner:.*?H = (' + pfloat + ')',
                                re.IGNORECASE),
        'entropy_p': re.compile(r'ml.learner.uncertaintylearner:.*?p = (' + pfloat + ')',
                                re.IGNORECASE),
        'training_time': re.compile(r'Training detection model... Done\. (' + pfloat + ')s', re.IGNORECASE),
        'prediction_time': re.compile(r'Predicting labels of dataset\.\.\. Done\. (' + pfloat + ')s', re.IGNORECASE),
        'prediction_time_test': re.compile(r'Predict labels of testset\.\.\. Done\. (' + pfloat + ')s', re.IGNORECASE)
    }

    data = {}
    it = 0
    for il, line in enumerate(open(filename)):
        res = iteration_pattern.search(line)
        if res is not None:
            it = cast_to_number(res.group(1))
            continue

        for key, pat in patterns.viewitems():
            if key not in data:
                data[key] = {}
            res = pat.search(line)
            if res is not None:
                data[key][it] = cast_to_number(res.group(1))
                break

    df = pd.DataFrame.from_dict(data, orient='columns')
    df['query_target'] = df['query_target'].astype(int)
    df.index = df.index.astype(int)
    df.index.name = 'iteration'
    return df


# ======================================================================


def read_response_log(filename, squeeze_lists=True):
    columns = ['timestamp', 'query_id', 'response_state', 'response_value', 'response_latency']

    df = pd.read_csv(filename, sep=';', header=None, dtype={'query_id': str},
                     names=columns, parse_dates=['timestamp'])

    df = df.set_index('timestamp')

    def squeeze_values(x):
        x = x.strip("[']")
        x = x.split(',')
        if len(x) == 1:
            return x[0]
        else:
            return map(lambda s: s.strip(" '"), x)

    df['response_value'] = df['response_value'].apply(squeeze_values)

    return df


def read_query_log(filename, extra_columns=[]):
    columns = ['timestamp', 'query_id', 'video', 'start', 'end'] + extra_columns

    df = pd.read_csv(filename, sep=';', header=None, dtype={1: str},
                     parse_dates=[0])

    ncols = df.shape[1]

    if ncols > len(columns):
        columns += ['column_{}'.format(i) for i in range(ncols - len(columns))]
    else:
        columns = columns[:ncols]

    df.columns = columns
    df = df.set_index('timestamp')

    return df


def read_suggestions_log(filename, extra_columns=[]):

    columns = ['timestamp', 'video', 'start', 'end'] + extra_columns

    df = pd.read_csv(filename, sep=';', header=None, parse_dates=[0])

    ncols = df.shape[1]

    if ncols > len(columns):
        columns += ['column_{}'.format(i) for i in range(ncols - len(columns))]
    else:
        columns = columns[:ncols]

    df.columns = columns
    df = df.set_index('timestamp')

    return df

# ======================================================================


def read_logged_label_sequence(path_or_filenames, labels=None):

    if isinstance(path_or_filenames, str):
        filenames = sorted(glob(os.path.join(path_or_filenames, '*.csv')))
    elif isinstance(path_or_filenames, (tuple, list)):
        filenames = path_or_filenames
    else:
        raise TypeError('path_or_filenames must either be a string containing a path or a list of filenames, but is {}.'.format(type(path_or_filenames)))

    data = {}
    for filename in filenames:
        video = os.path.splitext(filename)[0].split('_')[-1]
        data[video] = pd.read_csv(filename, sep=';', index_col=0)
    return pd.concat(data, names=['video', 'frame'])
