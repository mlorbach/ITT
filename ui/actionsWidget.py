# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-29 12:56:56
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:54:57

import os
import logging
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.uic import loadUiType

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_Widget, QWidget = loadUiType(os.path.join(curpath, 'actions.ui'))

logger = logging.getLogger(__name__)


class ActionsWidget(QWidget, Ui_Widget):
    """docstring for ActionsWidget"""

    single_iteration_requested = pyqtSignal()
    reset_requested = pyqtSignal()
    load_labels_requested = pyqtSignal()
    save_labels_requested = pyqtSignal()

    def __init__(self, parent=None):
        super(ActionsWidget, self).__init__(parent)
        self.setupUi(self)

        self.btnReset.clicked.connect(self.reset_requested.emit)
        self.btnSingleLabeling.clicked.connect(self.single_iteration_requested.emit)
        self.btnLoadLabels.clicked.connect(self.load_labels_requested.emit)
        self.btnSaveLabels.clicked.connect(self.save_labels_requested.emit)
