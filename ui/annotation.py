# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-01-03 12:07:16
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-31 11:21:32

import os
import logging
from PyQt5 import QtGui
from PyQt5.uic import loadUiType
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from numpy import argsort

from ml.oracle import QueryItem, QueryResult

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_Widget, QWidget = loadUiType(os.path.join(curpath, 'annotation.ui'))

logger = logging.getLogger(__name__)


class AnnotationWidget(QWidget, Ui_Widget):
    """
    UI for reacting to a learner's query: labeling a video segment.

    The UI provides a video player, appropriate labeling buttons and options to reject a query (unknown or illogical segmentation).

    This UI Dialog is typically intantiated by an Oracle, e.g., the HumanAnnotationOracle, who should also connect to the dialog's response signals.

    Attributes
    ----------
    query_item : QueryItem
        The query that this dialog should answer
    project : Project
        Current training project
    annot : Annotation
        Annotation acting upon
    label_set : list
        Lsit of possible labels
    accepted_labels : PyQtSignal
        Is emitted when response is positive with labels.
    rejected_extra : PyQtSignal
        Is emitted if this dialog is rejected with extra information (e.g., that video segmentation was not appropriate).
    video : dict
        Holds information about the video segment that is queried.
    """

    rejected = pyqtSignal()
    rejected_extra = pyqtSignal(int)
    accepted_labels = pyqtSignal(list)

    @property
    def annotation(self):
        return self.project_ctrl.dataset.annotation

    def __init__(self, project_ctrl, query_item,
                 response_mode='multilabel', parent=None):
        super(AnnotationWidget, self).__init__(parent=parent)

        if response_mode not in ['multilabel', 'singlelabel', 'binary']:
            raise ValueError('Unknown response mode: {}'.format(response_mode))
        self.response_mode = response_mode

        self.setupUi(self)

        self.project_ctrl = project_ctrl
        self.label_set = self.project_ctrl.get_label_set()

        self._query_counter = 0

        self.set_query(query_item)

    def poke(self):
        return None

    def set_query(self, query_item):

        self._query_counter += 1

        if query_item.parameter is not None:
            label_ranks = query_item.parameter
        else:
            label_ranks = None

        if self.response_mode == 'multilabel':
            self.responseButtons.set_info_text('Select one or multiple action labels:')

            btnAccept = self.responseButtons.add_accept_button('Continue')

            btnSegm = self.responseButtons.add_reject_button('Segmentation', custom_action=True)
            btnUnknown = self.responseButtons.add_reject_button('No idea')

            if label_ranks is not None:
                aidx = argsort(label_ranks)[::-1]
            else:
                aidx = range(len(self.label_set))

            for idx in aidx:
                self.responseButtons.add_label_button(self.label_set[idx])

            # connect to the reject options individually:
            btnSegm.clicked.connect(self.reject_segmentation)
            btnUnknown.clicked.connect(self.reject_uncertain)
        elif self.response_mode == 'singlelabel':

            self.responseButtons.set_info_text('Select one action label:')

            btnUnknown = self.responseButtons.add_reject_button('No idea')
            btnUnknown.clicked.connect(self.reject_uncertain)

            if label_ranks is not None:
                aidx = argsort(label_ranks)[::-1]
            else:
                aidx = range(len(self.label_set))

            for idx in aidx:
                if label_ranks is not None:
                    gradient = label_ranks[idx]
                else:
                    gradient = None
                self.responseButtons.add_label_button(self.label_set[idx],
                                                      mode='accept',
                                                      gradient=gradient)

        elif self.response_mode == 'binary':
            self.responseButtons.set_info_text('Is this "{}"?'.format(label_ranks[0]))
            self.responseButtons.add_label_button('Yes', mode='accept',
                                                  gradient=label_ranks[1])
            self.responseButtons.add_label_button('No', mode='accept',
                                                  gradient=1. - label_ranks[1])

        # connect to custom 'accepted' signal which passes on the selected labels
        self.responseButtons.accepted.connect(self.accept_labels)

        self.query_item = query_item
        self._init_query(self.query_item)

    def clear_query(self):
        # stop and remove video
        self.video = {}
        self.videoPlayer.clear()

        # Workaround for loss in performance of video player after about 30-40 queries (video lags, loading time increases):
        # reinitialize the video player every 50 queries
        if self._query_counter >= 25:
            self.videoPlayer.reinitialize_player()
            self._query_counter = 0

        # disconnect from accepted signal
        try:
            self.responseButtons.accepted.disconnect(self.accept_labels)
        except TypeError:
            pass

        # disconnect player ready signal
        try:
            self.videoPlayer.finished_loading.disconnect(self._player_ready)
        except TypeError:
            pass

        # remove buttons
        self.responseButtons.clear()
        self.responseButtons.set_info_text('Waiting for query...')

    def reject_segmentation(self):
        self.clear_query()
        self.rejected_extra.emit(QueryResult.SEGMENTATION)
        # self.done(QueryResult.SEGMENTATION)

    def reject_uncertain(self):
        self.clear_query()
        self.rejected_extra.emit(QueryResult.UNCERTAIN)
        # self.done(QueryResult.UNCERTAIN)

    def accept_labels(self, labels):
        self.clear_query()
        self.accepted_labels.emit(labels)
        # self.accept()

    def reject(self):
        self.clear_query()
        self.rejected.emit()

    def _init_query(self, query):

        self.video = {}

        self.video['name'] = query.value.get_level_values('video').unique()[0]
        video_frames = query.value.get_level_values('frame').values

        self.video['file'] = self.project_ctrl.get_video_collection().loc[self.video['name'], 'file']
        self.video['fps'] = self.project_ctrl.get_video_collection().loc[self.video['name'], 'fps']

        self.video['start_ms'] = int(video_frames[0] / self.video['fps'] * 1000)
        self.video['end_ms'] = int(video_frames[-1] / self.video['fps'] * 1000)

        logger.debug('Initialize query for human.')
        logger.debug('  Video: {}, frames: {} to {}.'.format(self.video['file'], self.video['start_ms'], self.video['end_ms']))

        # we have to wait until the video player is all set up and the video is loaded before we can seek to a specific time. So we connect to a signal of the player that notifies us once it's ready:
        self.videoPlayer.finished_loading.connect(self._player_ready)

        # open the video file
        self.videoPlayer.setFPS(self.video['fps'])
        self._open_video(self.video['file'])

    def _open_video(self, filename):
        logger.debug('Opening video file {}'.format(filename))
        self.videoPlayer.openFile(filename)

    def _player_ready(self):
        if 'start_ms' in self.video and 'end_ms' in self.video:
            self._set_play_loop(self.video['start_ms'],
                                self.video['end_ms'])
        self.videoPlayer.setControlsVisible(False)
        self.videoPlayer.play()

    def _set_play_loop(self, start, end):
        self.videoPlayer.set_play_loop(start, end)
