# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 14:22:29
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:55:10

from PyQt5 import QtGui


class AnnotationTableWidget(QtGui.QWidget):
    """docstring for AnnotationTableWidget"""
    def __init__(self, parent=None):
        super(AnnotationTableWidget, self).__init__(parent)

        self._table = QtGui.QTableView()
        self._model = None
        self._activeAnnotation = None

        # layout
        self._addButton = QtGui.QPushButton('New annotation')
        self._removeButton = QtGui.QPushButton('Remove selected')
        self._activateButton = QtGui.QPushButton('Set active')
        buttonLayout = QtGui.QVBoxLayout()
        buttonLayout.setContentsMargins(0, 0, 0, 0)
        buttonLayout.setSpacing(10)
        buttonLayout.addWidget(self._addButton)
        buttonLayout.addWidget(self._removeButton)
        buttonLayout.addWidget(self._activateButton)
        buttonLayout.addStretch()

        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(10, 10, 10, 10)
        layout.setSpacing(10)
        layout.addWidget(self._table)
        layout.addLayout(buttonLayout)

        self.setLayout(layout)

        # formatting
        self._table.setShowGrid(False)
        self._table.verticalHeader().setVisible(False)
        self._table.horizontalHeader().setStretchLastSection(True)

        # interaction
        self._addButton.clicked.connect(self._add)
        self._removeButton.clicked.connect(self._remove)
        self._activateButton.clicked.connect(self._activate)

    def setAnnotations(self, annotations, activeAnnotation=None):
        self._model = QtGui.QStandardItemModel(len(annotations), 2, self)
        self._model.setHorizontalHeaderLabels(['*', 'Name'])

        if isinstance(annotations, dict):
            annot_names = sorted(annotations.keys())
        else:
            annot_names = annotations

        if activeAnnotation is not None:
            self._activeAnnotation = activeAnnotation
        if len(annot_names) > 0 and self._activeAnnotation not in annot_names:
            self._activeAnnotation = annot_names[0]

        for i, name in enumerate(annot_names):
            item = QtGui.QStandardItem(name)
            self._model.setItem(i, 1, item)
            if name == self._activeAnnotation:
                self._model.setItem(i, 0, QtGui.QStandardItem('*'))

        self._table.setModel(self._model)
        self._table.resizeColumnToContents(0)

    def getAnnotations(self):
        annotations = []
        for item in self._model.takeColumn(1):
            annotations.append(str(item.text()))
        return annotations

    def getActiveAnnotation(self):
        return self._activeAnnotation

    def _add(self):
        self._model.insertRows(self._model.rowCount(), 1)
        lastidx = self._model.index(self._model.rowCount() - 1, 1)
        self._table.setCurrentIndex(lastidx)
        self._table.edit(lastidx)

    def _remove(self):
        rows = sorted([i.row() for i in self._table.selectedIndexes()])
        for row in rows[::-1]:
            self._model.removeRows(row, 1)

    def _activate(self):
        row = sorted([i.row() for i in self._table.selectedIndexes()])[-1]

        self._activateRow(row)

    def _activateRow(self, row):
        curactive = self._model.findItems('*', column=0)
        if len(curactive) > 0:
            curactive[0].setText('')

        if self._model.item(row, 0) is None:
            self._model.setItem(row, 0, QtGui.QStandardItem('*'))
        else:
            self._model.item(row, 0).setText('*')
        self._activeAnnotation = self._model.item(row, 1).text()

