# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-11-28 16:08:33
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 13:27:30

from PyQt5 import QtGui, QtWidgets, QtCore

# from pandasqt.views.DataTableView import DataTableWidget
# from pandasqt.models.DataFrameModel import DATAFRAME_ROLE

# from qtpandas.views.DataTableView import DataTableWidget
# from qtpandas.models.DataFrameModel import DATAFRAME_ROLE

class ClickableDataTableWidget(QtWidgets.QWidget):

    cellClicked = QtCore.pyqtSignal(int, int, list)
    cellDoubleClicked = QtCore.pyqtSignal(int, int, list)

    def __init__(self, parent=None):
        super(ClickableDataTableWidget, self).__init__(parent)

        # Signals/Slots
        # self.view().clicked.connect(self._on_click)
        # self.view().doubleClicked.connect(self._on_double_click)

    # def _on_click(self, index):
    #     if index.isValid():
    #         item = [self.model().data(index)]
    #         self.cellClicked.emit(index.row(), index.column(), item)

    # def _on_double_click(self, index):
    #     if index.isValid():
    #         item = [self.model().data(index)]
    #         self.cellDoubleClicked.emit(index.row(), index.column(), item)
