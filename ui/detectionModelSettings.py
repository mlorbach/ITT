# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-21 14:11:31
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:55:30

import os
import logging
import json
from PyQt5 import QtGui, QtCore
from PyQt5.uic import loadUiType
from PyQt5.QtCore import pyqtSignal, pyqtSlot

from modelSettings import ModelSettings

curpath = os.path.dirname(os.path.abspath(__file__))
dm_options_file = os.path.join(curpath, '..', 'ml', 'detection_models.json')

logger = logging.getLogger(__name__)

class DetectionModelSettings(ModelSettings):
    """docstring for DetectionModelSettings"""
    def __init__(self, model, parent=None):
        super(DetectionModelSettings, self).__init__(model, options_file=dm_options_file, parent=parent)
