# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-21 12:19:23
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:55:36

import os
import logging
import numpy as np
from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.uic import loadUiType

# from ui.mplCanvas import BarPlotCanvas
import pyqtgraph as pg

from core.project_ctrl import ProjectController
from core.annotation import Annotation
from ml.detection_model import DetectionModel
from ml.metrics import class_average, frame_average

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_DetectionModelWidget, QWidget = loadUiType(os.path.join(curpath, 'detectionModelWidget.ui'))

pg.setConfigOption('background', None)
pg.setConfigOption('foreground', 'k')

logger = logging.getLogger(__name__)

class DetectionModelView(QWidget, Ui_DetectionModelWidget):
    """docstring for DetectionModelView"""
    def __init__(self, parent=None, project_ctrl=None):
        super(DetectionModelView, self).__init__(parent)
        self.setupUi(self)
        self.project_ctrl = project_ctrl

        # plots
        self._pw = None
        self._curve = None
        self._nodata_text = pg.TextItem('No data yet',
                                        color=(0, 0, 0),
                                        anchor=(0.5, 0))
        self._nodata_text.setPos(0.5, .9)
        self._colors = ['#009900', '#990000']
        self._markers = ['o', 's']

        if self.project_ctrl is not None:
            self._update_accuracy_history_plot()

    def setModel(self, project_ctrl):
        self.project_ctrl = project_ctrl
        self.update_ui_from_model()

    @pyqtSlot()
    def update_ui_from_model(self, what=''):

        if self.project_ctrl is None:
            return

        self._update_accuracy_overview()
        self._update_accuracy_history_plot()

    def _update_accuracy_overview(self):

        # detection model information
        self.lblDetectionModelType.setText(self.project_ctrl.get_detection_model().get_type())
        self.lblFeatureProcessingType.setText(self.project_ctrl.get_feature_processor().get_type())

        # detection model accuracy
        dfscores = self.project_ctrl.get_accuracies()
        dftestscores = self.project_ctrl.get_test_accuracies()

        class_avg = class_average(dfscores)
        frame_avg = frame_average(dfscores)
        class_avg_test = class_average(dftestscores)
        frame_avg_test = frame_average(dftestscores)

        self.lblPrecFrames.setText('{: 3.1%} | {: 3.1%}'.format(
                    frame_avg[0], frame_avg_test[0]).replace('nan', '--'))
        self.lblRecallFrames.setText('{: 3.1%} | {: 3.1%}'.format(
                    frame_avg[1], frame_avg_test[1]).replace('nan', '--'))

        self.lblPrecClasses.setText('{: 3.1%} | {: 3.1%}'.format(
                    class_avg[0], class_avg_test[0]).replace('nan', '--'))
        self.lblRecallClasses.setText('{: 3.1%} | {: 3.1%}'.format(
                    class_avg[1], class_avg_test[1]).replace('nan', '--'))

    def _update_accuracy_history_plot(self):

        # Accuracy Plot
        # -------------

        # get data:
        # lcdata = np.array([[0, 1, 2, 3, 4], [0, 0.1, 0.45, 0.55, 0.6]]).T

        # lcdata = np.asarray([[0, dfscores.loc[:, 'F1'].mean()]])
        dfhist = self.project_ctrl.get_project().learning_stats.accuracy_history.copy()

        score_cols = ['Iteration', 'F1']
        if 'F1_test' in dfhist.columns:
            score_cols.append('F1_test')

        lcdata = dfhist.sort_values('Iteration').loc[:, score_cols].values

        if self._pw is None:
            self._pw = self.pwLearningCurve

            # Axis and ticks styling
            label_style = {'font-size': '6pt'}
            font_style = QtGui.QFont()
            font_style.setPointSize(6)
            self._pw.getAxis('bottom').setHeight(None)
            self._pw.getAxis('left').setWidth(None)

            self._pw.getAxis('bottom').setStyle(tickTextHeight=6,
                                                autoExpandTextSpace=False)
            self._pw.getAxis('left').setStyle(tickTextWidth=10,
                                              autoExpandTextSpace=False)

            self._pw.getAxis('bottom').tickFont = font_style
            self._pw.getAxis('left').tickFont = font_style

            # Labels and ranges
            self._pw.setLabel('left', 'Avg. F1', **label_style)
            self._pw.setLabel('bottom', 'Iterations', **label_style)
            self._pw.setYRange(0, 1)
            self._pw.setXRange(0, 1)
            self._pw.hideButtons()



            self._pw.addItem(self._nodata_text)

        if self._curve is None and lcdata.shape[0] > 0:
            # first call to plot
            self._pw.removeItem(self._nodata_text)

            self._pw.setXRange(0, np.max(lcdata[:, 0]))
            # self._pw.getAxis('left').setTicks([.2, .4, .6, .8, 1])

            self._curve = []
            for idata in xrange(lcdata.shape[1] - 1):
                self._curve.append(pg.PlotDataItem(lcdata[:, (0, idata + 1)],
                                        pen={'color': self._colors[idata], 'width': 2},
                                        symbol=self._markers[idata],
                                        symbolBrush='#333333',
                                        symbolSize=7,
                                        pxMode=True,
                                        antialias=True))
            for curve in self._curve:
                self._pw.addItem(curve)
                curve.getViewBox().setMouseEnabled(x=False, y=False)

        elif self._curve is not None and lcdata.shape[0] > 0:
            for idata in xrange(lcdata.shape[1] - 1):
                self._curve[idata].setData(lcdata[:, (0, idata + 1)])
            self._pw.setXRange(0, np.max(lcdata[:, 0]))

        elif self._curve is not None and lcdata.shape[0] == 0:
            for curve in self._curve:
                curve.clear()
                self._pw.removeItem(curve)
            self._curve = None
            self._pw.setXRange(0, 1)
            self._pw.addItem(self._nodata_text)
