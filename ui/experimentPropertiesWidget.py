# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 13:57:15
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:55:42

import os
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.uic import loadUiType

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_Widget, QWidget = loadUiType(os.path.join(curpath, 'experimentProperties.ui'))

class ExperimentPropertiesWidget(QWidget, Ui_Widget):

    changed = pyqtSignal()

    def __init__(self, experiment, parent=None):
        super(ExperimentPropertiesWidget, self).__init__(parent)
        self.setupUi(self)

        self.experiment = experiment
        self.editName.textEdited.connect(self.on_name_changed)
        self.editName.textEdited.connect(self.changed)
        self.editComment.textChanged.connect(self.changed)

        self.changed.connect(self.update_values)

        self.populate(self.experiment)

    def populate(self, exp):
        self.lblDate.setText(exp.date)
        self.lblProjectName.setText(exp.project_name)
        self.editName.setText(exp.participant_name)
        self.editComment.setPlainText(exp.comment)

    def update_values(self):
        self.experiment.participant_name = self.editName.text()
        self.experiment.comment = self.editComment.toPlainText()

    def get_experiment_data(self):
        return dict(participant_name=str(self.editName.text()),
                    comment=(self.editComment.toPlainText())
                    )

    def on_name_changed(self, new):
        if len(new) > 0:
            self.editName.setStyleSheet("")
        else:
            self.editName.setStyleSheet("background-color: rgb(255, 220, 220);")
