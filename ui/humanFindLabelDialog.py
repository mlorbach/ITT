# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-11-02 12:56:39
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:55:55

import os
import logging
from itertools import cycle
from PyQt5 import QtGui
from PyQt5.uic import loadUiType
from PyQt5.QtCore import pyqtSignal, pyqtSlot

from ml.oracle import QueryItem, QueryResult

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_HumanFindLabelDialog, QDialog = loadUiType(os.path.join(curpath,'humanFindLabelDialog.ui'))

logger = logging.getLogger(__name__)

class HumanFindLabelDialog(QDialog, Ui_HumanFindLabelDialog):
    """
    UI for reacting to a learner's query: labeling a video segment.

    The UI provides a video player, appropriate labeling buttons and options to reject a query (unknown or illogical segmentation).

    This UI Dialog is typically intantiated by an Oracle, e.g., the HumanAnnotationOracle, who should also connect to the dialog's response signals.

    Attributes
    ----------
    query_item : QueryItem
        The query that this dialog should answer
    project : Project
        Current training project
    annot : Annotation
        Annotation acting upon
    label_set : list
        Lsit of possible labels
    accepted_labels : PyQtSignal
        Is emitted when response is positive with labels.
    rejected_extra : PyQtSignal
        Is emitted if this dialog is rejected with extra information (e.g., that video segmentation was not appropriate).
    video : dict
        Holds information about the video segment that is queried.
    """

    def __init__(self, project_ctrl, videos, label):
        super(HumanFindLabelDialog, self).__init__()
        self.setupUi(self)

        self.project_ctrl = project_ctrl
        self.label_set = self.project_ctrl.get_label_set()
        self._requested_label = label

        # connect to custom 'accepted' signal which passes on the selected labels
        self.btnAccept.clicked.connect(self.accept_sample)

        # connect
        self.btnNextVideo.clicked.connect(self.next_video)

        # self.query_item = query_item
        self._init_query(videos, self._requested_label)

    def clear_query(self):
        # stop and remove video
        self.video = {}
        self.videoPlayer.stop()

    def next_video(self):
        self.videoPlayer.stop()
        self._init_video(self.videos.next())

    def accept_sample(self, labels):
        self.videoPlayer.pause()
        self._current_frame = self.videoPlayer.current_frame()
        self.accept()

    def get_selected_sample(self):
        frame = self.videoPlayer.current_frame()
        return (self.video['name'], frame)

    def _init_query(self, videos, label):

        # update button text
        self.btnAccept.setText('This is "{}"'.format(label))

        self.videos = cycle(videos)
        self._init_video(self.videos.next())

        self.setWindowTitle('Select an image that fits the label "{}".'.format(label))

        logger.debug('Initialize query for human.')
        logger.debug('  Video: {}, label: {}.'.format(self.video['file'], label))

    def _init_video(self, videoname):

        self.video = {}

        self.video['name'] = videoname
        # video_frames = query.value.get_level_values('frame').values

        self.video['file'] = self.project_ctrl.get_video_collection().loc[self.video['name'], 'file']
        self.video['fps'] = self.project_ctrl.get_video_collection().loc[self.video['name'], 'fps']

        self.video['start_ms'] = (self.project_ctrl.get_video_collection().loc[self.video['name'], 'first_valid_frame'] / self.video['fps'] * 1000)

        self.video['end_ms'] = (self.project_ctrl.get_video_collection().loc[self.video['name'], 'last_valid_frame'] / self.video['fps'] * 1000)

        # we have to wait until the video player is all set up and the video is loaded before we can seek to a specific time. So we connect to a signal of the player that notifies us once it's ready:
        self.videoPlayer.finished_loading.connect(self._player_ready)

        # open the video file
        self.videoPlayer.setFPS(self.video['fps'])
        self._open_video(self.video['file'])

    def _open_video(self, filename):
        logger.info('Opening video file {}'.format(filename))
        self.videoPlayer.openFile(filename)

    def _player_ready(self):
        if 'start_ms' in self.video and 'end_ms' in self.video:
            self._set_play_loop(self.video['start_ms'],
                                self.video['end_ms'])
        self.videoPlayer.play()

    def _set_play_loop(self, start, end):
        self.videoPlayer.set_play_loop(start, end)
