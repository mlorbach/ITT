# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-08-21 12:31:28
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-21 16:09:39

import os
import logging
from glob import glob
from PyQt5 import QtGui, QtWidgets
from PyQt5.uic import loadUiType

from core.utils import is_backup_folder

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_Dialog, QDialog = loadUiType(os.path.join(curpath, 'importBackupDialog.ui'))

logger = logging.getLogger(__name__)


class ImportBackupDialog(QDialog, Ui_Dialog):
    def __init__(self):
        super(ImportBackupDialog, self).__init__()
        self.setupUi(self)

        self.btnSelectBackupFolder.clicked.connect(self.select_backup)

        self._projects_folder = os.path.normpath(os.path.join(os.getcwd(), 'projects'))
        self.editBackupFolder.setText(self._projects_folder)

    def select_backup(self):
        # initialize file selector
        selected_folder = QtWidgets.QFileDialog.getExistingDirectory(self,
                                                                     caption='Select backup location',
                                                                     directory=self._projects_folder)

        # set lineedit
        self.editBackupFolder.setText(selected_folder)

    def accept(self):
        # check valid
        require_models = self.checkDetectionModel.isChecked() or self.checkFeatureProc.isChecked()
        if not is_backup_folder(str(self.editBackupFolder.text()),
                                require_models=require_models):

            QtGui.QMessageBox.warning(self,
                                      'Invalid selection',
                                      'The chosen directory does not contain a valid backup.\nPlease chose another directory.',
                                      buttons=QtGui.QMessageBox.Ok)
            return
        else:
            super(ImportBackupDialog, self).accept()

    def get_form_data(self):
        return dict(backup_folder=str(self.editBackupFolder.text()),
                    import_detection_model=self.checkDetectionModel.isChecked(),
                    import_feature_processor=self.checkFeatureProc.isChecked(),
                    import_logs=self.checkLogs.isChecked(),
                    import_annotations=self.checkAnnotations.isChecked())
