# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 14:22:29
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 13:31:11

from PyQt5 import QtGui, QtCore, QtWidgets


class LabelTableWidget(QtWidgets.QWidget):
    """docstring for LabelTableView"""
    def __init__(self, parent=None):
        super(LabelTableWidget, self).__init__(parent)

        self._table = QtWidgets.QTableView()
        self._model = None

        # layout
        self._addLabelButton = QtWidgets.QPushButton('New label')
        self._removeLabelButton = QtWidgets.QPushButton('Remove selected')
        self._addDefaultSocialButton = QtWidgets.QPushButton('Add standard social behaviors')
        buttonLayout = QtWidgets.QVBoxLayout()
        buttonLayout.setContentsMargins(0, 0, 0, 0)
        buttonLayout.setSpacing(10)
        buttonLayout.addWidget(self._addLabelButton)
        buttonLayout.addWidget(self._removeLabelButton)
        buttonLayout.addStretch()
        buttonLayout.addWidget(self._addDefaultSocialButton)

        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(10, 10, 10, 10)
        layout.setSpacing(10)
        layout.addWidget(self._table)
        layout.addLayout(buttonLayout)

        self.setLayout(layout)

        # formatting
        self._table.setShowGrid(False)
        self._table.verticalHeader().setVisible(False)
        self._table.horizontalHeader().setStretchLastSection(True)
        self._table.horizontalHeader().setVisible(False)

        # interaction
        self._addLabelButton.clicked.connect(self._addLabel)
        self._removeLabelButton.clicked.connect(self._removeLabel)
        self._addDefaultSocialButton.clicked.connect(self._addSocialBehaviors)

    def setLabelList(self, labels):
        self._model = QtCore.QStringListModel(labels, self)
        self._table.setModel(self._model)

    def getLabelList(self):
        return [str(l) for l in self._model.stringList()]

    def _addSocialBehaviors(self):
        social_beh = set(['Approaching', 'Contact', 'Following', 'Moving away', 'Solitary'])

        current = set(self.getLabelList())
        current |= social_beh
        self.setLabelList(sorted(list(current)))


    def _addLabel(self):
        self._model.insertRows(self._model.rowCount(), 1)
        lastidx = self._model.index(self._model.rowCount()-1)
        self._table.setCurrentIndex(lastidx)
        self._table.edit(lastidx)

    def _removeLabel(self):
        rows = sorted([i.row() for i in self._table.selectedIndexes()])

        for row in rows[::-1]:
            self._model.removeRows(row, 1)
