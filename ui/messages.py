# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-08-21 11:13:00
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-21 11:13:58

from PyQt5 import QtWidgets


def msg_incompatible_project(parent=None, project_name=""):
    QtWidgets.QMessageBox.warning(parent,
                                  'Incompatible project',
                                  'The chosen project ("{}") was created with an older version of ITT and is incompatible with the current version.\nCannot open project.'.format(project_name),
                                  buttons=QtWidgets.QMessageBox.Ok)
