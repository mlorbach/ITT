# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-21 14:11:31
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:56:15

import os
import logging
import json
from PyQt5 import QtGui, QtCore
from PyQt5.uic import loadUiType
from PyQt5.QtCore import pyqtSignal, pyqtSlot

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_ModelSettings, QDialog = loadUiType(os.path.join(curpath,'modelSettings.ui'))

logger = logging.getLogger(__name__)

class ModelSettings(QDialog, Ui_ModelSettings):
    """docstring for ModelSettings"""
    def __init__(self, model, options=None, options_file=None, parent=None):
        super(ModelSettings, self).__init__(parent)
        self.setupUi(self)

        self.model = model

        if options is not None:
            self.options = options
        else:
            if options_file is None:
                raise AttributeError('Must specify either options or options_file. None specified.')

            if not os.path.isfile(options_file):
                raise IOError("Couldn't find model options file: {}.".format(options_file))

            with open(options_file, 'rb') as fp:
                self.options = json.load(fp)

        # populate forms
        self.setup_options()

        logger.debug('Current model: {}'.format(self._get_model_type_str(self.model.model.__module__, self.model.model.__class__.__name__)))
        logger.debug('Model options: {}'.format(self.options.keys()))

        # select current model model
        if self._set_selected_combo_value(self.comboModels, self._get_model_identifier(self.model.model)) is None:
            self.comboModels.setCurrentIndex(0)

        # populate forms
        self.populate(self.comboModels.currentIndex())

        # connect to model selection change
        self.comboModels.activated.connect(self.populate)

    def _get_model_type_str(self, _module, _type):
        return _module + '.' + _type

    def _get_model_type(self, _module, _type):
        return getattr(_module, _type)

    def _get_model_identifier(self, model):
        model_class_name = self._get_model_type_str(model.__module__,
                                                    model.__class__.__name__)
        for k, v in self.options.viewitems():
            if model_class_name == self._get_model_type_str(v['model_type_namespace'], v['model_type']):
                return k
        return None

    def _set_selected_combo_value(self, combobox, item_value):
        idx = combobox.findData(item_value)
        if idx >= 0:
            combobox.setCurrentIndex(idx)
            return True
        else:
            return False

    def _set_selected_combo_text(self, combobox, item_text):
        idx = combobox.findText(item_text)
        if idx >= 0:
            combobox.setCurrentIndex(idx)
            return True
        else:
            return False

    def setup_options(self):

        if self.comboModels.count() > 0:
            # if it's already setup don't do it again
            return

        for k in sorted(self.options.keys()):
            self.comboModels.addItem(self.options[k]['name'], k)

        self.ui_elements = {}

        model = self.model.model

        for k, v in self.options.viewitems():
            self.ui_elements[k] = QtGui.QWidget(self)
            self.ui_elements[k].setLayout(QtGui.QFormLayout())

            is_active_model = self._get_model_identifier(model) == k

            if "baseModel" in v:
                qitem = QtGui.QLabel('.'.join(v['baseModel']), self)
                self.ui_elements[k].layout().addRow(QtGui.QLabel('baseModel',
                                                                 self.groupBox),
                                                    qitem)

            for name, para in v['init_parameters'].viewitems():

                # ------------------
                # INTEGER PARAMETER
                # ------------------
                if para['type'] == 'int':
                        qitem = QtGui.QSpinBox(self)
                        qitem.setMinimum(max(-1e9, para['min']))
                        qitem.setMaximum(max(1e9, para['max']))

                        if is_active_model:
                            qitem.setValue(model.get_params()[name])
                        else:
                            qitem.setValue(para['default'])

                # ------------------
                # FLOAT PARAMETER
                # ------------------
                elif para['type'] == 'float':
                    qitem = QtGui.QDoubleSpinBox(self)
                    qitem.setMinimum(max(-1e9, para['min']))
                    qitem.setMaximum(max(1e9, para['max']))
                    qitem.setSingleStep(10**(-para['precision']))
                    qitem.setDecimals(para['precision'])

                    if is_active_model:
                        qitem.setValue(model.get_params()[name])
                    else:
                        qitem.setValue(para['default'])

                # ------------------
                # STRING PARAMETER WITH OPTIONS
                # ------------------
                elif para['type'] == 'str' and 'options' in para:
                    qitem = QtGui.QComboBox(self)

                    # options can come as list, in which case items represent both combo labels and values
                    if isinstance(para['options'], list):
                        # construct dict
                        para['options'] = dict(zip(para['options'], para['options']))

                    for plabel in sorted(para['options'].keys()):
                        qitem.addItem(plabel, para['options'][plabel])

                    if is_active_model:
                        self._set_selected_combo_value(qitem,
                                                       model.get_params()[name])
                    else:
                        self._set_selected_combo_value(qitem,
                                                       para['default'])

                # ------------------
                # REGULAR STRING PARAMETER
                # ------------------
                elif para['type'] == 'str':
                    qitem = QtGui.QLineEdit(self)

                    if is_active_model:
                        qitem.setText(model.get_params()[name])
                    else:
                        qitem.setText(para['default'])

                # ------------------
                # BOOLEAN PARAMETER
                # ------------------
                elif para['type'] in ['bool', 'boolean']:
                    qitem = QtGui.QCheckBox(self)

                    if is_active_model:
                        qitem.setChecked(model.get_params()[name])
                    else:
                        qitem.setChecked(para['default'])

                else:
                    logger.warn('Invalid option type: {}.'.format(para['type']))

                # add the parameter field (label + input) to the form layout
                self.ui_elements[k].layout().addRow(QtGui.QLabel(name,
                                                                 self.groupBox),
                                                    qitem)
            # initially hide the widget
            self.ui_elements[k].setVisible(False)

        # add all widgets to the layout
        for k, v in self.ui_elements.viewitems():
            self.settingsLayout.addWidget(v)


    @pyqtSlot(int)
    def populate(self, selected_model_idx):

        selected_model_id = str(self.comboModels.itemData(selected_model_idx))
        if selected_model_id not in self.options:
            logger.warn('Selected model {} not in options'.format(selected_model_id))
            return

        # hiding other options
        for idx in range(self.settingsLayout.count()):
            qitem = self.settingsLayout.itemAt(idx)
            qitem.widget().setVisible(False)

        # set current selected visible
        self.ui_elements[selected_model_id].setVisible(True)


    def get_model_data(self):
        selected_model = str(self.comboModels.itemData(self.comboModels.currentIndex()))

        ret = {
        'module': str(self.options[selected_model]['model_type_namespace']),
        'class': str(self.options[selected_model]['model_type'])
        }

        if 'baseModel' in self.options[selected_model]:
            logger.info("add baseModel info to model data")
            ret['baseModel'] = self.options[selected_model]['baseModel']

        formLayout = self.ui_elements[selected_model].layout()

        for row in range(formLayout.rowCount()):
            name = str(formLayout.itemAt(row, QtGui.QFormLayout.LabelRole).widget().text())

            qitem = formLayout.itemAt(row, QtGui.QFormLayout.FieldRole).widget()

            if isinstance(qitem, QtGui.QComboBox):
                v = qitem.itemData(qitem.currentIndex())
                ret[name] = str(v) if v is not None else None
            elif isinstance(qitem, (QtGui.QSpinBox, QtGui.QDoubleSpinBox)):
                ret[name] = qitem.value()
            elif isinstance(qitem, QtGui.QLineEdit):
                ret[name] = qitem.text()
            elif isinstance(qitem, QtGui.QCheckBox):
                ret[name] = qitem.isChecked()
            elif isinstance(qitem, QtGui.QLabel):
                pass # nothing to read from a label
            else:
                logger.warning('Unknown option type {}. Skipping return value.'.format(type(qitem)))

        return ret
