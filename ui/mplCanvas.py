# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-21 12:47:19
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:56:19

from PyQt5 import QtGui

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.ticker import MaxNLocator


class MplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    @property
    def axes(self):
        return self._axes

    @axes.setter
    def axes(self, axes):
        self._axes = axes

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi, facecolor='none')
        self._axes = fig.add_subplot(111, axis_bgcolor='white', alpha=1.0)
        # We want the axes cleared every time plot() is called
        self._axes.hold(False)

        #
        super(MplCanvas, self).__init__(fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class BarPlotCanvas(MplCanvas):
    """docstring for BarPlotCanvas"""

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title

    def __init__(self, df=None, parent=None, width=5, height=4, dpi=100):
        super(BarPlotCanvas, self).__init__(parent, width, height, dpi)
        self.df = df
        self._title = ''

    def compute_initial_figure(self):
        if self.df is None:
            return
        else:
            self.update_figure()

    def update_figure(self):
        self.df.plot(kind='bar', ax=self._axes, rot=20, title=self._title,
                     fontsize=8)
        locator = MaxNLocator(5)
        self._axes.yaxis.set_major_locator(locator)
        try:
            self._axes.get_figure().tight_layout()
        except ValueError:
            # catching bug when ylabels are too large: https://github.com/matplotlib/matplotlib/issues/5456
            pass
        self.draw()
