# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-14 14:31:28
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-21 13:07:03

import os
import logging
from glob import glob
from PyQt5 import QtGui, QtWidgets
from PyQt5.uic import loadUiType

from core.utils import is_project_folder, clear_project_folder

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_NewProjectDialog, QDialog = loadUiType(os.path.join(curpath, 'newProjectDialog.ui'))

logger = logging.getLogger(__name__)


class NewProjectDialog(QDialog, Ui_NewProjectDialog):
    def __init__(self):
        super(NewProjectDialog, self).__init__()
        self.setupUi(self)
        self.btnSelectDataset.clicked.connect(self.select_dataset)
        self.btnSelectStorage.clicked.connect(self.select_storage)

        self._projects_folder = os.path.normpath(os.path.join(os.getcwd(), 'projects'))
        self.editStorage.setText(self._projects_folder)

        self.editName.textEdited.connect(self.on_name_changed)
        self.editStorage.textEdited.connect(self.disable_name2storage_map)

    def select_dataset(self):
        # initialize file selector
        selected_folder = QtWidgets.QFileDialog.getExistingDirectory(self, caption='Open dataset', directory='E:\Datasets')

        # TODO: check if valid dataset
        if not os.path.isfile(os.path.join(str(selected_folder), 'metadata.json')) or not os.path.isfile(os.path.join(str(selected_folder), 'dinfo.json')):
            self.editDatasetFile.setText('')

            QtWidgets.QMessageBox.critical(self, 'Invalid dataset',
                                       'The chosen directory does not contain a valid dataset.')

            return

        # set lineedit
        self.editDatasetFile.setText(selected_folder)

    def select_storage(self):
        # initialize file selector
        selected_folder = QtWidgets.QFileDialog.getExistingDirectory(self, caption='Select project storage location', directory=os.path.join(self._projects_folder, str(self.editName.text())))

        # check valid (e.g., overwrite existing project?)
        if is_project_folder(selected_folder):
            # ovret = QtWidgets.QMessageBox.warning(self, 'Overwriting previous project', 'The chosen directory already contains a project.\nDo you want to overwrite the existing project (deletes all project-related data!)?',
            #             buttons=QtWidgets.QMessageBox.Yes|QtWidgets.QMessageBox.No,
            #             defaultButton=QtWidgets.QMessageBox.No)

            QtWidgets.QMessageBox.warning(self, 'Existing project folder', 'The chosen directory already contains a project.\nPlease chose another directory.',
                        buttons=QtWidgets.QMessageBox.Ok)
            return

            # if ovret == QtWidgets.QMessageBox.Yes:
            #     logger.info('User chose to overwrite existing project: {}'.format(selected_folder))
            #     clear_project_folder(selected_folder, keep_project_file=False)
            # else:
            #     return

        # set lineedit
        self.editStorage.setText(selected_folder)
        self.disable_name2storage_map()

    def disable_name2storage_map(self):
        try:
            self.editName.textEdited.disconnect(self.on_name_changed)
        except TypeError:
            pass

    def on_name_changed(self, new):
        self.editStorage.setText(os.path.join(self._projects_folder, new))

    def get_project_data(self):
        return dict(name=str(self.editName.text()),
                    storage_location=str(self.editStorage.text()),
                    dataset_folder=str(self.editDatasetFile.text()))
