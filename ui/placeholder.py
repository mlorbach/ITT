# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-01-03 13:42:02
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 13:08:28

from PyQt5 import QtGui, QtWidgets

class EmptyPlaceholderWidget(QtWidgets.QWidget):
    """docstring for EmptyPlaceholderWidget"""
    def __init__(self, parent=None):
        super(EmptyPlaceholderWidget, self).__init__(parent)

        layout = QtWidgets.QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
