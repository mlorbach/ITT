# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 13:57:15
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:56:37

import os
from PyQt5 import QtGui, QtCore
from PyQt5.uic import loadUiType

from core.project import Project

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_ProjectPropertiesDialog, QDialog = loadUiType(os.path.join(curpath, 'projectPropertiesDialog.ui'))

class ProjectPropertiesDialog(QDialog, Ui_ProjectPropertiesDialog):
    def __init__(self, project_ctrl, parent=None):
        super(ProjectPropertiesDialog, self).__init__(parent)
        self.setupUi(self)

        # remove Enter and Escape interaction from Save and Cancel buttons:
        for btn in self.buttonBox.buttons():
            btn.setDefault(False)
            btn.setAutoDefault(False)

        self.project_ctrl = project_ctrl
        self.populate(self.project_ctrl)

    def populate(self, p):

        self.editName.setText(p.name)
        self.labelProjectStorage.setText(p.get_project_directory())
        self.labelDatasetName.setText(p.project.dataset.name)

        if 'basepath' in p.get_metadata():
            self.labelDatasetPath.setText(p.get_metadata()['basepath'])
        else:
            self.labelDatasetPath.setText('N/A')

        self.tableLabels.setLabelList(p.get_label_set())

    def get_project_data(self):
        return dict(name=str(self.editName.text()),
                    label_set=sorted(self.tableLabels.getLabelList())
                    )

    def keyPressEvent(self, e):

        # don't accept ENTER key for saving and closing dialog.
        if e.key() == QtCore.Qt.Key_Return or e.key() == QtCore.Qt.Key_Enter:
            return
        return QDialog.keyPressEvent(self, e)
