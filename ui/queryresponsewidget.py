# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 13:11:43
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 15:07:49

import os
import logging
from PyQt5 import QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal
from PyQt5.uic import loadUiType

from ui.utils import make_gradient_stylesheet

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_QueryResponseWidget, QWidget = loadUiType(os.path.join(curpath, 'queryResponseWidget.ui'))

logger = logging.getLogger(__name__)


class QueryResponseWidget(QWidget, Ui_QueryResponseWidget):

    accepted = pyqtSignal(list)

    def __init__(self, parent=None):
        super(QueryResponseWidget, self).__init__(parent)
        self.setupUi(self)

    def clear(self):
        self.set_info_text('')

        self._delete_buttons(self.submitButtons)
        self._delete_buttons(self.rejectButtons)
        self._delete_buttons(self.labelButtons)

    def _delete_buttons(self, box):

        for btn in box.buttons():
            while True:
                try:
                    btn.clicked.disconnect()
                except TypeError:
                    break
            box.removeButton(btn)
            btn.deleteLater()

    def set_info_text(self, text):
        """
        Sets the instructions for the user (e.g., "Select one action label:").

        Parameters
        ----------
        text : str
            User instruction
        """
        self.labelAction.setText(text)

    def add_accept_button(self, text, custom_action=False):
        action = QtWidgets.QDialogButtonBox.ActionRole if custom_action else QtWidgets.QDialogButtonBox.AcceptRole
        return self.submitButtons.addButton(text, action)

    def add_reject_button(self, text, custom_action=False):
        action = QtWidgets.QDialogButtonBox.ActionRole if custom_action else QtWidgets.QDialogButtonBox.RejectRole
        return self.rejectButtons.addButton(text, action)

    def add_label_button(self, text, mode='toggle', gradient=None):
        btn = QtWidgets.QPushButton(text, self)
        if gradient is not None:
            btn.setStyleSheet(make_gradient_stylesheet(gradient))

        if mode == 'accept':
            btn.clicked.connect(self._make_label_accept_func(text))
            self.labelButtons.addButton(btn, QtWidgets.QDialogButtonBox.ActionRole)
        else:
            btn.setCheckable(True)
            btn.toggled.connect(self.update_accept_btn)
            self.labelButtons.addButton(btn, QtWidgets.QDialogButtonBox.ActionRole)
            self.update_accept_btn(None)

        return btn

    def get_labels(self):
        labels = []
        for btn in self.labelButtons.buttons():
            if btn.isChecked():
                labels.append(btn.text())
        return labels

    def update_accept_btn(self, btn):
        for btn in self.labelButtons.buttons():
            if btn.isChecked():
                self.enable_accept()
                return
        self.disable_accept()

    def enable_accept(self):
        for btn in self.submitButtons.buttons():
            btn.setEnabled(True)

    def disable_accept(self):
        for btn in self.submitButtons.buttons():
            btn.setEnabled(False)

    def _make_label_accept_func(self, label):
        def accept_label():
            self.accepted.emit([label])
        return accept_label

    def accept(self):
        self.accepted.emit(self.get_labels())
