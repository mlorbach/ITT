# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-28 16:26:29
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:58:40

from PyQt5 import QtGui, QtCore, QtWidgets


class SystemTrayIcon(QtWidgets.QSystemTrayIcon):

    exit_requested = QtCore.pyqtSignal()

    """docstring for SystemTrayIcon"""
    def __init__(self, icon, parent=None):
        super(SystemTrayIcon, self).__init__(icon, parent)

        menu = QtWidgets.QMenu(parent)
        exitAction = menu.addAction("Exit")
        self.setContextMenu(menu)

        exitAction.triggered.connect(self.exit)

    def exit(self):
        self.exit_requested.emit()
