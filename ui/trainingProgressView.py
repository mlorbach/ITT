# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-20 12:19:57
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:56:57

import os
import logging
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.uic import loadUiType

# from ui.mplCanvas import BarPlotCanvas
import pyqtgraph as pg
# from ui.BarGraphItem import BarGraphItem

from ml.oracle import QueryResult

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_TrainingProgressWidget, QWidget = loadUiType(os.path.join(curpath, 'trainingProgressWidget.ui'))

pg.setConfigOption('background', None)
pg.setConfigOption('foreground', 'k')

logger = logging.getLogger(__name__)

class TrainingProgressView(QWidget, Ui_TrainingProgressWidget):
    """docstring for trainingProgressView"""

    @property
    def n_training_iterations(self):
        return int(self.lblNIterations.text())

    @n_training_iterations.setter
    def n_training_iterations(self, value):
        self.lblNIterations.setNum(value)

    @property
    def n_labeled_segments(self):
        return int(self.lblNLabeledSeg.text())

    @n_labeled_segments.setter
    def n_labeled_segments(self, value):
        self.lblNLabeledSeg.setNum(value)

    @property
    def n_rejected_segments(self):
        return int(self.lblNRejectedSeg.text())

    @n_rejected_segments.setter
    def n_rejected_segments(self, value):
        self.lblNRejectedSeg.setNum(value)

    @property
    def duration_video_labeled(self):
        return float(self.lblDurVideoLabeled.text())

    @duration_video_labeled.setter
    def duration_video_labeled(self, value):
        self.lblDurVideoLabeled.setNum(value)

    @property
    def frames_labeled(self):
        return int(self.lblFramesLabeled.text())

    @frames_labeled.setter
    def frames_labeled(self, value):
        self.lblFramesLabeled.setNum(value)

    def __init__(self, model, parent=None):
        super(TrainingProgressView, self).__init__(parent)
        self.setupUi(self)
        self.model = model

        self._pw = None
        self._bar = None
        self._nodata_text = pg.TextItem('No data yet',
                                        color=(0, 0, 0),
                                        anchor=(0.5, 0))
        self._nodata_text.setPos(0.5, .9)

    def setModel(self, model):
        self.model = model
        self.update_ui_from_model()

    @pyqtSlot()
    def update_ui_from_model(self):
        if self.model is None:
            return

        self.n_training_iterations = self.model.project.learning_stats.n_query_iterations

        self.n_labeled_segments = self.model.project.learning_stats.query_response_stats[QueryResult.LABELED]

        self.n_rejected_segments = self.model.project.learning_stats.query_response_stats[QueryResult.UNCERTAIN] + self.model.project.learning_stats.query_response_stats[QueryResult.SEGMENTATION]

        # labeling distribution
        y_dist = self.model.get_annotation().label_distribution()  # df

        # cnt, _ = self.model.get_annotation().count_labeled()
        self.frames_labeled = y_dist.sum()
        self.duration_video_labeled = self.frames_labeled / float(self.model.get_metadata()['fps'])

        y_dist /= float(self.model.get_metadata()['fps'])

        if self._pw is None:
            self._pw = self.plotLabelDist

            # Axis and ticks styling
            label_style = {'font-size': '6pt'}
            font_style = QtGui.QFont()
            font_style.setPointSize(6)

            self._pw.getAxis('bottom').setHeight(15)
            self._pw.getAxis('left').setWidth(None)

            self._pw.getAxis('bottom').setStyle(tickLength=0,
                                                tickTextHeight=6,
                                                autoExpandTextSpace=False)
            self._pw.getAxis('left').setStyle(tickTextWidth=10,
                                              autoExpandTextSpace=False)

            self._pw.getAxis('bottom').tickFont = font_style
            self._pw.getAxis('left').tickFont = font_style

            # Labels and ranges
            self._pw.setLabel('left', 'Seconds', **label_style)
            self._pw.addItem(self._nodata_text)
            self._pw.hideButtons()

        # setup or update x-axis labels
        if y_dist.shape[0] > 0:
            # y_dist is typically not empty (unless there are not labels specified yet); so we can set up the x-axis even if we don't have y_dist data yet (all 0)
            self._pw.setXRange(0, y_dist.shape[0] - 1)
            action_ticks = zip(range(y_dist.shape[0]), y_dist.index.values)
            self._pw.getAxis('bottom').setTicks([action_ticks])
            self._nodata_text.setPos(int(y_dist.shape[0] / 2), 1)

        if self._bar is None and y_dist.sum() > 0:
            # create bar plot for the first time

            self._pw.removeItem(self._nodata_text)
            self._pw.setYRange(0, y_dist.max())

            self._bar = pg.BarGraphItem(x=range(y_dist.shape[0]),
                            height=y_dist.values,
                            width=0.4,
                            brush='#42a7f4',
                            antialias=True)
            self._pw.addItem(self._bar)

            self._bar.getViewBox().setMouseEnabled(x=False, y=False)

        elif self._bar is not None and y_dist.sum() > 0:
            # simple data update
            self._bar.setOpts(x=range(y_dist.shape[0]), height=y_dist.values)
            self._pw.setYRange(0, y_dist.max())

        elif self._bar is not None and y_dist.sum() == 0:
            # data was available but is not anymore (e.g., project reset)
            self._pw.removeItem(self._bar)
            self._bar = None
            self._pw.setYRange(0, 1)
            self._pw.addItem(self._nodata_text)
