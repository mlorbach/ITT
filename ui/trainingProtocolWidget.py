# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-28 18:04:41
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 12:57:03

import os
import logging
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.uic import loadUiType

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_Widget, QWidget = loadUiType(os.path.join(curpath, 'trainingProtocol.ui'))

logger = logging.getLogger(__name__)


class TrainingProtocolWidget(QWidget, Ui_Widget):
    """docstring for TrainingProtocolWidget"""
    def __init__(self, parent=None):
        super(TrainingProtocolWidget, self).__init__(parent)
        self.setupUi(self)

    def set_protocol_values(self, max_iterations=None,
                            min_iterations=None, n_repetitions=None):

        if max_iterations is not None:
            self.numMaxIterations.setValue(max_iterations)

        if min_iterations is not None:
            self.numMinIterations.setValue(min_iterations)

        if n_repetitions is not None:
            self.numRepetitions.setValue(n_repetitions)

    def clear_progress(self):
        self.progressBarIteration.setValue(0)
        self.progressBarRepetition.setValue(0)
        self.set_status_message('Ready')

    def update_values(self, n_iter=None, repetition_idx=None,
                      max_iterations=None, max_repetitions=None):
        if n_iter is not None:
            self.progressBarIteration.setValue(n_iter)

        if max_iterations is not None:
            self.progressBarIteration.setMaximum(max_iterations)

        if repetition_idx is not None:
            self.progressBarRepetition.setValue(repetition_idx)

        if max_repetitions is not None:
            self.progressBarRepetition.setMaximum(max_repetitions)

    def set_status_message(self, text):
        self.lblProgressStatus.setText(text)
