# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-20 18:31:01
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2016-12-20 18:59:04

def make_gradient_stylesheet(break_pos,
                             color_left=(0, 177, 0, 255),
                             color_right=(255, 255, 255, 255)):

    gradient_style = 'background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba{color_left:}, stop:{stop_a:.2f} rgba{color_left:}, stop:{stop_b:.2f} rgba{color_right:}, stop:1 rgba{color_right:})'.format(stop_a=max(0, break_pos - .025),
                                stop_b=min(1, break_pos + .025),
                                color_left=color_left,
                                color_right=color_right)
    return gradient_style
