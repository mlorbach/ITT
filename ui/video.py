# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-09-15 12:12:00
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-31 11:21:58

import logging
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal

from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent

logger = logging.getLogger(__name__)


class VideoWidget(QtWidgets.QWidget):
    """docstring for VideoWidget"""

    finished_loading = pyqtSignal()
    loopmarkReached = pyqtSignal()

    def __init__(self, parent=None):
        super(VideoWidget, self).__init__(parent)

        self.currentMovieDirectory = ''
        self.fps = 25  # default fps, should be set by parent accordingly
        self.loopA = 0
        self.loopB = 0

        self._init_player()

        self.controlsLayout = None
        self.buttonsLayout = None
        self.createControls()
        self.createButtons()

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.qvideowidget)
        mainLayout.addLayout(self.buttonsLayout)
        mainLayout.addLayout(self.controlsLayout)
        self.setLayout(mainLayout)

    def _init_player(self):
        self.qvideowidget = QVideoWidget(self)
        self.player = QMediaPlayer(self, QMediaPlayer.VideoSurface)
        self.player.setVideoOutput(self.qvideowidget)
        self.player.setNotifyInterval(100)
        self.player.positionChanged.connect(self.tok)
        self.player.mediaStatusChanged.connect(self.mediaStateChanged)
        self.player.stateChanged.connect(self.playerStateChanged)
        self.player.durationChanged.connect(self.set_slider_range)

    def reinitialize_player(self):
        if self.player is not None:
            logger.log(15, 'Reinitialize video player.')

            # remove from layout
            self.layout().removeWidget(self.qvideowidget)
            self.qvideowidget.deleteLater()
            self.player.deleteLater()  # mark for deletion

            # recreate video player and insert at original spot in layout
            self._init_player()
            self.layout().insertWidget(0, self.qvideowidget)

            # recreate the controls that are linked to the player instance
            self.createControls()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Content initialization
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def clear(self):
        self.stop()
        self.player.setMedia(QMediaContent())

    def openFile(self, fileName, autoPlay=False):
        self.currentMovieDirectory = QtCore.QFileInfo(fileName).path()
        self.player.setMedia(QMediaContent(QtCore.QUrl.fromLocalFile(fileName)))
        if autoPlay:
            self.player.play()

    def set_play_loop(self, start, end):
        self.loopA = max(0, start)
        self.loopB = min(self.player.duration(), end)

        # set mark
        self.loopmarkReached.connect(self.back_to_loopA)

        # seek to start
        self.seek_ms(self.loopA)

        self.loop_enabled = True

        logger.debug('Playback loop enabled ({}-{})'.format(self.loopA, self.loopB))

    def disable_play_loop(self):
        self.loopA = 0
        self.loopB = self.player.duration()
        self.loopmarkReached.disconnect(self.back_to_loopA)
        self.loop_enabled = False

        logger.debug('Playback loop disabled')

    def setFPS(self, fps):
        self.fps = fps

    def setSeekingEnabled(self, state):
        self.frameSlider.setEnabled(state)

    def set_slider_range(self, time):
        self.frameSlider.setRange(0, time)

    def setControlsVisible(self, state):
        for idx in range(self.controlsLayout.count()):
            qitem = self.controlsLayout.itemAt(idx)
            if qitem.widget() is not None:
                qitem.widget().setVisible(state)

        for idx in range(self.buttonsLayout.count()):
            qitem = self.buttonsLayout.itemAt(idx)
            if qitem.widget() is not None:
                qitem.widget().setVisible(state)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Playback control
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def play(self):
        self.player.play()

    def pause(self):
        self.player.pause()

    def stop(self):
        self.player.stop()

    def is_playing(self):
        return self.player.state() == QMediaPlayer.PlayingState

    def is_paused(self):
        return self.player.state() == QMediaPlayer.PausedState

    def play_clicked(self):
        if self.is_playing():
            self.pause()
        else:
            self.play()

    def current_time(self):
        # returns seconds
        return self.player.position() / 1000.

    def current_frame(self):
        return int(self.current_time() * self.fps)

    def set_seek_position(self, sliderPos):
        self.seek_ms(sliderPos)

    def seek_ms(self, time):
        self.player.setPosition(time)

    def back_to_loopA(self):
        self.seek_ms(self.loopA)

    def forward_to_loopB(self):
        self.seek_ms(self.loopB)

    def previous_frame(self):
        """
        not working with Phonon
        """
        if not self.is_paused():
            self.pause()
        # logger.info('Current: {}'.format(self.player.currentTime()))
        self.seek_ms(self.player.position() - 1000./self.fps)

    def next_frame(self):
        """
        not working with Phonon
        """
        if not self.is_paused():
            self.pause()
        # logger.info('Current: {}'.format(self.player.currentTime()))
        self.seek_ms(self.player.position() + 1000./self.fps)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # UI
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def tok(self, time):
        times = time / 1000  # ms -> s
        ms = time - times * 1000
        h = times / 3600
        m = (times - 3600 * h) / 60
        s = (times - 3600 * h - m * 60)
        self.frameLabel.setText('%02d:%02d:%02d.%03d' % (h, m, s, ms))

        if time >= self.loopB:
            self.loopmarkReached.emit()

    def mediaStateChanged(self, state):
        logger.debug("Media state changes: {}".format(state))

        # if we were loading the video and are done with this now, let the world know:
        if state == QMediaPlayer.LoadedMedia:
            self.loopA = 0
            self.loopB = self.player.duration()
            self.finished_loading.emit()

    def playerStateChanged(self, state):
        logger.debug("Player state changes: {}".format(state))

        # toggle play/pause and stop button
        if state == QMediaPlayer.PlayingState:
            self.playButton.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaPause))
            self.playButton.setToolTip("Pause")
        else:
            self.playButton.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaPlay))
            self.playButton.setToolTip("Play")

        self.stopButton.setEnabled(state != QMediaPlayer.StoppedState)

    def createControls(self):
        if self.controlsLayout is not None:
            self.controlsLayout.removeWidget(self.frameSlider)
            self.controlsLayout.removeWidget(self.frameLabel)
            self.frameSlider.deleteLater()
            self.frameLabel.deleteLater()

        self.frameSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.frameSlider.setRange(0,0)
        self.frameSlider.sliderMoved.connect(self.set_seek_position)
        self.frameLabel = QtWidgets.QLabel(self)
        self.frameLabel.setAlignment(QtCore.Qt.AlignRight |
                                     QtCore.Qt.AlignVCenter)

        # speedLabel = QtWidgets.QLabel("Speed:")
        # self.speedSpinBox = QtWidgets.QSpinBox()
        # self.speedSpinBox.setRange(1, 9999)
        # self.speedSpinBox.setValue(100)
        # self.speedSpinBox.setSuffix("%")

        self.controlsLayout = QtWidgets.QHBoxLayout()
        self.controlsLayout.addWidget(self.frameSlider)
        self.controlsLayout.addWidget(self.frameLabel)
        # self.controlsLayout.addWidget(speedLabel, 2, 0)
        # self.controlsLayout.addWidget(self.speedSpinBox, 2, 1)

    def createButtons(self):
        iconSize = QtCore.QSize(36, 36)

        # PLAY/PAUSE BUTTON
        # ---------------
        self.playButton = QtWidgets.QToolButton()
        self.playButton.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaPlay))
        self.playButton.setIconSize(iconSize)
        self.playButton.setToolTip("Play")
        self.playButton.clicked.connect(self.play_clicked)


        # STOP BUTTON
        # ---------------
        self.stopButton = QtWidgets.QToolButton()
        self.stopButton.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaStop))
        self.stopButton.setIconSize(iconSize)
        self.stopButton.setToolTip("Stop")
        self.stopButton.clicked.connect(self.stop)

        # JUMP START BUTTON
        # ---------------
        self.jumpStartBtn = QtWidgets.QToolButton()
        self.jumpStartBtn.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaSkipBackward))
        self.jumpStartBtn.setIconSize(iconSize)
        self.jumpStartBtn.setToolTip("Jump back to start")
        self.jumpStartBtn.clicked.connect(self.back_to_loopA)

        # JUMP END BUTTON
        # ---------------
        self.jumpEndBtn = QtWidgets.QToolButton()
        self.jumpEndBtn.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaSkipForward))
        self.jumpEndBtn.setIconSize(iconSize)
        self.jumpEndBtn.setToolTip("Jump to end")
        self.jumpEndBtn.clicked.connect(self.forward_to_loopB)

        # PREVIOUS FRAME BUTTON
        # ---------------
        self.prevFrameBtn = QtWidgets.QToolButton()
        self.prevFrameBtn.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaSeekBackward))
        self.prevFrameBtn.setIconSize(iconSize)
        self.prevFrameBtn.setToolTip("Go back one frame")
        self.prevFrameBtn.clicked.connect(self.previous_frame)

        # NEXT FRAME BUTTON
        # ---------------
        self.nextFrameBtn = QtWidgets.QToolButton()
        self.nextFrameBtn.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MediaSeekForward))
        self.nextFrameBtn.setIconSize(iconSize)
        self.nextFrameBtn.setToolTip("Go forward one frame")
        self.nextFrameBtn.clicked.connect(self.next_frame)

        # BUTTONS LAYOUT
        # ---------------
        self.buttonsLayout = QtWidgets.QHBoxLayout()
        self.buttonsLayout.addWidget(self.jumpStartBtn)
        self.buttonsLayout.addWidget(self.prevFrameBtn)
        self.buttonsLayout.addStretch(20)
        self.buttonsLayout.addWidget(self.playButton)
        self.buttonsLayout.addWidget(self.stopButton)
        self.buttonsLayout.addStretch(20)
        self.buttonsLayout.addWidget(self.nextFrameBtn)
        self.buttonsLayout.addWidget(self.jumpEndBtn)
        self.buttonsLayout.addStretch()
