# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-11-28 17:47:21
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 14:38:22

import os
import logging
from glob import glob
from PyQt5 import QtGui
from PyQt5.uic import loadUiType

# get the directory of this script
curpath = os.path.dirname(os.path.abspath(__file__))
Ui_Dialog, QDialog = loadUiType(os.path.join(curpath, 'videoAnnotationDialog.ui'))

logger = logging.getLogger(__name__)


class VideoAnnotationDialog(QDialog, Ui_Dialog):
    def __init__(self, project_ctrl, videoname):
        super(VideoAnnotationDialog, self).__init__()
        self.setupUi(self)

        self.project_ctrl = project_ctrl
        self.videoname = videoname

        if self.videoname not in self.project_ctrl.get_video_names():
            raise ValueError('Not a valid video in this project: "{}".'.format(self.videoname))

        self.video = self.project_ctrl.get_video_collection().loc[self.videoname].to_dict()

        self.video['start_ms'] = int(self.video['first_valid_frame'] / self.video['fps'] * 1000)
        self.video['end_ms'] = int(self.video['last_valid_frame'] / self.video['fps'] * 1000)

        # we have to wait until the video player is all set up and the video is loaded before we can seek to a specific time. So we connect to a signal of the player that notifies us once it's ready:
        self.videoPlayer.finished_loading.connect(self._player_ready)

        # open the video file
        self.videoPlayer.setFPS(self.video['fps'])
        self._open_video(self.video['file'])

    def _open_video(self, filename):
        logger.debug('Opening video file {}'.format(filename))
        self.videoPlayer.openFile(filename)

    def _player_ready(self):
        if 'start_ms' in self.video and 'end_ms' in self.video:
            self._set_play_loop(self.video['start_ms'],
                                self.video['end_ms'])
        self.videoPlayer.play()

    def _set_play_loop(self, start, end):
        self.videoPlayer.set_play_loop(start, end)
