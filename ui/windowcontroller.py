# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2016-12-29 11:25:14
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-08-18 13:19:10
#
# Controls the MDI windows of the application.
# Creates them, shows them, hides them and gives access to the child widgets.

from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import pyqtSignal


class DockWindowController(QtCore.QObject):

    window_visibility_changed = pyqtSignal(str)

    """docstring for DockWindowController"""
    def __init__(self, main_window, default_parent=None):
        super(DockWindowController, self).__init__()
        self.default_parent = default_parent
        self.main_window = main_window
        self.windows = {}

    @staticmethod
    def translateDockArea(position):

        if position == 'left':
            return QtCore.Qt.LeftDockWidgetArea
        elif position == 'right':
            return QtCore.Qt.RightDockWidgetArea
        elif position == 'top':
            return QtCore.Qt.TopDockWidgetArea
        elif position == 'bottom':
            return QtCore.Qt.BottomDockWidgetArea
        elif position == 'float':
            return QtCore.Qt.BottomDockWidgetArea
        else:
            return QtCore.Qt.NoDockWidgetArea

    def __getitem__(self, name):
        return self.windows[name].widget()

    def get_window(self, name):
        return self.windows[name]

    def add_window(self, name, widget, dock_pos='float'):

        subWindow = QtWidgets.QDockWidget(widget.windowTitle(), self.default_parent)
        subWindow.setWidget(widget)
        self.windows[name] = subWindow
        self.main_window.addDockWidget(self.translateDockArea(dock_pos),
                                       subWindow)
        subWindow.setFloating(dock_pos == 'float')
        subWindow.visibilityChanged.connect(lambda: self.window_visibility_changed.emit(name))

    def close_window(self, name):
        if name in self.windows:
            self.main_window.removeDockWidget(self.windows[name])
            del self.windows[name]

    def toggle_visible(self, name):
        if name in self.windows:
            self.windows[name].setVisible(not self.windows[name].isVisible())

    def set_visible(self, name, state):
        if name in self.windows:
            self.windows[name].setVisible(state)
